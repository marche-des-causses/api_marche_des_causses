# API_Marche_des_Causses

**master:**
[![pipeline status](https://gitlab.com/marche-des-causses/api_marche_des_causses/badges/master/pipeline.svg)](https://gitlab.com/marche-des-causses/api_marche_des_causses/-/commits/master)
[![coverage report](https://gitlab.com/marche-des-causses/api_marche_des_causses/badges/master/coverage.svg)](https://gitlab.com/marche-des-causses/api_marche_des_causses/-/commits/master)
&emsp;**dev:**
[![pipeline status](https://gitlab.com/marche-des-causses/api_marche_des_causses/badges/dev/pipeline.svg)](https://gitlab.com/marche-des-causses/api_marche_des_causses/-/commits/dev)
[![coverage report](https://gitlab.com/marche-des-causses/api_marche_des_causses/badges/dev/coverage.svg)](https://gitlab.com/marche-des-causses/api_marche_des_causses/-/commits/dev)

API permettant de centraliser les informations sur la marche des Causses et ses participants