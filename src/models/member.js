'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {

  /**
   * Member Class
   */
  class Member extends Model {
    
    /**
     * Generic fields to use for databases requests
     */
    static basicFields = ["id", "teamId", "caussenardId", "isLeader"];

    /**
     * Generic order to use for databases requests
     */
    static basicOrder = [["createdAt","DESC"]]; 

    /**
     * Create Link between models
     * @param {Model} models 
     */
    static associate(models) {
      
      models.Team.belongsToMany(models.Caussenard, {
        through: models.Member,
        foreignKey: 'teamId',
        as: 'members'
      }),

      models.Caussenard.belongsToMany(models.Team, {
        through: models.Member,
        foreignKey: 'caussenardId',
        as: 'teams'
      })

      models.Member.belongsTo(models.Team, {
        foreignKey: 'teamId',
        as: 'team'
      })
      
      models.Member.belongsTo(models.Caussenard, {
        foreignKey: 'caussenardId',
        as: 'caussenard'
      })
      
    }
  };
  Member.init({
    teamId: DataTypes.INTEGER,
    caussenardId: DataTypes.INTEGER,
    isLeader: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Member',
  });
  return Member;
};