'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TeamMaker extends Model {

    /**
     * Generic fields to use for databases requests
     */
    static basicFields = ["id", "status", "progress", "updatedAt"];
    static fullFields = ["id", "status", "progress", "params", "result", "updatedAt"];

    /**
     * Generic order to use for databases requests
     */
    static basicOrder = [["updatedAt", "DESC"]];

    static associate(models) {
      // define association here
    }
  };
  TeamMaker.init({
    params: DataTypes.JSON,
    result: DataTypes.JSON,
    progress: DataTypes.INTEGER,
    status: DataTypes.ENUM('pending', 'running', 'finished', 'cancel', 'error')
  }, {
    sequelize,
    modelName: 'TeamMaker',
  });
  return TeamMaker;
};