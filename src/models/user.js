'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {

  /**
   * User Class
   */
  class User extends Model {

    /**
     * Generic fields to use for databases requests
     */
    static basicFields = ["id", "email", "authLevel", "password"];

    /**
     * Generic order to use for databases requests
     */
    static basicOrder = [["email", "ASC"]];
    
    /**
     * Create Link between models
     * @param {Model} models 
     */
    static associate(models) {
    
    }
  };
  User.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    authLevel: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};