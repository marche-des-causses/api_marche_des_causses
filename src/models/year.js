'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {

  /**
   * Year Class
   */
  class Year extends Model {
    
    /**
     * Generic fields to use for databases requests
     */
    static basicFields = ["id", "number", "theme"];

    /**
     * Generic order to use for databases requests
     */
    static basicOrder = [["number", 'DESC']];

    /**
     * Create Link between models
     * @param {Model} models 
     */
    static associate(models) {
      
      models.Year.hasMany(models.Team, { as: 'teams'})

    }
  };
  Year.init({
    number: DataTypes.INTEGER,
    theme: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Year',
  });
  return Year;
};