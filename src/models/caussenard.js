'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {

  /**
   * Caussenard Class
   */
  class Caussenard extends Model {

    /**
     * Generic fields to use for databases requests
     */
    static basicFields = ["id", "firstname", "lastname"];

    /**
     * Generic fields to use for databases requests
     */
    static classicFields = ["id", "firstname", "lastname", "birth", "gender"];

    /**
     * Generic order to use for databases requests
     */
    static basicOrder = [["lastname", 'ASC'], ["firstname", 'ASC']];
    
    /**
     * @summary Create Link between models
     * @param {Model} models 
     */
    static associate(models) {
      
    }
    
  };
  
  Caussenard.init({
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    birth: DataTypes.DATEONLY,
    gender: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Caussenard',
  });
  return Caussenard;
};