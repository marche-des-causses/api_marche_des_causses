'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {

  /**
   * Team Class
   */
  class Team extends Model {

    /**
     * Generic fields to use for databases requests
     */
    static basicFields = ["id", "number", "name", "yearId"];

    /**
     * Generic order to use for databases requests
     */
    static basicOrder = [["number", "ASC"]];
    static complexeOrder = [["year", "number", "DESC"], ["number", "ASC"]];

    /**
     * Create Link between models
     * @param {Model} models 
     */
    static associate(models) {
      
      models.Team.belongsTo(models.Year, {
        foreignkey: "yearId",
        as: 'year'
      }) 
      
    }
  };
  Team.init({
    yearId: DataTypes.INTEGER,
    number: DataTypes.INTEGER,
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Team',
  });
  return Team;
};