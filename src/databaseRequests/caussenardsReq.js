// Import
var models  = require("../models");
var Return = require("../functions/returns");
var errno = require("../functions/errno");
const { Op } = require("sequelize");
const { sequelize } = require("../models");


module.exports = {

  //------------Getter functions---------------

  getCaussenards: function(order, fields, limit, offset, where, res, done) {
    var orderList = [];
    if (order == undefined) {
      orderList = models.Caussenard.basicOrder;
    } else {
      var orderStr = order.split(";");
      if (orderStr.length == 1) {
        orderList.push(orderStr[0].split(":"));
        orderList.push([(orderList[0][0] == "firstname") ? "lastname" : "firstname" , orderList[0][1]]);
      } else {
        orderStr.forEach(function(ord) {
          orderList.push(ord.split(":"));
        });
      }
    }
        
    models.Caussenard.findAll({
      order: orderList,
      attributes: (fields !== "*" && fields != null) ? fields.split(",") : models.Caussenard.basicFields,
      limit: (!isNaN(limit)) ? limit : null,
      offset: (!isNaN(offset)) ? offset : null,
      distinct: true,
      where: where
    })
      .then(function(caussenardsFound) {
        done(null, caussenardsFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  getCaussenardInfos: function(id, res, done) {
    models.Caussenard.findOne({
      where: {id: id},
      attributes: models.Caussenard.classicFields,
      include: {
        model: models.Team,
        as: "teams",
        attributes: models.Team.basicFields,
        include: {
          model: models.Year,
          as: "year",
          attributes: ["id", "number"]
        },
        through: {
          as: "role",
          attributes: ["isLeader"]
        }
      }
    })
      .then(function(caussenardFound) {
        done(null, caussenardFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  //------------Finder functions---------------

  findCaussenardsWithId: function(order, fields, limit, offset, listOfId, res, done) {
    var orderList = [];
    if (order == undefined) {
      orderList = models.Caussenard.basicOrder;
    } else {
      var orderStr = order.split(";");
      if (orderStr.length == 1) {
        orderList.push(orderStr[0].split(":"));
        orderList.push([(orderList[0][0] == "firstname") ? "lastname" : "firstname" , orderList[0][1]]);
      } else {
        orderStr.forEach(function(ord) {
          orderList.push(ord.split(":"));
        });
      }
    }

    models.Caussenard.findAll({
      order: orderList,
      attributes: (fields !== "*" && fields != null) ? fields.split(",") : models.Caussenard.basicFields,
      limit: (!isNaN(limit)) ? limit : null,
      offset: (!isNaN(offset)) ? offset : null,
      where: {id: listOfId},
      distinct: true
    })
      .then(function(caussenardsFound) {
        done(null, caussenardsFound);
      })
      .catch(function() {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", "findCaussenardsWithId"));
      });
  },

  findCaussenardWithName: function(firstname, lastname, res, done) {
    models.Caussenard.findOne({
      attributes: models.Caussenard.basicFields,
      where: {
        firstname: firstname,
        lastname: lastname
      }
    })
      .then(function(caussenardFound) {
        done(null, caussenardFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  //------------Classical functions---------------

  createCaussenard: function(firstname, lastname, birth, gender, res, done) {
    models.Caussenard.create({
      firstname: firstname,
      lastname: lastname,
      birth: birth,
      gender: gender,
    }, {
      fields: models.Caussenard.classicFields
    })
      .then(function(newCaussenard) {
        done(null, newCaussenard);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.create_fail, "Internal Error", err));
      });
  },

  update: function(caussenard, newFirstname, newLastname, newBirth, newGender, res, done) {
    caussenard.update({
      firstname: (newFirstname != undefined) ? newFirstname : caussenard.firstname,
      lastname:  (newLastname != undefined) ? newLastname : caussenard.lastname,
      birth:  (newBirth != undefined) ? newBirth : caussenard.birth,
      gender:  (newGender != undefined) ? newGender : caussenard.gender,
    }, {
      fields: models.Caussenard.classicFields
    })
      .then(function(caussenardFound) {
        done(null, caussenardFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.update_fail, "Internal Error", err));
      });
  },

  delete: function(id, res, done) {
    models.Caussenard.destroy({
      where: {id: id}
    }, {
      fields: models.Caussenard.basicFields
    })
      .then(function(caussenardDestroyed) {
        done(null, caussenardDestroyed);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.delete_fail, "Internal Error", err));
      });
  },

  nbOfCaussenards: function(where, res, done) {
    models.Caussenard.count({
      where: where
    })
      .then(function(count) {
        done(null, count);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  getAgeofCaussenards: function(caussenardId, date, res, done) {
    models.Caussenard.findAll({
      where: {id: caussenardId, birth: {[Op.not]: null}},
      distinct: true,
      attributes: [
        [sequelize.fn("datediff", date, sequelize.col("birth")), "age"]
      ]
    })
      .then(function(caussenardsFound) {
        done(null, caussenardsFound);
      })
      .catch(function() {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", "getAgeofCaussenards"));
      });
  }

};



