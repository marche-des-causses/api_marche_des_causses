// Import 
var asyncLib  = require("async");
var Return = require("../functions/returns");
var errno = require("../functions/errno");
var yeardb = require("./yearsReq");
var teamdb = require("./teamsReq");
var caussenarddb = require("./caussenardsReq");

module.exports = {

  caussenardsCountOfYear: function(yearId, res) {
    return new Promise(function (resolve) {
      asyncLib.waterfall([
        // Check if the year exist and get full informations
        function (done) {
          yeardb.getYearInfos(yearId, res, done);
        },
        // Translate into an array of teamId
        function (yearFound, done) {
          if (yearFound) {
            var caussenardsIdFound = [];
            yearFound.dataValues.teams.forEach(function(team) {
              team.dataValues.members.forEach(function (caussenard) {
                caussenardsIdFound.push(caussenard.dataValues.id);
              });
            }); 
            done(null, caussenardsIdFound);
          } else {
            done(Return.fail(res, 404, errno.not_exist, "Année inexistant", "year with id " + yearId + " was not found"));
          }
        },
        // Find caussenards with Id
        function(caussenardsIdFound, done) {
          done(caussenardsIdFound.length);
        }
      ], 
      function(stats) {
        resolve(stats);
      });
    });
  },
  
  caussenardsDistinctCountOfYear: function(yearId, list, res) {
    return new Promise(function (resolve) {
      asyncLib.waterfall([
        // Check if the year exist and get full informations
        function (done) {
          yeardb.getYearInfos(yearId, res, done);
        },
        // Translate into an array of teamId
        function (yearFound, done) {
          if (yearFound) {
            var caussenardsIdFound = [];
            yearFound.dataValues.teams.forEach(function(team) {
              team.dataValues.members.forEach(function (caussenard) {
                if (!(list.some((e) => e == caussenard.dataValues.id))) {
                  list.push(caussenard.dataValues.id);
                  caussenardsIdFound.push(caussenard.dataValues.id);
                }
              });
            }); 
            done(null, caussenardsIdFound);
          } else {
            done(Return.fail(res, 404, errno.not_exist, "Année inexistant", "year with id " + yearId + " was not found"));
          }
        },
        // Find caussenards with Id
        function(caussenardsIdFound, done) {
          done(caussenardsIdFound.length);
        }
      ], 
      function(stats) {
        resolve(stats);
      });
    });
  },

  caussenardGenderOfYear: function(yearId, res) {
    return new Promise(function (resolve) {
      var nbOfCaussenards = 0;
      asyncLib.waterfall([
        // Check if the year exist and get full informations
        function (done) {
          yeardb.getYearInfos(yearId, res, done);
        },
        // Translate into an array of teamId
        function (yearFound, done) {
          if (yearFound) {
            var caussenardsIdFound = [];
            yearFound.dataValues.teams.forEach(function(team) {
              team.dataValues.members.forEach(function (caussenard) {
                caussenardsIdFound.push(caussenard.dataValues.id);
              });
            }); 
            done(null, caussenardsIdFound);
          } else {
            done(Return.fail(res, 404, errno.not_exist, "Année inexistant", "year with id " + yearId + " was not found"));
          }
        },
        // Find caussenards with Id
        function(caussenardsIdFound, done) {
          if (caussenardsIdFound.length > 0) {
            nbOfCaussenards = caussenardsIdFound.length;
            caussenarddb.nbOfCaussenards({id: caussenardsIdFound, gender: 1}, res, done);
          } else {
            done(null, 0);
          }
        },
        // Calculate stats
        function(nbOfMen, done) {
          done({men: nbOfMen, 
            women: nbOfCaussenards - nbOfMen});
        }
      ], 
      function(stats) {
        resolve(stats);
      });
    });
  },

  caussenardAgeOfYear: function(yearId, res) {
    return new Promise((resolve) => {
      var yearNumber = 0;
      asyncLib.waterfall([
        // Check if the year exist and get full informations
        function (done) {
          yeardb.getYearInfos(yearId, res, done);
        },
        // Translate into an array of teamId
        function (yearFound, done) {
          if (yearFound) {
            yearNumber = yearFound.number;
            var caussenardsIdFound = [];
            yearFound.dataValues.teams.forEach(function(team) {
              team.dataValues.members.forEach(function (caussenard) {
                caussenardsIdFound.push(caussenard.dataValues.id);
              });
            }); 
            done(null, caussenardsIdFound);
          } else {
            done(Return.fail(res, 404, errno.not_exist, "Année inexistant", "year with id " + yearId + " was not found"));
          }
        },
        // Find caussenards with Id
        function(caussenardsIdFound, done) {
          if (caussenardsIdFound.length > 0) {
            caussenarddb.getAgeofCaussenards(caussenardsIdFound, new Date(yearNumber, 4, 15), res, done);
          } else {
            done(null, caussenardsIdFound);
          }
        },
        // Find caussenards with Id
        function(agesFound, done) {
          done(agesFound);
        }
      ],
      // Return the response
      function(agesFound) {
        resolve(agesFound);  
      });
    });
  },

  teamsCountOfYear: function(yearId, res) {

    return new Promise((resolve) => {
      asyncLib.waterfall([
        function(done) {
          teamdb.nbOfTeamsOfYear(yearId, res, done);
        },
        function(nbOfTeam, done) {
          done(nbOfTeam);
        }
      ], 
      function (nbOfTeam) {
        resolve(nbOfTeam);
      });
    });

  }

};

