// Import
var models  = require("../models");
var Return = require("../functions/returns");
var errno = require("../functions/errno");


module.exports = {

  //------------Getter functions---------------

  getTeamMakers: function(order, fields, limit, offset, res, done) {
    var orderList = [];
    if (order == undefined) {
      orderList = models.TeamMaker.basicOrder;
    } else {
      const orderStr = order.split(";");
      orderStr.forEach(function(ord) {
        orderList.push(ord.split(":"));
      });
    }

    models.TeamMaker.findAll({
      order: orderList,
      attributes: (fields !== "*" && fields != null) ? fields.split(",") : models.TeamMaker.basicFields,
      limit: (!isNaN(limit)) ? limit : null,
      offset: (!isNaN(offset)) ? offset : null,
      distinct: true
    }).then(function(teamMakersFound) {
      done(null, teamMakersFound);
    }).catch(function(err) {
      done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
    });
  },

  getTeamMakerInfos: function(id, res, done) {
    models.TeamMaker.findOne({
      where: {
        id: id
      },
      attributes: models.TeamMaker.fullFields,
    })
      .then(function(teamMakersFound) {
        done(null, teamMakersFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error 1", err));
      });
  },
    
  //------------Finder functions---------------

  findTeamMakersWithstatus: function(order, fields, limit, offset, status, res, done) {
    var orderList = [];
    if (order == undefined) {
      orderList = models.TeamMaker.basicOrder;
    } else {
      var orderStr = order.split(";");
      orderStr.forEach(function(ord) {
        orderList.push(ord.split(":"));
      });
    }
    models.TeamMaker.findAll({
      order: orderList,
      attributes: (fields !== "*" && fields != null) ? fields.split(",") : null,
      limit: (!isNaN(limit)) ? limit : null,
      offset: (!isNaN(offset)) ? offset : null,
      where: {status: status},
      distinct: true
    }).then(function(teamMakersFound) {
      done(null, teamMakersFound);
    }).catch(function(err) {
      done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
    });
  },

  //------------Classical functions---------------

  createTeamMaker: function(params, res, done) {
    models.TeamMaker.create({
      params: params,
      progress: 0,
    }).then(function(newTeamMaker) {
      done(null, newTeamMaker);
    }).catch(function(err) {
      done(Return.fail(res, 500, errno.create_fail, "Internal Error", err));
    });
  },

  update: function(teamMaker, newStatus, newProgress, newresult, res, done) {
    teamMaker.update({
      status: (newStatus ? newStatus : teamMaker.status),
      progress:  (newProgress ? newProgress : teamMaker.progress),
      result:  (newresult ? newresult : teamMaker.result)
    }, {
      fields: models.TeamMaker.basicFields
    }).then(function() {
      done(null, teamMaker);
    }).catch(function(err) {
      done(Return.fail(res, 500, errno.update_fail, "Internal Error", err));
    });
  },

  delete: function(id, res, done) {
    models.TeamMaker.destroy({
      where: {id: id}
    }, {
      fields: models.TeamMaker.basicFields
    }).then(function(teamMakerDestroyed) {
      done(null, teamMakerDestroyed);
    }).catch(function(err) {
      done(Return.fail(res, 500, errno.delete_fail, "Internal Error", err));
    });
  },

  deleteTeamMakersFromYearId: function(yearId, res, done) {
    models.TeamMaker.destroy({
      where: {yearId: yearId}
    }, {
      fields: models.TeamMaker.basicFields
    }).then(function(teamMakersDestroyed) {
      done(null, teamMakersDestroyed);
    }).catch(function(err) {
      done(Return.fail(res, 500, errno.delete_fail, "Internal Error", err));
    });
  },

  nbOfTeamMakers: function(res, done) {
    models.TeamMaker.count({
      distinct: true 
    }).then(function(count) {
      done(null, count);
    }).catch(function(err) {
      done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
    });
  },

};


