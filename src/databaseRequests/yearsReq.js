// Import
var models  = require("../models");
var Return = require("../functions/returns");
var errno = require("../functions/errno");
const { Op } = require("sequelize");


module.exports = {

  //------------Getter functions---------------

  getYears: function(order, fields, limit, offset, where, res, done) {
    var orderList = [];
    if (order == undefined) {
      orderList = models.Year.basicOrder;
    } else {
      var orderStr = order.split(";");
      orderStr.forEach(function(ord) {
        orderList.push(ord.split(":"));
      });
    }

    models.Year.findAll({
      order: orderList,
      attributes: (fields !== "*" && fields != null) ? fields.split(",") : models.Year.basicFields,
      limit: (!isNaN(limit)) ? limit : null,
      offset: (!isNaN(offset)) ? offset : null,
      distinct: true,
      where: where
    })
      .then(function(yearsFound) {
        done(null, yearsFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  getYearInfos: function(id, res, done) {
    models.Year.findOne({
      where: {
        id: id
      },
      attributes: models.Year.basicFields,
      include: {
        model: models.Team,
        as: "teams",
        attributes: models.Team.basicFields,
        include: {
          model: models.Caussenard,
          as: "members",
          attributes: ["id"],
          through: {
            as: "role",
            attributes: ["isLeader"]
          }
        }
      }
    }).then(function(yearsFound) {
      done(null, yearsFound);
    })
      .catch(function(err) {
        console.log(err);
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", "getYearInfos"));
      });
  },
    
  //------------Finder functions---------------
    
  findYearsWithId: function(order, fields, limit, offset, listOfId, res, done) {
    var orderList = [];
    if (order == undefined) {
      orderList = models.Year.basicOrder;
    } else {
      var orderStr = order.split(";");
      if (orderStr.length == 1) {
        orderList = [orderStr[0].split(":")];
      } else {
        orderStr.forEach(function(ord) {
          orderList.push(ord.split(":"));
        });
      }
    }

    models.Year.findAll({
      order: orderList,
      attributes: (fields !== "*" && fields !== undefined && fields != null) ? fields.split(",") : models.Year.basicFields,
      limit: (!isNaN(limit)) ? limit : null,
      offset: (!isNaN(offset)) ? offset : null,
      where: {
        id: listOfId
      },
      distinct: true
    })
      .then(function(yearsFound) {
        done(null, yearsFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  findYearWithNumber: function(number, res, done) {
    models.Year.findOne({
      where: {number: number},
      attributes: models.Year.basicFields
    })
      .then(function(yearFound) {
        done(null, yearFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },
    
  findYearWithId: function(id, res, done) {
    models.Year.findOne({
      where: {id: id},
      attributes: models.Year.basicFields
    })
      .then(function(yearFound) {
        done(null, yearFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  selectYearsBetween: function(start, end, order, res, done) {

    console.log("je lance la recherche");
    console.log(order);
    models.Year.findAll({
      where: {number: {[Op.between]: [start, end]}},
      attributes: ["id", "number"],
      order: order
    })
      .then(function(yearFound) {
        console.log("job fini");
        done(null, yearFound);
      })
      .catch(function() {
        console.log("petite erreur");
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", "FindYearId"));
      });
    console.log("recherche lancé");
  },

  //------------Classical functions---------------

  createYear: function(number, theme, res, done) {
    models.Year.create({
      number: number,
      theme: theme,
    }, {
      fields: models.Year.basicFields
    })
      .then(function(newYear) {
        done(null, newYear);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.create_fail, "Internal Error", err));
      });
  },

  update: function(year, newNumber, newTheme, res, done) {
    year.update({
      number: (newNumber ? newNumber : year.number),
      theme:  (newTheme? newTheme : year.theme),
    })
      .then(function(yearFound) {
        done(null, yearFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.update_fail, "Internal Error", err));
      });
  },

  delete: function(id, res, done) {
    models.Year.destroy({
      where: {id: id}
    }, {
      fields: models.Year.basicFields
    })
      .then(function() {
        done(null);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.delete_fail, "Internal Error", err));
      });
  },

  nbOfYears: function(where, res, done) {
    models.Year.count({
      where: where
    })
      .then(function(count) {
        done(null, count);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  }
  
};



