// Import
var models  = require("../models");
var Return = require("../functions/returns");
var errno = require("../functions/errno");


module.exports = {

  //------------Getter functions---------------

  getMembers: function(order, fields, limit, offset, res, done) {
    var orderList = [];
    if (order == undefined) {
      orderList = models.Member.basicOrder;
    } else {
      var orderStr = order.split(";");
      orderStr.forEach(function(ord) {
        orderList.push(ord.split(":"));
      });
    }

    models.Member.findAll({
      order: orderList,
      attributes: (fields !== "*" && fields != null) ? fields.split(",") : models.Member.basicFields,
      limit: (!isNaN(limit)) ? limit : null,
      offset: (!isNaN(offset)) ? offset : null
    })
      .then(function(membersFound) {
        done(null, membersFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  //------------Finder functions---------------
  
  findMemberWithIds: function(teamId, caussenardId, res, done) {
    models.Member.findOne({
      attributes: models.Member.basicFields,
      where: {
        teamId: teamId,
        caussenardId: caussenardId
      }
    })
      .then(function(memberFound) {
        done(null, memberFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  //------------Classical functions---------------

  createMember: function(teamId, caussenardId, isLeader, res, done) {
    models.Member.create({
      teamId: teamId,
      caussenardId: caussenardId,
      isLeader: isLeader
    })
      .then(function(newMember) {
        done(null, newMember);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.create_fail, "Internal Error", err));
      });
  },

  update: function(member, isLeader, res, done) {
    member.update({
      isLeader: (isLeader != null ? isLeader : member.isLeader),
    })
      .then(function(member) {
        done(null, member);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.update_fail, "Internal Error", err));
      });
  },

  deleteMember: function(teamId, caussenardId, res, done) {
    models.Member.destroy({
      where: {teamId: teamId, caussenardId: caussenardId}
    }, {
      fields: models.Member.basicFields
    })
      .then(function(membersDeleted) {
        done(null, membersDeleted);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.delete_fail, "Internal Error", err));
      });
  },

  deleteMembersFromTeam: function(teamId, res, done) {
    models.Member.destroy({
      where: {teamId: teamId}
    }, {
      fields: models.Member.basicFields
    })
      .then(function(membersDeleted) {
        done(null, membersDeleted);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.delete_fail, "Internal Error", err));
      });
  },

  deleteMembersFromCaussenard: function(caussenardId, res, done) {
    models.Member.destroy({
      where: {caussenardId: caussenardId}
    }, {
      fields: models.Member.basicFields
    })
      .then(function(membersDeleted) {
        done(null, membersDeleted);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.delete_fail, "Internal Error", err));
      });
  },

  nbOfMembers: function(res, done) {
    models.Member.count({
    })
      .then(function(count) {
        done(null, count);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  nbOfMembersInTeams: function(teamIds, res, done) {
    models.Member.count({
      where: { teamId: teamIds }
    })
      .then(function(count) {
        done(null, count);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  }
};



