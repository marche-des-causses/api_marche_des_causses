// Import
var models  = require("../models");
var Return = require("../functions/returns");
var errno = require("../functions/errno");


module.exports = {

  //------------Getter functions---------------

  getTeams: function(order, fields, limit, offset, where, res, done) {
    var orderList = [];
    if (order == undefined) {
      orderList = models.Team.basicOrder;
    } else {
      const orderStr = order.split(";");
      orderStr.forEach(function(ord) {
        orderList.push(ord.split(":"));
      });
    }

    models.Team.findAll({
      include: "year",
      order: orderList,
      attributes: (fields !== "*" && fields != null) ? fields.split(",") : models.Team.basicFields,
      limit: (!isNaN(limit)) ? limit : null,
      offset: (!isNaN(offset)) ? offset : null,
      distinct: true,
      where: where
    })
      .then(function(teamsFound) {
        done(null, teamsFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  getTeamInfos: function(id, res, done) {
    models.Team.findOne({
      where: {
        id: id
      },
      attributes: models.Team.basicFields,
      include: [{
        model: models.Caussenard,
        as: "members",
        attributes: ["id"],
        through: {
          as: "role",
          attributes: ["isLeader"]
        }
      },{
        model: models.Year,
        as: "year",
        attributes: models.Year.basicFields
      }]
    })
      .then(function(teamsFound) {
        done(null, teamsFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error 1", err));
      });
  },
    
  //------------Finder functions---------------

  findTeamsWithId: function(order, fields, limit, offset, listOfId, res, done) {
    var orderList = [];
    if (order == undefined) {
      orderList = models.Team.complexeOrder;
    } else {
      var orderStr = order.split(";");
      orderStr.forEach(function(ord) {
        orderList.push(ord.split(":"));
      });
    }
        
    models.Team.findAll({
      order: orderList,
      attributes: (fields !== "*" && fields != null) ? fields.split(",") : null,
      limit: (!isNaN(limit)) ? limit : null,
      offset: (!isNaN(offset)) ? offset : null,
      where: {id: listOfId},
      include: {
        model: models.Year,
        as: "year",
        attributes: models.Year.basicFields,
      },
      distinct: true
    })
      .then(function(teamsFound) {
        done(null, teamsFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },
    
  findTeamWithId: function(id, res, done) {
    models.Team.findOne({
      attributes: models.Team.basicFields,
      where: {id: id}
    })
      .then(function(teamFound) {
        done(null, teamFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },
    
  findTeamWithYearIdAndNumber: function(yearId, number, res, done) {
    models.Team.findOne({
      attributes: models.Team.basicFields,
      where: {
        yearId: yearId,
        number: number
      }
    })
      .then(function(teamFound) {
        done(null, teamFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal-Error", err));
      });
  },

  findTeamIds: function(where, res, done) {
    models.Team.findOne({
      attributes: ["id"],
      where: where
    })
      .then(function(teamFound) {
        done(null, teamFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  //------------Classical functions---------------

  createTeam: function(number, name, yearId, res, done) {
    models.Team.create({
      number: number,
      name: name,
      yearId: yearId
    }, {
      fields: models.Team.basicFields
    })
      .then(function(newTeam) {
        done(null, newTeam);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.create_fail, "Internal Error", err));
      });
  },

  update: function(team, newNumber, newName, newYearId, res, done) {
    team.update({
      number: (newNumber ? newNumber : team.number),
      name:  (newName ? newName : team.name),
      yearId:  (newYearId ? newYearId : team.yearId)
    }, {
      fields: models.Team.basicFields
    })
      .then(function() {
        done(null, team);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.update_fail, "Internal Error", err));
      });
  },

  delete: function(id, res, done) {
    models.Team.destroy({
      where: {id: id}
    }, {
      fields: models.Team.basicFields
    })
      .then(function(teamDestroyed) {
        done(null, teamDestroyed);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.delete_fail, "Internal Error", err));
      });
  },

  deleteTeamsFromYearId: function(yearId, res, done) {
    models.Team.destroy({
      where: {yearId: yearId}
    }, {
      fields: models.Team.basicFields
    })
      .then(function(teamsDestroyed) {
        done(null, teamsDestroyed);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.delete_fail, "Internal Error", err));
      });
  },

  nbOfTeams: function(where, res, done) {
    models.Team.count({
      distinct: true ,
      where: where
    })
      .then(function(count) {
        done(null, count);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  nbOfTeamsOfYear: function(yearId, res, done) {
    models.Team.count({
      distinct: true,
      where: {
        yearId: yearId,
      }
    })
      .then(function(count) {
        done(null, count);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  }

};


