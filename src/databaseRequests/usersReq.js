// Import
var models  = require("../models");
var Return = require("../functions/returns");
var errno = require("../functions/errno");


module.exports = {

  //------------Getter functions---------------

  getUsers: function(order, fields, limit, offset, where, res, done) {
    var orderList = [];
    if (order == undefined) {
      orderList = models.User.basicOrder;
    } else {
      var orderStr = order.split(";");
      orderStr.forEach(function(ord) {
        orderList.push(ord.split(":"));
      });
    }
    models.User.findAll({
      order: orderList,
      attributes: (fields !== "*" && fields != null) ? fields.split(",") : models.User.basicFields,
      limit: (!isNaN(limit)) ? limit : null,
      offset: (!isNaN(offset)) ? offset : null,
      distinct: true,
      where: where
    })
      .then(function(usersFound) {
        done(null, usersFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  },

  getUserInfos: function(id, res, done) {
    models.User.findOne({
      attributes: models.User.basicFields,
      where: { id: id }
    }).then(function(userFound) {
      done(null, userFound);
    }).catch(function(err) {
      done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
    });
  },

  //------------Finder functions---------------
    
  findUserWithId: function(id, res, done) {
    models.User.findOne({
      attributes: ["id", "email", "password"],
      where: {id: id}
    })
      .then(function(userFound) {
        done(null, userFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error_", err));
      });
  },

  findUserWithEmail: function(email, res, done) {
    models.User.findOne({
      attributes: models.User.basicFields,
      where: {email: email},
      distinct: true
    })
      .then(function(userFound) {
        done(null, userFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error !", err));
      });
  },

  //------------Classical functions---------------

  createUser: function( email, password, authLevel, res, done) {
    models.User.create({
      email: email,
      password: password,
      authLevel: authLevel
    })
      .then(function(newUser) {
        done(null, newUser);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.create_fail, "Internal Error", err));
      });
  },

  updateUser: function(user, newEmail, newPassword, newAuthLevel, res, done) {
    user.update({
      email: (newEmail ? newEmail : user.email),
      password: (newPassword ? newPassword : user.password),
      authLevel: (newAuthLevel ? newAuthLevel : user.authLevel),
    }, {
      fields: models.User.basicFields
    })
      .then(function(userFound) {
        done(null, userFound);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.update_fail, "Internal Error", err));
      });
  },

  deleteUser: function(id, res, done) {
    models.User.destroy({
      where: {id: id}
    })
      .then(function(userDeleted) {
        done(null, userDeleted);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.delete_fail, "Internal Error", err));
      });
  },

  nbOfUsers: function(where, res, done) {
    models.User.count({
      distinct: true,
      where: where
    })
      .then(function(count) {
        done(null, count);
      })
      .catch(function(err) {
        done(Return.fail(res, 500, errno.find_fail, "Internal Error", err));
      });
  }
};



