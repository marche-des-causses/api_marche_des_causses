// Imports
var jwt = require("jsonwebtoken");
var Return = require("../functions/returns");
var errno = require("../functions//errno");

// Exported functions
module.exports = {
  
  /**
   * @description Generate Token for user
   * @param {Object} userData 
   * @returns a jwtToken object
   */
  generateTokenForUser: function(userData) {
    if (userData) {
      return jwt.sign({
        userId: userData.id,
        authLevel: userData.authLevel
      },
      process.env.JWT_SIGN_SECRET,
      {
        expiresIn: "24h"
      });
    } else {
      return jwt.sign({
        authLevel: 0
      },
      process.env.JWT_SIGN_SECRET,
      {
        expiresIn: "24h"
      });
    
    }
  },

  /**
   * @description Parse the string to get the Token
   * @param {String} authorization
   * @returns the Token in String
   */
  parseAuthorization: function(authorization) {
    return (authorization != null) ? authorization.replace("Bearer ", "") : null;
  },

  /**
   * @description Get Token from authorization
   * @param {String} authorization 
   * @return jwtToken
   */
  getToken: function(authorization) {
    var jwtToken = null;
    var token = module.exports.parseAuthorization(authorization);
    if(token != null) {
      jwtToken = jwt.decode(token);
    }
    return jwtToken;
  },


  /**
   * @description To verify authorizations
   * @param {Integer} authLevel Authentification level required
   * @param {*} req request
   * @param {*} res response
   * @returns the token if check succeed else 0.
   */
  checkAuthorization: function(authLevel, req, res, done) {
    var token = this.parseAuthorization(req.headers["authorization"]);
    var decoded;

    if (token == "null") {
      done(Return.fail(res, 498, errno.miss_token, "Token missing", "Token missing"));
      return null;
    }
    
    try {
      decoded = jwt.verify(token, process.env.JWT_SIGN_SECRET);
    } catch(err) {
      switch (err.name) {
      case "TokenExpiredError":
        done(Return.fail(res, 498, errno.expired_token, "Token expired", err.message));
        break;
      case "JsonWebTokenError":
        done(Return.fail(res, 498, errno.invalid_token, "Invalid token", err.message));
        break;
      default:
        done(Return.fail(res, 498, errno.failed_token, "Invalid token", err.message));
        break;
      }
      return null;
    }
    
    if (decoded.authLevel < authLevel) {
      done(Return.fail(res, 401, errno.unauthorized, "Unauthorized", "Unauthorized"));
      return null;
    }
    
    return decoded;
  }

};