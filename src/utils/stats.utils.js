
module.exports = {

  ageStats: function(listAge) {
    var sum = 0;
    var list = new Array();
    var sum2 = 0;
    listAge.forEach(caussenard => {
      sum += caussenard.dataValues.age/365;
      list.push(Math.trunc(caussenard.dataValues.age/365));
    });
    var average = (listAge.length == 0) ? 0 : sum/listAge.length;
    listAge.forEach(caussenard => {
      sum2 += Math.pow(caussenard.dataValues.age/365 - average, 2);
    });
    var sd = (listAge.length == 0) ? 0 : Math.sqrt(sum2/listAge.length);

    return {
      average: average,
      sd: sd,
      breakdown: this.getRepartitionIntList(list)
    };
  },

  genderStats: function(genderObject) {
    return {
      percent: {
        men: (genderObject.men+genderObject.women == 0) ? 0 : Math.round(genderObject.men/(genderObject.men+genderObject.women)*100),
        women: (genderObject.men+genderObject.women == 0) ? 0 : Math.round(genderObject.women/(genderObject.men+genderObject.women)*100),
      },
      number: {
        men: genderObject.men,
        women: genderObject.women
      }
    };
  },

  getRepartition: function (list) {
    var result = {};
    list.forEach(value => {
      if (result.get(value)) {
        result.set(value, result.get(value) + 1);
      }
    });
    return result;
  },

  getRepartitionIntList: function(list) {
    const max = Math.max(...list);
    const min = Math.min(...list);
    var result = new Object();
    for (let k = min; k <= max; k++) {
      result[k] = list.filter(value => value == k).length;
    }
    return result;
  },

  stats(list) {
    var sum = 0;
    var sum2 = 0;
    var min = list[0] ? list[0] : 0;
    list.forEach(value => {
      sum += value;
      min = (min > value) ? value : min;
    });
    var average = (list.length == 0) ? 0 : sum/list.length;
    list.forEach(value => {
      sum2 += Math.pow(value - average, 2);
    });
    var sd = (list.length == 0) ? 0 : Math.sqrt(sum2/list.length);
    return {
      average: average,
      sd: sd,
      rsd: sd/average,
      min: min
    };
  }

};