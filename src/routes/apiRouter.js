// Import 
var express = require("express");

var yearsCtrl       = require("./api/yearsCtrl");
var teamsCtrl       = require("./api/teamsCtrl");
var caussenardsCtrl = require("./api/caussenardsCtrl");
var membersCtrl     = require("./api/membersCtrl");
var usersCtrl       = require("./api/usersCtrl");
var teamMakersCtrl  = require("./api/teamMakersCtrl");
var loadCtrl        = require("./api/loadCtrl.js");

// Router
exports.router = (function() {
  var apiRouter = express.Router();

  apiRouter.route("/token/")  .get(usersCtrl.getAPIKey);
  apiRouter.route("/token/up").get(usersCtrl.updateToken);
       
  // Users Routes 
  apiRouter.route("/users/")          .get(usersCtrl.getList);
  apiRouter.route("/users/")          .post(usersCtrl.new);
  apiRouter.route("/users/search")    .get(usersCtrl.search);
  apiRouter.route("/users/register/") .post(usersCtrl.register);
  apiRouter.route("/users/login/")    .post(usersCtrl.login);
  apiRouter.route("/users/:id/")      .get(usersCtrl.getOne);
  apiRouter.route("/users/:id/")      .put(usersCtrl.update);
  apiRouter.route("/users/:id/")      .delete(usersCtrl.delete);

  // Years Routes 
  apiRouter.route("/years/")      .get(yearsCtrl.getList);
  apiRouter.route("/years/")      .post(yearsCtrl.new);
  apiRouter.route("/years/search").get(yearsCtrl.search);
  apiRouter.route("/years/:id/")  .get(yearsCtrl.getOne);
  apiRouter.route("/years/:id/")  .put(yearsCtrl.update);
  apiRouter.route("/years/:id/")  .delete(yearsCtrl.delete);
  apiRouter.route("/years/:id/teams/")        .get(yearsCtrl.getTeams);
  apiRouter.route("/years/:yearId/teams/:id") .delete(teamsCtrl.delete);
  apiRouter.route("/years/:id/caussenards/")  .get(yearsCtrl.getParticipants);
  apiRouter.route("/years/:id/caussenards/:caussenardId").delete(yearsCtrl.deleteParticipant);
  apiRouter.route("/years/:id/caussenards/:caussenardId").put(yearsCtrl.updateParticipant);

  // Teams Routes
  apiRouter.route("/teams/")        .get(teamsCtrl.getList);
  apiRouter.route("/teams/")        .post(teamsCtrl.new);
  apiRouter.route("/teams/search")  .get(teamsCtrl.search);
  apiRouter.route("/teams/:id/")    .get(teamsCtrl.getOne);
  apiRouter.route("/teams/:id/")    .put(teamsCtrl.update);
  apiRouter.route("/teams/:id/")    .delete(teamsCtrl.delete);
  apiRouter.route("/teams/:id/caussenards")               .get(teamsCtrl.getMembers);
  apiRouter.route("/teams/:id/caussenards/:caussenardId") .delete(teamsCtrl.deleteMember);
  apiRouter.route("/teams/:id/caussenards/:caussenardId") .put(teamsCtrl.updateMember);
  
  // Caussenards Routes
  apiRouter.route("/caussenards/")      .get(caussenardsCtrl.getList);
  apiRouter.route("/caussenards/")      .post(caussenardsCtrl.new);
  apiRouter.route("/caussenards/search").get(caussenardsCtrl.search);
  apiRouter.route("/caussenards/:id/")  .get(caussenardsCtrl.getOne);
  apiRouter.route("/caussenards/:id/")  .put(caussenardsCtrl.update);
  apiRouter.route("/caussenards/:id/")  .delete(caussenardsCtrl.delete);
  apiRouter.route("/caussenards/:id/years/")        .get(caussenardsCtrl.getYears);
  apiRouter.route("/caussenards/:id/teams/")        .get(caussenardsCtrl.getTeams);
  apiRouter.route("/caussenards/:id/teams/:teamId") .delete(caussenardsCtrl.removeTeam);
  apiRouter.route("/caussenards/:id/teams/:teamId") .put(caussenardsCtrl.updateTeam);

  // Members Routes
  apiRouter.route("/members/")        .get(membersCtrl.getList);
  apiRouter.route("/members/")        .put(membersCtrl.update);
  apiRouter.route("/members/link/")   .post(membersCtrl.link);
  apiRouter.route("/members/unlink/") .delete(membersCtrl.unlink);

  // teamMakers Routes
  apiRouter.route("/teammakers/")   .get(teamMakersCtrl.getList);
  apiRouter.route("/teammakers/")   .post(teamMakersCtrl.new);
  apiRouter.route("/teammakers/:id").get(teamMakersCtrl.getOne);
  apiRouter.route("/teammakers/:id").put(teamMakersCtrl.update);
  apiRouter.route("/teammakers/:id").delete(teamMakersCtrl.delete);

  //Load & Download
  apiRouter.route("/upload")  .put(loadCtrl.upload);
  apiRouter.route("/download").get(loadCtrl.download);

  return apiRouter;
})();