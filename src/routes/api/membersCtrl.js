// Import 
var asyncLib  = require("async");
var Return = require("../../functions/returns");
var errno = require("../../functions/errno");
var teamdb = require("../../databaseRequests/teamsReq");
var memberdb = require("../../databaseRequests/membersReq");
var caussenarddb = require("../../databaseRequests/caussenardsReq");
var check  = require("../../functions/check");

/**
 * @module Member Controler
 * @description Routes about Members
 */
module.exports = {

  /**
   * @description To get the list of Members
   * @param {*} req request received
   * @param {*} res response to send
   */
  getList: function(req, res) {
    // Authorization
    var authLevel = 2;
    if (req.query.count) {
      authLevel = 0;
    }
    
    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Get members
      function (done) {
        if (req.query.count) {
          memberdb.nbOfMembers(res, done);
        } else {
          memberdb.getMembers(order, fields, limit, offset, res, done);
        }
      },
      // Return the response
      function (membersFound, done) {
        done(Return.success(res, 200, membersFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
   * @description To update informations about a Member
   * @param {*} req request received
   * @param {*} res response to send
   */
  update: function (req, res) {
    // Authorization
    const authLevel = 3;
    // Params
    var teamId  = req.body.teamId;
    var caussenardId   = req.body.caussenardId;
    var isLeader = req.body.isLeader;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check teamId
      function(done) {
        check.elementId(teamId, "teamId", res, done);
      },
      // Check caussenardId
      function(done) {
        check.elementId(caussenardId, "caussenardId", res, done);
      },
      // Check if the member exist
      function(done) {
        if (isLeader != undefined && isLeader != null && (!isLeader || isLeader)) {
          memberdb.findMemberWithIds(teamId, caussenardId, res, done);
        } else {
          done(Return.fail(res, 400, errno.not_exist, "Paramètre manquant", "isLeader is missing"));
        }
      },
      // Update member
      function(memberFound, done) {
        if (memberFound) {
          memberdb.update(memberFound, isLeader, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Member inexistante", 
            "Member with caussenardId " + caussenardId + " and teamId " + teamId + " not exist"));
        }
      },
      // Return the response
      function(memberUpdated, done) {
        done(Return.success(res, 200, memberUpdated));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },
  
  /**
   * @description To link a Caussenard to a Team
   * @param {*} req request received
   * @param {*} res response to send
   */
  link: function(req, res) {
    // Authorization
    var authLevel = 3;
    // Params
    var teamId  = req.body.teamId;
    var caussenardId   = req.body.caussenardId;
    var isLeader   = req.body.isLeader;
    var yearId;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check teamId
      function(done) {
        check.elementId(teamId, "teamId", res, done);
      },
      // Check caussenardId
      function(done) {
        check.elementId(caussenardId, "caussenardId", res, done);
      },
      // Check caussenardId
      function(done) {
        if (isLeader == null || isLeader == undefined) {
          isLeader = false;
        } else {
          isLeader = true;
        }
        done(null);
      },
      // Check if the team exist
      function(done) {
        teamdb.findTeamWithId(teamId, res, done);
      },
      // Check if the caussenard exist
      function(teamFound, done) {
        if (teamFound) {
          yearId = teamFound.yearId;
          caussenarddb.getCaussenardInfos(caussenardId, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Équipe inexistante", "Team with id " + teamId + " not exist"));
        }
      },
      // Check year
      function(caussenardFound, done) {
        if (caussenardFound) {
          var teamIdFound = null;
          caussenardFound.dataValues.teams.forEach(function(team) {
            if (team.dataValues.yearId == yearId) {
              teamIdFound = team.dataValues.id;
            }
          }); 
          if (teamIdFound == null) {
            done(null);
          } else {
            Return.fail(res, 409, errno.already_exist, "Le Caussenard participe déjà à cette année là", 
              "Caussenard with id " + caussenardId + " is already participating the year with id " + 
                      yearId + " in the team with id" + teamIdFound);
          }
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Caussenard inexistante", "Caussenard with id " + caussenardId + " not exist"));
        }
      },
      // Check if the link exist
      function(done) {
        memberdb.findMemberWithIds(teamId, caussenardId, res, done);
      },
      // Create the member
      function(memberFound, done) {
        if (!memberFound) {
          memberdb.createMember(teamId, caussenardId, isLeader, res, done);
        } else {
          done(Return.fail(res, 409, errno.already_exist, "Le Caussenard est déjà dans l'équipe", "Member with teamId " + teamId + 
                                                  " and caussenardId " + caussenardId + " already exist"));
        }
      },
      // Return the response
      function(newMember, done) {
        done(Return.success(res, 201, newMember));
      }
    ], 
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
      
  },

  /**
   * @description To unlink a Caussenard from a Team
   * @param {*} req request received
   * @param {*} res response to send
   */
  unlink: function(req, res) {
    // Authorization
    var authLevel = 3;
    // Params
    var teamId  = req.body.teamId;
    var caussenardId   = req.body.caussenardId;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check teamId
      function(done) {
        check.elementId(teamId, "teamId", res, done);
      },
      // Check caussenardId
      function(done) {
        check.elementId(caussenardId, "caussenardId", res, done);
      },
      // Check if the team exist
      function(done) {
        teamdb.getTeamInfos(teamId, res, done);
      },      
      // Check if the caussenard exist
      function(teamFound, done) {
        if (teamFound) {
          caussenarddb.getCaussenardInfos(caussenardId, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Équipe inexistante", "Team with id " + teamId + " not exist"));
        }
      },
      //Check if the link exist
      function(caussenardFound, done) {
        if (caussenardFound) {
          memberdb.findMemberWithIds(teamId, caussenardId, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Caussenard inexistant", "Caussenard with id " + caussenardId + " not exist"));
        }
      },
      // Delete member
      function(memberFound, done) {
        if (memberFound) {
          memberdb.deleteMember(teamId, caussenardId, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Le Caussenard n'est pas member de l'équipe", "Member with teamId " + teamId + 
                                                " and caussenardId " + caussenardId + " not exist"));
        }
      },
      // Return the response
      function(memberDeleted, done) {
        done(Return.success(res, 200, {teamId: teamId,
          caussenardId: caussenardId}));
      },
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  }
};