// Import 
const asyncLib  = require("async");
const Return = require("../../functions/returns");
const errno = require("../../functions/errno");
const yeardb = require("../../databaseRequests/yearsReq");
const teamdb = require("../../databaseRequests/teamsReq");
const memberdb = require("../../databaseRequests/membersReq");
const caussenarddb = require("../../databaseRequests/caussenardsReq");
const { unlink, update } = require("./membersCtrl");
const check  = require("../../functions/check");
const { Op } = require("sequelize");

/**
 * @module Caussenard Controler
 * @description Routes about Caussenards
 */
module.exports = {

  /**
     * @description To Create a new Caussenard
     * @param {*} req request received
     * @param {*} res response to send
     */
  new: function(req, res) {
    // Authorization
    const authLevel = 3;
    // Params
    var firstname = req.body.firstname;
    var lastname  = req.body.lastname;
    var birth     = req.body.birth;
    var gender     = req.body.gender;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check firstname
      function(done) {
        check.name(firstname, "firstname", res, done);
      },
      // Check lastname
      function(done) {
        check.name(lastname, "lastname", res, done);
      },
      // Check birth
      function(done) {
        if (birth) {
          check.birth(birth, res, done);
        } else {
          done(null);
        }
      },
      // Check gender
      function(done) {
        check.gender(gender, res, done);
      },
      // Find if caussenard exist
      function(done) {
        caussenarddb.findCaussenardWithName(firstname, lastname, res, done);
      },
      // Create caussenard
      function(caussenardFound, done) {
        if (!caussenardFound) {
          caussenarddb.createCaussenard(firstname, lastname, birth, gender, res, done);
        } else {
          done(Return.fail(res, 409, errno.already_exist, "Caussenard déjà existant", "caussenard with id " + 
                                                      caussenardFound.id + " already exist" ));
        }
      },
      // Return the response
      function(newCaussenard, done) {
        done(Return.success(res, 201, newCaussenard));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To get the list of Caussenards
     * @param {*} req request received
     * @param {*} res response to send
     */
  getList: function(req, res) {
    // Authorization
    var authLevel = 2;
    if (req.query.count) {
      authLevel = 0;
    }

    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Get caussenards
      function(done) {
        if (req.query.count) {
          caussenarddb.nbOfCaussenards(null, res, done);
        } else {
          caussenarddb.getCaussenards(order, fields, limit, offset, null, res, done);
        }
      },
      // Return the response
      function(caussenardsFound, done) {
        done(Return.success(res, 200, caussenardsFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To get informations of a Caussenard
     * @param {*} req request received
     * @param {*} res response to send
     */
  getOne: function (req, res) {
    // Authorization
    const authLevel = 2;

    // Params
    var caussenardId = req.params.id;
        
    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check caussenardId
      function(done) {
        check.elementId(caussenardId, "caussenardId", res, done);
      },
      // Get caussenard's informations
      function(done) {
        caussenarddb.getCaussenardInfos(caussenardId, res, done);
      },
      // Return the response
      function(caussenardFound, done) {
        if (caussenardFound) {
          done(Return.success(res, 200, caussenardFound));
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Caussenard inexistant", "caussenard with id: " + caussenardId + " not found"));
        }
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To search list of Caussenards from string
     * @param {*} req request received
     * @param {*} res response to send
     */
  search: function(req, res) {
    // Authorization
    var authLevel = 2;
    if (req.query.count) {
      authLevel = 0;
    }

    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;

    var searchText = req.query.search;
    var list = searchText.split(" ").filter(word => word.length > 1).map(w => "%" + w + "%");
    console.log(list);
    var likeList = new Array();
    list.forEach((elem) => {
      likeList.push({[Op.like]: elem});
    });
    console.log(likeList);
    

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Get caussenards
      function(done) {
        if (req.query.count) {
          caussenarddb.nbOfCaussenards({ [Op.and]: [{ firstname: { [Op.or]: likeList }},
            { lastname: { [Op.or]: likeList }}] },res, done);
        } else {
          caussenarddb.getCaussenards(order, fields, limit, offset, { [Op.and]: [
            { firstname: { [Op.or]: likeList }},
            { lastname: { [Op.or]: likeList }}] }, res, done);
        }
      },
      // Get caussenards
      function(caussenardsFound, done) {
        if (caussenardsFound && ( typeof caussenardsFound != "object" || caussenardsFound.length != 0)) {
          done(null, caussenardsFound);
        } else {
          if (req.query.count) {
            caussenarddb.nbOfCaussenards({ [Op.or]: [{ firstname: { [Op.or]: likeList }},
              { lastname: { [Op.or]: likeList }}] },res, done);
          } else {
            caussenarddb.getCaussenards(order, fields, limit, offset, { [Op.or]: [
              { firstname: { [Op.or]: likeList }},
              { lastname: { [Op.or]: likeList }}] }, res, done);
          }
        }
      },
      // Return the response
      function(caussenardsFound, done) {
        //console.log("Op.or : ", caussenardsFound);
        done(Return.success(res, 200, caussenardsFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },
  
  /**
     * @description To update informations about a Caussenard
     * @param {*} req request received
     * @param {*} res response to send
     */
  update: function (req, res) {
    // Authorization
    var authLevel = 3;
    // Params
    var caussenardId = req.params.id;
    var newFirstName = req.body.firstname;
    var newLastName  = req.body.lastname;
    var newBirth     = req.body.birth;
    var newGender     = req.body.gender;
    var caussenardToUpdate;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check caussenardId
      function(done) {
        check.elementId(caussenardId, "caussenardId", res, done);
      },
      // Check newFirstName
      function(done) {
        if (!newFirstName && !newLastName && !newBirth && !newGender) {
          done(Return.fail(res, 400, errno.miss_param, "Rien à améliorer", "nothing to update"));
        } else {
          done(null);
        }
      },
      // Check newFirstName
      function(done) {
        if (newFirstName) {
          check.name(newFirstName, "firstname", res, done);
        } else {
          done(null);
        }
      },
      // Check newLastName
      function(done) {
        if (newLastName) {
          check.name(newLastName, "lastname", res, done);
        } else {
          done(null);
        }
      },
      // Check newBirth
      function(done) {
        if (newBirth) {
          check.birth(newBirth, res, done);
        } else {
          done(null);
        }
      },
      // Check newGender
      function(done) {
        if (newGender) {
          check.gender(newGender, res, done);
        } else {
          done(null);
        }
      },
      // Check if the caussenard exist
      function(done) {
        caussenarddb.getCaussenardInfos(caussenardId, res, done);
      },
      // Find if the new Caussenard exist
      function(caussenardFound, done) {
        if (caussenardFound) {
          caussenardToUpdate = caussenardFound;
          caussenarddb.findCaussenardWithName(
            (newFirstName ? newFirstName : caussenardFound.firstname),
            (newLastName ? newLastName : caussenardFound.lastname), res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Caussenard inexistant", "caussenard with id " + caussenardId + " was not found"));
        }
      },
      // Update the caussenard
      function(caussenardFound, done) {
        if (!caussenardFound || caussenardFound.id == caussenardId) {
          caussenarddb.update(caussenardToUpdate, newFirstName, newLastName, newBirth, newGender, res, done);
        } else {
          done(Return.fail(res, 409, errno.already_exist, "Caussenard déjà existant", "caussenard already exist: id = " + caussenardFound.id));
        }
      },
      // Return the response
      function (caussenardUpdated, done) {
        done(Return.success(res, 200, caussenardUpdated));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To delete a Caussenard
     * @param {*} req request received
     * @param {*} res response to send
     */
  delete: function (req, res) {
    // Authorization
    const authLevel = 4;
    // Param
    var caussenardId = req.params.id;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check caussenardId
      function(done) {
        check.elementId(caussenardId, "caussenardId", res, done);
      },
      // Check if the caussenard exist
      function(done) {
        caussenarddb.getCaussenardInfos(caussenardId, res, done);
      },
      // Delete members relation of caussenards
      function(caussenardFound, done) {
        if (caussenardFound) {
          memberdb.deleteMembersFromCaussenard(caussenardId, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Caussenard inexistant", "caussenard with id " + caussenardId + " was not found"));
        }
      },
      // Delete the caussenard
      function(membersDeleted, done) {
        caussenarddb.delete(caussenardId, res, done);
      },
      // Return the response
      function (caussenardDeleted, done) {
        done(Return.success(res, 200, {id : parseInt(caussenardId)}));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To get a list of the years during which he participated
     * @param {*} req request received
     * @param {*} res response to send
     */
  getYears: function (req, res) {
    // Authorization
    const authLevel = 2;
    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;
    var caussenardId = req.params.id;

    // Process 
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check caussenardId
      function(done) {
        check.elementId(caussenardId, "caussenardId", res, done);
      },
      // Check if the caussenard exist and get full informations
      function(done) {
        caussenarddb.getCaussenardInfos(caussenardId, res, done);
      },
      // Translate into an array of yearId
      function (caussenardFound, done) {
        if (caussenardFound) {
          var yearsIdFound = [];
          caussenardFound.dataValues.teams.forEach(function(team) {
            yearsIdFound.push(team.dataValues.yearId);
          });
          done(null, yearsIdFound);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Caussenard inexistant", "caussenard with id " + caussenardId + " was not found"));
        }
      },
      // Find Years
      function(yearsIdFound, done) {
        yeardb.findYearsWithId(order, fields, limit, offset, yearsIdFound, res, done);
      },
      // Return the response
      function(yearsFound, done) {
        done(Return.success(res, 200, yearsFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To get the list of teams in which he has been a member
     * @param {*} req request received
     * @param {*} res response to send
     */
  getTeams: function (req, res) {
    // Authorization
    const authLevel = 2;
    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;
    var caussenardId = req.params.id;
    var linkRole = new Object();

    // Process 
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check caussenardId
      function(done) {
        check.elementId(caussenardId, "caussenardId", res, done);
      },
      // Check if the caussenard exist and get full informations
      function(done) {
        caussenarddb.getCaussenardInfos(caussenardId, res, done);
      },
      // Translate into an array of teamId
      function (caussenardFound, done) {
        if (caussenardFound) {
          var teamsIdFound = [];
          caussenardFound.dataValues.teams.forEach(function(team) {
            teamsIdFound.push(team.dataValues.id);
            linkRole[team.dataValues.id] = team.dataValues.role;
          }); 
          done(null, teamsIdFound);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Caussenard inexistant", "caussenard with id " + caussenardId + " was not found"));
        }
      },
      // Find teams
      function(teamsIdFound, done) {
        teamdb.findTeamsWithId(order, fields, limit, offset, teamsIdFound, res, done);
      },
      // Return the response
      function(teamsFound, done) {
        if (req.query.count) {
          done(Return.success(res, 200, teamsFound.length));
        } else {
          teamsFound.forEach(function(team) {
            team.dataValues["role"] = linkRole[team.dataValues.id];
          });
          done(Return.success(res, 200, teamsFound));
        }
      },
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To delete a a team
     * @param {*} req request received
     * @param {*} res response to send
     */
  removeTeam: function(req, res) {

    req.body.teamId = req.params.teamId;
    req.body.caussenardId = req.params.id;

    unlink(req, res);
  },

  /**
     * @description To update a team relation
     * @param {*} req request received
     * @param {*} res response to send
     */
  updateTeam: function(req, res) {

    req.body.teamId = req.params.teamId;
    req.body.caussenardId = req.params.id;

    update(req, res);
  }
};