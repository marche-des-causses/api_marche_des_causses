// Import 
var asyncLib  = require("async");
var Return = require("../../functions/returns");
var errno = require("../../functions/errno");
var yeardb = require("../../databaseRequests/yearsReq");
var teamdb = require("../../databaseRequests/teamsReq");
var memberdb = require("../../databaseRequests/membersReq");
var caussenarddb = require("../../databaseRequests/caussenardsReq");
const { unlink, update} = require("../../routes/api/membersCtrl");
var check  = require("../../functions/check");
const { Op } = require("sequelize");

/**
 * @module Team Controler
 * @description Routes about Teams
 */
module.exports = {

  /**
     * @description Creates a new Team
     * @param {*} req request received
     * @param {*} res response to send
     */
  new: function(req, res) {
    // Authorization
    const authLevel = 3;
    // Params
    var yearId  = req.body.yearId;
    var number  = req.body.number;
    var name    = req.body.name;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearId
      function(done) {
        check.elementId(yearId, "yearId", res, done);
      },
      // Check team number
      function(done) {
        check.teamNumber(number, res, done);
      },
      // Check name
      function(done) {
        if (name != undefined && name != null) {
          check.teamName(name, res, done);
        } else {
          done(null);
        }
      },
      // Check if the year exist
      function (done) {
        yeardb.findYearWithId(yearId, res, done);
      },
      // Check if the team doesn't exist
      function (yearFound, done) {
        if (yearFound) {
          teamdb.findTeamWithYearIdAndNumber(yearId, number, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Année inexistant", "year with id " + yearId + " was not found"));
        }
      },
      // Create the team
      function (teamFound, done) {
        if (!teamFound) {
          teamdb.createTeam(number, name, yearId, res, done);
        } else {
          done(Return.fail(res, 409, errno.already_exist, "Équipe déjà existante", "Team number " + number + 
                                                    "of the year with id " + yearId + " already exist"));
        }
      }, 
      // Return the response
      function (teamCreated, done) {
        done(Return.success(res, 201, teamCreated));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To get the list of Teams
     * @param {*} req request received
     * @param {*} res response to send
     */
  getList: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Find teams
      function(done) {
        if (req.query.count) {
          teamdb.nbOfTeams(null, res, done);
        } else {
          teamdb.getTeams(order, fields, limit, offset, null, res, done);
        }
      },
      // Return the response
      function(teamsFound, done) {
        done(Return.success(res, 200, teamsFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To get informations of a Team
     * @param {*} req request received
     * @param {*} res response to send
     */
  getOne : function(req, res) {
    // Authorization
    const authLevel = 0;        
    // Params
    var teamId      = parseInt(req.params.id);

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check teamId
      function(done) {
        check.elementId(teamId, "teamId", res, done);
      },
      // Get team's informations
      function(done) {
        teamdb.getTeamInfos(teamId, res, done);
      },
      // Return the response
      function(teamFound, done) {
        if (teamFound) {
          done(Return.success(res, 200, teamFound));
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Équipe inexistante", "team with id " + teamId + " was not found"));
        }
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To search list of Teams from string
     * @param {*} req request received
     * @param {*} res response to send
     */
  search: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;
    var searchText = "%" + req.query.search + "%";

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Find teams
      function(done) {
        if (req.query.count) {
          teamdb.nbOfTeams({ name : {[Op.like]: searchText} }, res, done);
        } else {
          teamdb.getTeams(order, fields, limit, offset, { name : {[Op.like]: searchText} }, res, done);
        }
      },
      // Return the response
      function(teamsFound, done) {
        done(Return.success(res, 200, teamsFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To update informations about a Team
     * @param {*} req request received
     * @param {*} res response to send
     */
  update: function (req, res) {
    // Authorization
    const authLevel = 3;
    // Params
    var teamId    = req.params.id;
    var newNumber = req.body.number;
    var newYearId = req.body.yearId;
    var newName   = req.body.name;
    var teamToUpdate;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check teamId
      function(done) {
        check.elementId(teamId, "teamId", res, done);
      },
      // Check yearId
      function(done) {
        if (newYearId != undefined && newYearId != null) {
          check.elementId(newYearId, "yearId", res, done);
        } else {
          done(null);
        }
      },
      // Check team number
      function(done) {
        if (newNumber != undefined && newNumber != null) {
          check.teamNumber(newNumber, res, done);
        } else {
          done(null);
        }
      },
      // Check team name
      function(done) {
        if (newName) {
          check.teamName(newName, res, done);
        } else {
          done(null);
        }
      },
      // check if team exist
      function (done) {
        teamdb.getTeamInfos(teamId, res, done);
      },
      // Save team
      function(teamFound, done) {
        if (teamFound) {
          teamToUpdate = teamFound;
          done(null);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Équipe inexistante", "team with id " + teamId + " was not found"));
        }
      },
      // Check if year exist
      function(done) {
        if (newYearId) {
          yeardb.findYearWithId(newYearId, res, done);
        } else {
          done(null, null);
        }
      },
      // check if the new team exist
      function(yearFound, done) {
        if ((yearFound || !newYearId) && (newYearId != null || newNumber != null)) {
          newYearId = (yearFound ? newYearId : teamToUpdate.yearId);
          teamdb.findTeamWithYearIdAndNumber( newYearId,
            (newNumber ? newNumber : teamToUpdate.number) , res, done);
        } else if (!yearFound && newYearId) {
          done(Return.fail(res, 404, errno.not_exist, "Année inexistant", "year with id " + newYearId + " was not found"));
        } else {
          done(null, null);
        }
      },
      // Update the team
      function(teamFound, done) {
        if (!teamFound) {
          teamdb.update(teamToUpdate, newNumber, newName, newYearId, res, done);
        } else {
          done(Return.fail(res, 409, errno.already_exist, "Équipe déjà existante",
            "The team number " + teamFound.number + 
            " of the year with id " + (newYearId ? newYearId : teamToUpdate.year.number)));
        }
      },
      // Return the response
      function(teamUpdated, done) {
        done(Return.success(res, 200, teamUpdated));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To delete a Team
     * @param {*} req request received
     * @param {*} res response to send
     */
  delete: function (req, res) {
    // Authorization
    const authLevel = 4;
    // Params
    var teamId = parseInt(req.params.id);

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check teamId
      function(done) {
        check.elementId(teamId, "teamId", res, done);
      },
      // Check if team exist
      function(done) {
        teamdb.findTeamWithId(teamId, res, done);
      },
      // Delete members of the team
      function(teamFound, done) {
        if (teamFound) {
          memberdb.deleteMembersFromTeam(teamId, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Équipe inexistante", "team with id " + teamId + " was not found"));
        }
      },
      // Delete team
      function(membersFound, done) {
        teamdb.delete(teamId, res, done);
      },
      // Return the response
      function(teamDeleted, done) {
        done(Return.success(res, 200, {id: teamId}));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To get the list of Team members
     * @param {*} req request received
     * @param {*} res response to send
     */
  getMembers: function (req, res) {
    // Authorization
    const authLevel = 2;
    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;
    var teamId  = req.params.id;
    var linkRole = new Object();

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check teamId
      function(done) {
        check.elementId(teamId, "teamId", res, done);
      },
      // Check if team exist
      function(done) {
        teamdb.getTeamInfos(teamId, res, done);
      },
      // Translate into a array of caussenardId
      function(teamFound, done) {
        if (teamFound) {
          var caussenardIdFound = [];
          teamFound.dataValues.members.forEach(function(caussenard) {
            caussenardIdFound.push(caussenard.dataValues.id);
            linkRole[caussenard.dataValues.id] = caussenard.dataValues.role;
          }); 
          done(null, caussenardIdFound);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Équipe inexistante", "team with id " + teamId + " was not found"));
        }
      },
      // Find caussenards
      function(caussenardsIdFound, done) {
        if (caussenardsIdFound.length > 0) {
          caussenarddb.findCaussenardsWithId(order, fields, limit, offset, caussenardsIdFound, res, done);
        } else {
          done(null, caussenardsIdFound);
        }
      },
      // Return the response
      function (caussenardsFound, done) {
        if (req.query.count) {
          done(Return.success(res, 200, caussenardsFound.length));
        } else {
          caussenardsFound.forEach(function(caussenard) {
            caussenard.dataValues["role"] = linkRole[caussenard.dataValues.id];
          });
          done(Return.success(res, 200, caussenardsFound));
        }
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To delete a member
     * @param {*} req request received
     * @param {*} res response to send
     */
  deleteMember: function(req, res) {

    req.body.teamId = req.params.id;
    req.body.caussenardId = req.params.caussenardId;

    unlink(req, res);
  },

  /**
     * @description To update a member relation
     * @param {*} req request received
     * @param {*} res response to send
     */
  updateMember: function(req, res) {

    req.body.teamId = req.params.id;
    req.body.caussenardId = req.params.caussenardId;

    update(req, res);
  }
  
};