// Import 
const asyncLib  = require("async");
const Return = require("../../functions/returns");
const errno = require("../../functions/errno");
const teamMakerdb = require("../../databaseRequests/teamMakersReq");
const check  = require("../../functions/check");
const teamMaker  = require("../../functions/teamMaker/TeamMaker");

/**
 * @module TeamMaker Controler
 * @description Routes about TeamMakers
 */
module.exports = {

  /**
     * @description Creates a new TeamMaker
     * @param {*} req request received
     * @param {*} res response to send
     */
  new: function(req, res) {
    // Authorization
    const authLevel = 3;
    // Params
    var params = req.body.params;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check params
      function(done) {
        if (params == undefined || params == null) {
          done(Return.fail(res, 404, errno.miss_param, "Paramètre manquant", "params is missing"));
        } else if (params.list == undefined || params.list == null) {
          done(Return.fail(res, 404, errno.miss_param, "Pas de participants", "params.list is undefined or null"));
        } else if (params.list.length < 2) {
          done(Return.fail(res, 400, errno.wrong_param, "Pas assez de participants", "params.list is too small"));
        } else if ( params.nbOfteamsList == undefined || 
                    params.nbOfteamsList == null || 
                    params.nbOfteamsList.length == 0) {
          done(Return.fail(res, 404, errno.miss_param, "Pas nombre d'équipe", "params.nbOfteamsList is undefined or null"));
        } else {
          done(null);
        }
      },
      // Create the teamMaker
      function (done) {
        teamMakerdb.createTeamMaker(params, res, done);
      }, 
      // Return the response
      function (teamMakerCreated, done) {
        done(Return.success(res, 201, teamMakerCreated));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
      new teamMaker();
    });
  },

  /**
     * @description To get the list of TeamMakers
     * @param {*} req request received
     * @param {*} res response to send
     */
  getList: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Find teamMakers
      function(done) {
        if (req.query.count) {
          teamMakerdb.nbOfTeamMakers(res, done);
        } else {
          teamMakerdb.getTeamMakers(order, fields, limit, offset, res, done);
        }
      },
      // Return the response
      function(teamMakersFound, done) {
        done(Return.success(res, 200, teamMakersFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To get informations of a TeamMaker
     * @param {*} req request received
     * @param {*} res response to send
     */
  getOne : function(req, res) {
    // Authorization
    const authLevel = 0;        
    // Params
    var teamMakerId   = parseInt(req.params.id);

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check teamMakerId
      function(done) {
        check.elementId(teamMakerId, "teamMakerId", res, done);
      },
      // Get teamMaker's informations
      function(done) {
        teamMakerdb.getTeamMakerInfos(teamMakerId, res, done);
      },
      // Return the response
      function(teamMakerFound, done) {
        if (teamMakerFound) {
          done(Return.success(res, 200, teamMakerFound));
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Process inexistant", "teamMaker with id " + teamMakerId + " was not found"));
        }
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To update informations about a TeamMaker
     * @param {*} req request received
     * @param {*} res response to send
     */
  update: function (req, res) {
    // Authorization
    const authLevel = 3;
    // Params
    var teamMakerId = req.params.id;
    var newStatus = req.body.status;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check teamMakerId
      function(done) {
        check.elementId(teamMakerId, "teamMakerId", res, done);
      },
      // Check teamMaker status
      function(done) {
        check.status(newStatus, res, done);
      },
      // check if teamMaker exist
      function (done) {
        teamMakerdb.getTeamMakerInfos(teamMakerId, res, done);
      },
      // Save teamMaker
      function(teamMakerFound, done) {
        if (teamMakerFound) {
          teamMakerdb.update(teamMakerFound, newStatus, null, null, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Équipe inexistante", "teamMaker with id " + teamMakerId + " was not found"));
        }
      },
      // Return the response
      function(teamMakerUpdated, done) {
        done(Return.success(res, 200, teamMakerUpdated));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To delete a TeamMaker
     * @param {*} req request received
     * @param {*} res response to send
     */
  delete: function (req, res) {
    // Authorization
    const authLevel = 4;
    // Params
    var teamMakerId = parseInt(req.params.id);

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check teamMakerId
      function(done) {
        check.elementId(teamMakerId, "teamMakerId", res, done);
      },
      // Check if teamMaker exist
      function(done) {
        teamMakerdb.getTeamMakerInfos(teamMakerId, res, done);
      },
      // Delete teamMaker
      function(teamMakerFound, done) {
        if (teamMakerFound) {
          teamMakerdb.delete(teamMakerId, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Process inexistant", "teamMaker with id " + teamMakerId + " was not found"));
        }
      },
      // Return the response
      function(teamMakerDeleted, done) {
        done(Return.success(res, 200, {id: teamMakerId}));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

};