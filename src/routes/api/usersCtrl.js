// Imports
var bcrypt    = require("bcrypt");
var jwtUtils  = require("../../utils/jwt.utils");
var check  = require("../../functions/check");
var asyncLib  = require("async");
var Return = require("../../functions/returns");
var errno = require("../../functions/errno");
var userdb = require("../../databaseRequests/usersReq");
const { Op } = require("sequelize");

/**
 * @module User Controler
 * @description Routes about Users
 */
module.exports = {

  /**
   * @description To get an API key
   * @param {*} req request received
   * @param {*} res response to send
   */
  getAPIKey: function(req, res) {
    Return.success(res, 200, {
      "token": jwtUtils.generateTokenForUser(null)
    }); 
  },

  /**
   * @description To Update Token 
   * @param {*} req request received
   * @param {*} res response to send
   */
  updateToken: function(req, res) {
    // Process
    asyncLib.waterfall([
      // Check token
      function(done) {
        var token = jwtUtils.getToken(req.headers["authorization"]);
        if (token) {
          done(token.userId, null);
        }
      },
      // get user
      function(userId, done) {
        userdb.getUserInfos(userId, res, done);
      },
      // Return the response
      function(userFound, done) {
        if(userFound) {
          done(Return.success(res, 200, {
            "user": userFound,
            "token": jwtUtils.generateTokenForUser(userFound)
          }));
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Utilisateur inexistant", "User with not exist"));
        }
      }
    ], 
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
   * @description To register a new User
   * @param {*} req request received
   * @param {*} res response to send
   */
  register: function(req, res) {

    // Params
    var email    = req.body.email;
    var password = req.body.password;

    // Process
    asyncLib.waterfall([
      // Check email
      function(done) {
        check.email(email, res, done);
      },
      // Check password
      function(done) {
        check.password(password, res, done);
      },
      // Check if user doesn't exist
      function(done) {
        userdb.findUserWithEmail(email, res, done);
      },
      // Hash the password
      function(userFound, done) {
        if (!userFound) {
          bcrypt.hash(password, 5, function( err, bcryptedPassword ) {
            if (err) {
              done(Return.fail(res, 500, errno.hash_fail, "Hash fail", err));
            } else {
              done(null, bcryptedPassword);
            }
          });
        } else {
          done(Return.fail(res, 409, errno.already_exist, "Utilisateur déjà existant", "User with id " + userFound.id + " already exist"));
        }
      },
      // Create User
      function(bcryptedPassword, done) {
        userdb.createUser(email, bcryptedPassword, 1, res, done);
      },
      // Return the response
      function(newUser, done) {
        done(Return.success(res, 201, newUser));
      }
    ], 
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
   * @description To login an existing User
   * @param {*} req 
   * @param {*} res 
   */
  login: function(req, res) {

    // Params
    var email    = req.body.email;
    var password = req.body.password;

    // Process
    asyncLib.waterfall([
      // Check email
      function(done) {
        check.email(email, res, done);
      },
      // Check password
      function(done) {
        check.password(password, res, done);
      },
      // Check if user exist
      function(done) {
        userdb.findUserWithEmail(email, res, done);
      },
      // Verify the password
      function(userFound, done) {
        if (userFound) {
          bcrypt.compare(password, userFound.password, function(errBycrypt, resBycrypt) {
            if (errBycrypt) {
              done(Return.fail(res, 500, errno.compare_fail, "Echec du décryptage du mot de passe", "Password compare fail"));
            } else {
              done(null, userFound, resBycrypt);
            }
          });
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Utilisateur inexistant", "User with email " + email + " not exist"));
        }
      },
      // Return the response
      function(userFound, resBycrypt, done) {
        if(resBycrypt) {
          done(Return.success(res, 200, {
            user: userFound,
            token: jwtUtils.generateTokenForUser(userFound)
          }));
        } else {
          done(Return.fail(res, 403, errno.fail, "Mot de passe invalide", "invalid password"));
        }
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
   * @description To create a new User
   * @param {*} req request received
   * @param {*} res response to send
   */
  new: function(req, res) {
    // Authorization
    var authLevelrequired = 5;
    // Params
    var email    = req.body.email;
    var password = req.body.password;
    var authLevel = req.body.authLevel;

    // Process
    asyncLib.waterfall([
      // Check email
      function(done) {
        check.authorization(authLevelrequired, req, res, done);
      },
      // Check email
      function(done) {
        check.email(email, res, done);
      },
      // Check password
      function(done) {
        check.password(password, res, done);
      },
      // Check authLevel
      function(done) {
        check.authLevel(authLevel, res, done);
      },
      // Check if user doesn't exist
      function(done) {
        userdb.findUserWithEmail(email, res, done);
      },
      // Hash the password
      function(userFound, done) {
        if (!userFound) {
          bcrypt.hash(password, 5, function( err, bcryptedPassword ) {
            if (err) {
              done(Return.fail(res, 500, errno.hash_fail, "Hash fail", err));
            } else {
              done(null, bcryptedPassword);
            }
          });
        } else {
          done(Return.fail(res, 409, errno.already_exist, "Utilisateur déjà existant", "User with id " + userFound.id + " already exist"));
        }
      },
      // Create User
      function(bcryptedPassword, done) {
        userdb.createUser(email, bcryptedPassword, authLevel, res, done);
      },
      // Return the response
      function(newUser, done) {
        done(Return.success(res, 201, newUser));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
   * @description To get the list of Users
   * @param {*} req request received
   * @param {*} res response to send
   */
  getList: function(req, res) {
    // Authorization
    const authLevel = 5;

    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Get caussenards
      function(done) {
        if (req.query.count) {
          userdb.nbOfUsers(null, res, done);
        } else {
          userdb.getUsers(order, fields, limit, offset, null, res, done);
        }
      },
      // Return the response
      function(usersFound, done) {
        done(Return.success(res, 200, usersFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
   * @description To get informations about a User
   * @param {*} req 
   * @param {*} res 
   */
  getOne: function(req, res) {
    // Authentification 
    var userId = req.params.id;
    var authLevel = 5;

    if (userId == "me")
      authLevel = 1;
    var token  = jwtUtils.checkAuthorization(authLevel, req, res, (v) => {return v;});
    if (!token)
      return 0;
    if (userId == "me") {
      userId = token.userId;
    } 

    // Process
    asyncLib.waterfall([
      // Check userId
      function(done) {
        check.elementId(userId, "userId", res, done);
      },
      // Check if user exist and get full informations
      function(done) {
        userdb.getUserInfos(userId, res, done);
      },
      // Return the response
      function(userFound, done) {
        if (userFound) {
          done(Return.success(res, 200, userFound));
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Utilisateur inexistant", "User with id " + userId + " not exist"));
        }
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
   * @description To search list of Users from string
   * @param {*} req request received
   * @param {*} res response to send
   */
  search: function(req, res) {
    // Authorization
    const authLevel = 5;

    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;

    var searchText = "%" + req.query.search + "%";

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Get caussenards
      function(done) {
        if (req.query.count) {
          userdb.nbOfUsers({ email : { [Op.like]: searchText }}, res, done);
        } else {
          userdb.getUsers(order, fields, limit, offset, { email : { [Op.like]: searchText }}, res, done);
        }
      },
      // Return the response
      function(usersFound, done) {
        done(Return.success(res, 200, usersFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
   * @description To update informations about a User
   * @param {*} req request received
   * @param {*} res response to send
   */
  updateMyProfile: function(req, res) {
    // Authentification 
    var authLevel = 1;
    var userId;
    
    var token  = jwtUtils.checkAuthorization(authLevel, req, res, (v) => {return v;});
    if (!token) {
      return 0;
    } else {
      userId = token.userId;
    }

    // Params
    var newEmail = req.body.email;
    var newPassword = req.body.newPassword;
    var lastPassword = req.body.lastPassword;

    // Process
    asyncLib.waterfall([
      // Check email
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check email
      function(done) {
        if (newEmail != undefined && newEmail != null) {
          check.email(newEmail, res, done);
        } else {
          newEmail = null;
          done(null);
        }
      },
      // Check password
      function(done) {
        if (newPassword != undefined && newPassword != null) {
          check.password(newPassword, res, done);
        } else {
          newPassword = null;
          done(null);
        }
      },
      // Check last password
      function(done) {
        if (newPassword != undefined && newPassword != null) {
          check.password(lastPassword, res, done);
        } else {
          lastPassword = null;
          done(null);
        }
      },
      // Check if email exist
      function(done) {
        userdb.findUserWithEmail(newEmail, res, done);
      },
      // Check if user exist
      function(userFound, done) {
        if(!userFound) {
          userdb.findUserWithId(userId, res, done);
        } else {
          done(Return.fail(res, 409, errno.already_exist, "Email déjà utilisé", "Email " + newEmail + " already used"));
        }
      },
      // Verify last password
      function(userFound, done) {
        if (userFound) {
          if (newPassword != null) {
            bcrypt.compare(lastPassword, userFound.password, function(errBycrypt, resBycrypt) {
              if (errBycrypt) {
                done(Return.fail(res, 500, errno.compare_fail, "Echec du décryptage du mot de passe", "Password compare fail"));
              } else {
                done(null, userFound, resBycrypt);
              }
            });
          } else {
            done(null, userFound, null);
          }
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Utilisateur inexistant", "User with id " + userId + " not exist"));
        }
      },
      // Hash the password
      function(userFound, resBycrypt, done) {
        if (newPassword != null) {
          if (resBycrypt) {
            bcrypt.hash(newPassword, 5, function( err, bcryptedPassword ) {
              if (err) {
                done(Return.fail(res, 500, errno.hash_fail, "Hash fail", err));
              } else {
                done(null, userFound, bcryptedPassword);
              }
            });
          } else {
            done(Return.fail(res, 403, errno.fail, "Mot de passe invalide", "invalid password"));
          }
        } else {
          done(null, userFound, null);
        }
      },
      // Update user
      function(userFound, bcryptedPassword, done) {
        userdb.updateUser(userFound, newEmail, bcryptedPassword, null, res, done);
      },
      // Return the response
      function(userFound, done) {
        done(Return.success(res, 200, userFound));
      }
    ], 
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
   * @description To update informations about a User
   * @param {*} req request received
   * @param {*} res response to send
   */
  update: function(req, res) {
    // Authentification 
    var authLevel = 5;

    // Params
    var userId = req.params.id;
    if (userId == "me") {
      module.exports.updateMyProfile(req, res);
      return 0;
    }
    var newEmail = req.body.email;
    var newPassword = req.body.password;
    var newAuthLevel = req.body.authLevel;

    // Process
    asyncLib.waterfall([
      // Check email
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check email
      function(done) {
        if (newEmail != undefined && newEmail != null) {
          check.email(newEmail, res, done);
        } else {
          done(null);
        }
      },
      // Check password
      function(done) {
        if (newPassword != undefined && newPassword != null) {
          check.password(newPassword, res, done);
        } else {
          done(null);
        }
      },
      // Check authLevel
      function(done) {
        if (newAuthLevel != undefined && newAuthLevel != null) {
          check.authLevel(newAuthLevel, res, done);
        } else {
          done(null);
        }
      },
      // Check if email exist
      function(done) {
        if (newEmail) {
          userdb.findUserWithEmail(newEmail, res, done);
        } else {
          done(null, null);
        }
      },
      // Check if user exist
      function(userFound, done) {
        if(!userFound || userFound.id == userId) {
          userdb.findUserWithId(userId, res, done);
        } else {
          done(Return.fail(res, 409, errno.already_exist, "Email déjà utilisé", "Email " + newEmail + " already used"));
        }
      },
      // Hash the password
      function(userFound, done) {
        if (userFound) {
          if (newPassword != undefined && newPassword != null) {
            bcrypt.hash(newPassword, 5, function( err, bcryptedPassword ) {
              if (err) {
                done(Return.fail(res, 500, errno.hash_fail, "Hash fail", err));
              } else {
                done(null, userFound, bcryptedPassword);
              }
            });
          } else {
            done(null, userFound, null);
          }
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Utilisateur inexistant", "User with id " + userId + " not exist"));
        }
      },
      // Update user
      function(userFound, bcryptedPassword, done) {
        userdb.updateUser(userFound, newEmail, bcryptedPassword, newAuthLevel, res, done);
      },
      // Return the response
      function(userFound, done) {
        done(Return.success(res, 200, userFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
   * @description To update informations about a User
   * @param {*} req request received
   * @param {*} res response to send
   */
  delete: function(req, res) {
    // Authentification 
    var userId = req.params.id;
    var authLevel = 5;

    if (userId == "me")
      authLevel = 1;
    var token  = jwtUtils.checkAuthorization(authLevel, req, res, (v) => {return v;});
    if (!token)
      return 0;
    if (userId == "me") {
      userId = token.userId;
    }
    
    // Process
    asyncLib.waterfall([
      // Check userId
      function(done) {
        check.elementId(userId, "userId", res, done);
      },
      // Check if user exist
      function(done) {
        userdb.findUserWithId(userId, res, done);
      },
      // Update user
      function(userFound, done) {
        if(userFound) {
          userdb.deleteUser(userFound.id, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Utilisateur inexistant", "User with id " + userId + " not exist"));
        }
      },
      // Return the response
      function(userFound, done) {
        done(Return.success(res, 200, {id: parseInt(userId)}));
      }
    ], 
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  }

};