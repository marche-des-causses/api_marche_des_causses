// Import 
var asyncLib  = require("async");
var Return = require("../../functions/returns");
var errno = require("../../functions/errno");
var yeardb = require("../../databaseRequests/yearsReq");
var teamdb = require("../../databaseRequests/teamsReq");
var memberdb = require("../../databaseRequests/membersReq");
var caussenarddb = require("../../databaseRequests/caussenardsReq");
var check  = require("../../functions/check");
const { unlink, update } = require("./membersCtrl");
const { Op } = require("sequelize");

/**
 * @module Year Controler
 * @description Routes about Years
 */
module.exports = {

  /**
     * @description Creates a new Year
     * @param {*} req request received
     * @param {*} res response to send
     */
  new: function(req, res) {
    // Authorization
    const authLevel = 3;
    // Params
    var number  = req.body.number;
    var theme   = req.body.theme;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check year number
      function(done) {
        check.year(number, res, done);
      },
      // Check theme
      function(done) {
        if (theme) {
          check.theme(theme, res, done);
        } else {
          done(null);
        }
      },
      // Check if the year exist
      function (done) {
        yeardb.findYearWithNumber(number, res, done);
      },
      // Create the new year
      function (yearFound, done) {
        if (!yearFound) {
          yeardb.createYear(number, theme, res, done);
        } else {
          done(Return.fail(res, 409, errno.already_exist, "Année déjà existante", "year already exist: id = " + yearFound.id));
        }
      },
      // Return the response
      function(newYear, done) {
        done(Return.success(res, 201, newYear));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To get the list of Years
     * @param {*} req request received
     * @param {*} res response to send
     */
  getList: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Get years
      function (done) {
        if (req.query.count) {
          yeardb.nbOfYears(null, res, done);
        } else {
          yeardb.getYears(order, fields, limit, offset, null, res, done);
        }
      },
      // Return the response
      function (yearsFound, done) {
        done(Return.success(res, 200, yearsFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To get informations of a Year
     * @param {*} req request received
     * @param {*} res response to send
     */
  getOne: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var yearId = parseInt(req.params.id);
     
    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearId
      function(done) {
        check.elementId(yearId, "yearId", res, done);
      },
      // Get year's informations
      function (done) {
        yeardb.getYearInfos(yearId, res, done);
      },
      // Return the response
      function (yearFound, done) {
        if (yearFound) {
          done(Return.success(res, 200, yearFound));
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Année inexistant", "year with id " + yearId + " was not found"));
        }
      }
    ], 
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To search list of Years from string
     * @param {*} req request received
     * @param {*} res response to send
     */
  search: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;

    var searchText = "%" + req.query.search + "%";

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Get years
      function (done) {
        if (req.query.count) {
          yeardb.nbOfYears({ [Op.or]: [{ number: { [Op.like]: searchText }},
            { theme: { [Op.like]: searchText}}]}, res, done);
        } else {
          yeardb.getYears(order, fields, limit, offset, { [Op.or]: [{ number: { [Op.like]: searchText }},
            { theme: { [Op.like]: searchText}}]}, res, done);
        }
      },
      // Return the response
      function (yearsFound, done) {
        done(Return.success(res, 200, yearsFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },
  
  /**
     * @description To update informations about a Year
     * @param {*} req request received
     * @param {*} res response to send
     */
  update: function (req, res) { 
    // Authorization
    const authLevel = 3;
    // Params
    var yearId      = req.params.id;
    var newNumber   = req.body.number;
    var newTheme    = req.body.theme;
        
    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearId
      function(done) {
        check.elementId(yearId, "yearId", res, done);
      },
      // Check year number
      function(done) {
        if (newNumber) {
          check.year(newNumber, res, done);
        } else {
          done(null);
        }
      },
      // Check theme
      function(done) {
        if (newTheme) {
          check.theme(newTheme, res, done);
        } else {
          done(null);
        }
      },
      // Check if the new number doesn't exist
      function (done) {
        if (newNumber != null) {
          yeardb.findYearWithNumber(newNumber, res, done);
        } else {
          done(null, false);
        }
      },
      // Check if the year exist
      function (yearFound, done) {
        if (!yearFound || yearFound.id == yearId) {
          yeardb.findYearWithId(yearId, res, done);
        } else {
          done(Return.fail(res, 409, errno.already_exist, "Année déjà existante", "year already exist: id = " + yearId.id));
        }
      },
      // Update the year
      function update(yearFound, done) {
        if (yearFound) {
          yeardb.update(yearFound, newNumber, newTheme, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Année inexistant", "year with id " + yearId + " was not found"));
        }
      },
      // Return the response
      function(yearFound, done) {
        done(Return.success(res, 200, yearFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To delete a Year
     * @param {*} req request received
     * @param {*} res response to send
     */
  delete: function (req, res) {
    // Authorization
    const authLevel = 4;
    // Params
    var yearId = parseInt(req.params.id);

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearId
      function(done) {
        check.elementId(yearId, "yearId", res, done);
      },
      // Check if the year exist and get full informations
      function (done) {
        yeardb.getYearInfos(yearId, res, done);
      },
      // Translate into a array of teamId
      function (yearFound, done) {
        if (yearFound) {
          var teamsIdFound = [];
          yearFound.dataValues.teams.forEach(function(team) {
            teamsIdFound.push(team.dataValues.id);
          }); 
          done(null, teamsIdFound);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Année inexistant", "year with id " + yearId + " was not found"));
        }
      },
      // Delete members relation of teams
      function (teamsIdFound, done) {
        if (teamsIdFound && teamsIdFound.length) {
          memberdb.deleteMembersFromTeam(teamsIdFound, res, done);
        } else {
          done(null, null);
        }
      },
      // Delete teams of the year
      function (membersDeleted, done) {
        teamdb.deleteTeamsFromYearId(yearId, res, done);
      },
      // Delete year
      function (teamsDeleted, done) {
        yeardb.delete(yearId, res, done);
      },
      // Return the response
      function (done) {
        done(Return.success(res, 200, {id: yearId}));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To get the list of created Teams during a Year
     * @param {*} req request received
     * @param {*} res response to send
     */
  getTeams: function (req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;
    var yearId  = req.params.id;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearId
      function(done) {
        check.elementId(yearId, "yearId", res, done);
      },
      // Check if the year exist and get full informations
      function (done) {
        yeardb.getYearInfos(yearId, res, done);
      },
      // Translate into an array of teamId
      function (yearFound, done) {
        if (yearFound) {
          var teamsIdFound = [];
          yearFound.dataValues.teams.forEach(function(team) {
            teamsIdFound.push(team.dataValues.id);
          }); 
          if (req.query.count) {
            done(teamsIdFound.length);
          } else {
            done(null, teamsIdFound);
          }
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Année inexistant", "year with id " + yearId + " was not found"));
        }
      },
      // Find caussenards with Id
      function(teamsIdFound, done) {
        if (teamsIdFound.length > 0) {
          teamdb.findTeamsWithId(order, fields, limit, offset, teamsIdFound, res, done);
        } else {
          done(null, teamsIdFound);
        }
      },
      function(teamsFound, done) {
        done(Return.success(res, 200, teamsFound));
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To get the list of participants of a Year
     * @param {*} req request received
     * @param {*} res response to send
     */
  getParticipants: function (req, res) {
    // Authorization
    const authLevel = 2;
    // Params
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;
    var yearId  = req.params.id;
    var linkRole = new Object();
        
    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearId
      function(done) {
        check.elementId(yearId, "yearId", res, done);
      },
      // Check if the year exist and get full informations
      function (done) {
        yeardb.getYearInfos(yearId, res, done);
      },
      // Translate into an array of teamId
      function (yearFound, done) {
        if (yearFound) {
          var caussenardsIdFound = [];
          yearFound.dataValues.teams.forEach(function(team) {
            team.dataValues.members.forEach(function (caussenard) {
              caussenardsIdFound.push(caussenard.dataValues.id);
              linkRole[caussenard.dataValues.id] = caussenard.dataValues.role;
            });
          }); 
          done(null, caussenardsIdFound);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Année inexistant", "year with id " + yearId + " was not found"));
        }
      },
      // Find caussenards with Id
      function(caussenardsIdFound, done) {
        if (caussenardsIdFound.length > 0) {
          caussenarddb.findCaussenardsWithId(order, fields, limit, offset, caussenardsIdFound, res, done);
        } else {
          done(null, caussenardsIdFound);
        }
      },
      // Return the response
      function(caussenardsFound, done) {
        if (req.query.count) {
          done(Return.success(res, 200, caussenardsFound.length));
        } else {
          caussenardsFound.forEach(function(caussenard) {
            caussenard.dataValues["role"] = linkRole[caussenard.dataValues.id];
          });
          done(Return.success(res, 200, caussenardsFound));
        }
      }
    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

  /**
     * @description To remove one participant from a Year
     * @param {*} req request received
     * @param {*} res response to send
     */
  deleteParticipant: function (req, res) {
    // Authorization
    var authLevel = 3;
    // Params
    var yearId = req.params.id;
    var caussenardId = req.params.caussenardId;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check caussenardId
      function(done) {
        check.elementId(caussenardId, "caussenardId", res, done);
      },
      // Check yearId
      function(done) {
        check.elementId(yearId, "yearId", res, done);
      },
      // Check if the year exist
      function(done) {
        yeardb.getYearInfos(yearId, res, done);
      },
      // Check if the caussenard exist
      function(yearFound, done) {
        if (yearFound) {
          caussenarddb.getCaussenardInfos(caussenardId, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Année inexistante", "year with id " + yearId + " not exist"));
        }
      },
      // Find the team
      function(caussenardFound, done) {
        if (caussenardFound) {
          var teamIdFound = null;
          caussenardFound.dataValues.teams.forEach(function(team) {
            if (team.dataValues.yearId == yearId) {
              teamIdFound = team.dataValues.id;
            }
          }); 
          done(teamIdFound, null);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Caussenard inexistant", "caussenard with id " + caussenardId + " not exist"));
        }
      },
      // Find the team
      function(teamIdFound, done) {
        if (teamIdFound) {
          req.body.teamId = teamIdFound;
          req.body.caussenardId = caussenardId;
          done(true);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Le Caussenard n'a pas participé cette année",
            "year with id " + yearId + " has not the caussenard with id " + caussenardId));
        }
      },
    ], 
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      } else {
        return unlink(req, res);
      }
    });
  },

  /**
     * @description To update a participant
     * @param {*} req request received
     * @param {*} res response to send
     */
  updateParticipant: function (req, res) {
    // Authorization
    var authLevel = 3;
    // Params
    var yearId = req.params.id;
    var caussenardId = req.params.caussenardId;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check caussenardId
      function(done) {
        check.elementId(caussenardId, "caussenardId", res, done);
      },
      // Check yearId
      function(done) {
        check.elementId(yearId, "yearId", res, done);
      },
      // Check if the year exist
      function(done) {
        yeardb.getYearInfos(yearId, res, done);
      },
      // Check if the caussenard exist
      function(yearFound, done) {
        if (yearFound) {
          caussenarddb.getCaussenardInfos(caussenardId, res, done);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Année inexistante", "year with id " + yearId + " not exist"));
        }
      },
      // Find the team
      function(caussenardFound, done) {
        if (caussenardFound) {
          var teamIdFound = null;
          caussenardFound.dataValues.teams.forEach(function(team) {
            if (team.dataValues.yearId == yearId) {
              teamIdFound = team.dataValues.id;
            }
          });
          done(null, teamIdFound);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Caussenard inexistant", "caussenard with id " + caussenardId + " not exist"));
        }
      },
      // Find the team
      function(teamIdFound, done) {
        if (teamIdFound != null) {
          req.body.teamId = teamIdFound;
          req.body.caussenardId = caussenardId;
          done(true);
        } else {
          done(Return.fail(res, 404, errno.not_exist, "Le Caussenard n'a pas participé cette année",
            "year with id " + yearId + " has not the caussenard with id " + caussenardId));
        }
      },
    ], 
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      } else {
        return update(req, res);
      }
    });
  }


  
};