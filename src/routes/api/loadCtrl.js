// Import 
var asyncLib  = require("async");
var Return = require("../../functions/returns");
var errno = require("../../functions/errno");
var models  = require("../../models");
var yeardb = require("../../databaseRequests/yearsReq");
var teamdb = require("../../databaseRequests/teamsReq");
var caussenarddb = require("../../databaseRequests/caussenardsReq");
var memberdb = require("../../databaseRequests/membersReq");
var check  = require("../../functions/check");
const { getMembers } = require("../../databaseRequests/membersReq");

/**
 * @module Member Controler
 * @description Routes about Members
 */
module.exports = {

  /**
   * @description To get the list of Members
   * @param {*} req request received
   * @param {*} res response to send
   */
  upload: function(req, res) {
    
    // Authorization
    const authLevel = 3;

    // Params
    var dataString = req.body.csv;
    
    var results = {
      added : [],
      ignored : []
    };

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },

      // Check data
      function(done) {
        if ( dataString != null ) {
          done(null, dataString.split("\r\n"));
        } else {
          done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", "Le contenue du fichier est manquant"));
        }
      },

      function(data, done) {
        var iterator = data[Symbol.iterator]();

        var titles = iterator.next().value.split(";");
        console.log(titles);
        var map = new Map();
        map.set("Prénom",       titles.indexOf("Prénom"));
        map.set("Nom",          titles.indexOf("Nom"));
        map.set("Genre",        titles.indexOf("Genre"));
        map.set("Naissance",    titles.indexOf("Naissance"));
        map.set("N° équipe",    titles.indexOf("N° équipe"));
        map.set("Nom équipe",   titles.indexOf("Nom équipe"));
        map.set("Année",        titles.indexOf("Année"));
        map.set("Thème",        titles.indexOf("Thème"));
        map.set("Binôme",       titles.indexOf("Binôme"));

        let it = map.entries();
        let bool = false;
        for (var e of it) {
          if (e[1] == -1) {
            bool = true;
            done(Return.fail(res, 400, errno.miss_param, "Colonne manquante", "La colonne " + e[0] + " est manquante"));
            break;
          }
        }
        if (!bool) {
          addToBDD(iterator, map, results, res, done);
        }
      }

    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });


  },

  /**
   * @description To download database in a format passed in param
   * @param {*} req request received
   * @param {*} res response to send
   */
  download: function(req, res) {

    // Authorization
    const authLevel = 3;

    var list = new Array();
    var titre = new Array();
    titre.push("Prénom");
    titre.push("Nom");
    titre.push("Genre");
    titre.push("Naissance");
    titre.push("N° équipe");
    titre.push("Nom équipe");
    titre.push("Binôme");
    titre.push("Année");
    titre.push("Thème");
    list.push(titre);
    
    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Get all link between 
      function(done) {
        getMembers(null, null, null, null, res, done);
      },

      function(membersFound, done) {
        var iterator = membersFound[Symbol.iterator]();
        setLine(iterator, list, done);
      },

      function(done) {
        // let csvContent = "data:text/csv;charset=utf-8," 
        //   + list.map(e => e.join(";")).join("\n");
        let data = list.map(e => e.join(";")).join("\r\n");
        done(Return.success(res, 200, data));
      },

    ],
    // Check if response has been send
    function(response) {
      if (!response) {
        Return.fail(res, 500, errno.unkown, "Erreur Interne", "Unknow error");
      }
    });
  },

};


function setLine (iterator, list, done) {
  
  let member = iterator.next().value;
  
  if (member == null) {
    return done(null);
  } else {
    let map = new Array();
    models.Caussenard.findOne({
      where: {id: member.caussenardId},
      attributes: models.Caussenard.classicFields,
    }).then(function(caussenard) {
      map.push(caussenard.firstname); // Prénom
      map.push(caussenard.lastname);  // Nom
      map.push((caussenard.gender == 1) ? "H" : "F"); // Genre
      map.push(caussenard.birth);     // Naissance
      models.Team.findOne({
        where: {id: member.teamId},
        attributes: models.Team.classicFields
      }).then(function(team) {
        map.push(team.number);  // Numéro d'équipe
        map.push(team.name);    // Nom équipe
        map.push((member.isLeader) ? "oui" : "non"); // Binôme
        models.Year.findOne({
          where: {id: team.yearId},
          attributes: models.Year.classicFields
        }).then(function(year) {
          map.push(year.number);  // Année
          map.push(year.theme);   // Thème
          list.push(map);
          return setLine(iterator, list, done);
        });
      });
    });
  }
}


function addToBDD (iterator, map, results, res, doneMain) {

  let nextElem = iterator.next();
  if (nextElem.value == null ) {
    return doneMain(Return.success(res, 200, {results: results}));
  }

  var dataLine = nextElem.value.split(";");

  // saved data
  var caussenardSave  = null;
  var yearSave  = null;
  var teamSave  = null;

  if (dataLine[map.get("Prénom")].trim() == "" 
    || dataLine[map.get("Nom")].trim() == "" 
    || dataLine[map.get("Genre")].trim() == "" 
    || dataLine[map.get("Année")].trim() == "" 
    || dataLine[map.get("N° équipe")].trim() == "") 
  {
    // infos manquantes 
    results.ignored.push({
      data: nextElem.value,
      reason: "infos manquantes"
    });
    return addToBDD(iterator, map, results, res, doneMain);
  }

  // Process
  asyncLib.waterfall([

    // Find Caussenard
    function(done) {
      caussenarddb.findCaussenardWithName(dataLine[map.get("Prénom")].trim(), dataLine[map.get("Nom")].trim(), res, done);
    },
    // Create Caussenard if necesary 
    function(caussenard, done) {
      if (!caussenard) {
        let gender = (dataLine[map.get("Genre")].trim() == "H") ? 1 : 2;
        caussenarddb.createCaussenard((dataLine[map.get("Prénom")].trim() != "") ? dataLine[map.get("Prénom")].trim() : null, 
          dataLine[map.get("Nom")].trim(), 
          (dataLine[map.get("Naissance")].trim() != "") ? dataLine[map.get("Naissance")].trim() : null, 
          gender, res, done);
      } else {
        let firstname = (dataLine[map.get("Prénom")].trim() != "") ? dataLine[map.get("Prénom")].trim() : null;
        let lastname = (dataLine[map.get("Nom")].trim() != "") ? dataLine[map.get("Nom")].trim() : null;
        let birth = (dataLine[map.get("Naissance")].trim() != "") ? dataLine[map.get("Naissance")].trim() : null;
        let gender = (dataLine[map.get("Genre")].trim() != "") ? null : ((dataLine[map.get("Genre")].trim() == "H") ? 1 : 2);
        caussenarddb.update(caussenard, firstname, lastname, birth, gender, res, done);
      }
    },
    // Find Year
    function(caussenard, done) {
      if (caussenard) {
        caussenardSave = caussenard;
        yeardb.findYearWithNumber(dataLine[map.get("Année")].trim(), res, done);
      } else {
        // creation ou update du caussenard impossible
        results.ignored.push({
          data: nextElem.value,
          reason: "Creation ou update du caussenard impossible"
        });
        done(0);
      }
    },
    // Create Year if necesary 
    function(year, done) {
      if (!year) {
        yeardb.createYear(dataLine[map.get("Année")].trim(),
          dataLine[map.get("Thème")].trim(), res, done);
      } else {
        let number = (dataLine[map.get("Année")].trim() == "") ? dataLine[map.get("Année")].trim() : null;
        let theme = (dataLine[map.get("Thème")].trim() == "") ? dataLine[map.get("Thème")].trim() : null;
        yeardb.update(year, number, theme, res, done);
      }
    },
    // Find Team
    function(year, done) {
      if (year) {
        yearSave = year;
        teamdb.findTeamWithYearIdAndNumber(year.id, dataLine[map.get("N° équipe")].trim(), res, done);
      } else {
        // creation ou update de l'année impossible
        results.ignored.push({
          data: nextElem.value,
          reason: "Creation ou update de l'Année impossible"
        });
        done(0);
      }
    },
    // Create Team if necesary 
    function(team, done) {
      if (!team) {
        let name = (dataLine[map.get("Nom équipe")].trim() == "" ) ? "Équipe " + dataLine[map.get("N° équipe")].trim() : dataLine[map.get("Nom équipe")].trim() ;
        teamdb.createTeam(dataLine[map.get("N° équipe")].trim(), name, yearSave.id, res, done);
      } else {
        let number = (dataLine[map.get("N° équipe")].trim() == "") ? dataLine[map.get("N° équipe")].trim() : null;
        let name = (dataLine[map.get("Nom équipe")].trim() == "") ? dataLine[map.get("Nom équipe")].trim() : null;
        teamdb.update(team, number, name, null, res, done);
      }
    },
    // Find Member
    function(team, done) {
      if (team) {
        teamSave = team;
        memberdb.findMemberWithIds(teamSave.id, caussenardSave.id, res, done);
      } else {
        // Création ou update de l'équipe impossible
        results.ignored.push({
          data: nextElem.value,
          reason: "Creation ou update de l'équipe impossible"
        });
        done(0);
      }
    },
    // Get Caussenard Team participation
    function(member, done) {
      if (!member) {
        caussenarddb.getCaussenardInfos(caussenardSave.id, res, done);
      } else {
        // Déjà existant
        results.ignored.push({
          data: nextElem.value,
          reason: "Déjà existant"
        });
        done(null, null);
      }
    },
    // Check year participation
    function (caussenardFound, done) {
      if (caussenardFound) {
        var teamIdFound = null;
        caussenardFound.dataValues.teams.forEach(function(team) {
          if (team.dataValues.yearId == yearSave.id) {
            teamIdFound = team.dataValues.id;
          }
        }); 
        if (teamIdFound == null) {
          memberdb.createMember(teamSave.id, caussenardSave.id, 
            (dataLine[map.get("Binôme")].trim() != "") ? ((dataLine[map.get("Binôme")].trim() == "oui") ? true : false) : false, res, done);
        } else {
          // Participation existante cette année là 
          results.ignored.push({
            data: nextElem.value,
            reason: "Participation existante cette année là "
          });
          done(null, null);
        }
      } else {
        results.ignored.push({
          data: nextElem.value,
          reason: "Caussenard Introuvable"
        });
        done(null, null);
      }
    },
    function (member, done) {
      if (member) {
        // Ajout réussi
        results.added.push({
          data: nextElem.value,
          reason: "Ajout réussi"
        });
        done();
      } else {
        // Echec
        results.ignored.push({
          data: nextElem.value,
          reason: ""
        });
        done();
      }
    }
  ],
  // Check if response has been send
  function(response) {
    if (!response) {
      addToBDD(iterator, map, results, res, doneMain);
    } else {
      doneMain(response);
    }
  });

}

