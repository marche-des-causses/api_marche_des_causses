// Import 
var asyncLib  = require("async");
var Return = require("../../functions/returns");
var statsUtils = require("../../utils/stats.utils");
var statsdb = require("../../databaseRequests/stats");
var check  = require("../../functions/check");


module.exports = {

  getGender: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var yearId = parseInt(req.params.id);
     
    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearId
      function(done) {
        check.elementId(yearId, "yearId", res, done);
      },
      function (done) {
        statsdb.caussenardGenderOfYear(yearId, res)
          .then((response) => {
            done(statsUtils.genderStats(response));
          });
      },
    ], 
    // Return the response
    function(stats) {
      Return.success(res, 200, stats);
    });

  },

  getAge: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var yearId  = req.params.id;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearId
      function(done) {
        check.elementId(yearId, "yearId", res, done);
      },
      function (done) {
        statsdb.caussenardAgeOfYear(yearId, res)
          .then((response) => {
            done(statsUtils.ageStats(response));
          });
      }
    ],
    // Return the response
    function(caussenardsFound) {
      Return.success(res, 200, caussenardsFound);
    });
  },

};