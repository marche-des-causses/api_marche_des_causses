// Import 
var asyncLib  = require("async");
var Return = require("../../functions/returns");
var errno = require("../../functions/errno");
var statsUtils = require("../../utils/stats.utils");
var teamdb = require("../../databaseRequests/teamsReq");
var caussenarddb = require("../../databaseRequests/caussenardsReq");
var yeardb = require("../../databaseRequests/yearsReq");
var check  = require("../../functions/check");


module.exports = {

  getGender: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var teamId = parseInt(req.params.id);
    var nbOfCaussenards = 0;
     
    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearId
      function(done) {
        check.elementId(teamId, "teamId", res, done);
      },
      // Check if the year exist and get full informations
      function (done) {
        teamdb.getTeamInfos(teamId, res, done);
      },
      // Translate into an array of caussenardsId
      function (teamFound, done) {
        if (teamFound) {
          var caussenardsIdFound = [];
          teamFound.dataValues.members.forEach(function (caussenard) {
            caussenardsIdFound.push(caussenard.dataValues.id);
          });
          done(null, caussenardsIdFound);
        } else {
          Return.fail(res, 404, errno.not_exist, "Équipe inexistant", "team with id " + teamId + " was not found");
        }
      },
      // Find caussenards with Id
      function(caussenardsIdFound, done) {
        if (caussenardsIdFound.length > 0) {
          nbOfCaussenards = caussenardsIdFound.length;
          caussenarddb.nbOfCaussenards({id: caussenardsIdFound, gender: 1}, res, done);
        } else {
          done(null, 0);
        }
      },
      // Calculate stats
      function(nbOfMen, done) {
        done(statsUtils.genderStats({men: nbOfMen, 
          women: nbOfCaussenards - nbOfMen}));
      }
    ], 
    // Return the response
    function(stats) {
      Return.success(res, 200, stats);
    });

  },

  getAge: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var teamId  = req.params.id;
    var team = null;
    var yearNumber = null;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearId
      function(done) {
        check.elementId(teamId, "teamId", res, done);
      },
      // Check if the team exist and get full informations
      function (done) {
        teamdb.getTeamInfos(teamId, res, done);
      },
      // Check if the year exist
      function (teamFound, done) {
        if (teamFound) {
          team = teamFound;
          yeardb.getYearInfos(teamFound.yearId, res, done);
        } else {
          Return.fail(res, 404, errno.not_exist, "Équipe inexistant", "team with id " + teamId + " was not found");
        }
      },
      // Translate into an array of teamId
      function (yearFound, done) {
        if (yearFound) {
          yearNumber = yearFound.number;
          var caussenardsIdFound = [];
          team.dataValues.members.forEach(function(caussenard) {
            caussenardsIdFound.push(caussenard.dataValues.id);
          });
          done(null, caussenardsIdFound);
        } else {
          Return.fail(res, 404, errno.not_exist, "Année inexistant", "year was not found");
        }
      },
      // Find caussenards with Id
      function(caussenardsIdFound, done) {
        if (caussenardsIdFound.length > 0) {
          caussenarddb.getAgeofCaussenards(caussenardsIdFound, new Date(yearNumber, 4, 15), res, done);
        } else {
          done(null, caussenardsIdFound);
        }
      },
      // Find caussenards with Id
      function(agesFound, done) {
        done(statsUtils.ageStats(agesFound));
      }
    ],
    // Return the response
    function(caussenardsFound) {
      Return.success(res, 200, caussenardsFound);
    });
  },

};