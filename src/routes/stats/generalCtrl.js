// Import 
var asyncLib  = require("async");
var Return = require("../../functions/returns");
var statsUtils = require("../../utils/stats.utils");
var statsdb = require("../../databaseRequests/stats");
var yeardb = require("../../databaseRequests/yearsReq");
var check  = require("../../functions/check");


module.exports = {


  getCaussenardsPerYear: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var yearStart = req.query.start;
    var yearEnd = req.query.end;
    var stats = new Array();
     
    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearStart
      function(done) {
        if (yearStart != undefined || yearStart != null) {
          check.year(yearStart, res, done);
        } else {
          yearStart = 1975;
          done(null);
        }
      },
      // Check yearEnd
      function(done) {
        if (yearEnd != undefined || yearEnd != null) {
          check.year(yearEnd, res, done);
        } else {
          yearEnd = 2100;
          done(null);
        }
      },
      // Get YearId 
      function(done) {
        yeardb.selectYearsBetween( yearStart, yearEnd, [["number", "ASC"]], res, done);
      },
      // For each year 
      function(yearsFound, done) {
        if (yearsFound.length > 0) {
          var count = 0;
          stats = new Array(yearsFound.length);
          yearsFound.forEach((year, index) => {
            var yearId = year.dataValues.id;
            statsdb.caussenardsCountOfYear(yearId, res)
              .then((response) => {
                stats.splice(index, 1, {
                  year: year.dataValues.number,
                  stats: response
                });
                count += 1;
                if(count == yearsFound.length) {
                  done();
                }
              });
          });
        } else {
          done();
        }
      }
    ], 
    // Return the response
    function() {
      Return.success(res, 200, stats);
    });

  },

  getGenderPerYear: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var yearStart = req.query.start;
    var yearEnd = req.query.end;
    var stats = new Array();
     
    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearStart
      function(done) {
        if (yearStart != undefined || yearStart != null) {
          check.year(yearStart, res, done);
        } else {
          yearStart = 1975;
          done(null);
        }
      },
      // Check yearEnd
      function(done) {
        if (yearEnd != undefined || yearEnd != null) {
          check.year(yearEnd, res, done);
        } else {
          yearEnd = 2100;
          done(null);
        }
      },
      // Get YearId 
      function(done) {
        yeardb.selectYearsBetween( yearStart, yearEnd,
          [["number", "ASC"]], res, done);
      },
      // For each year 
      function(yearsFound, done) {
        if (yearsFound.length > 0) {
          var count = 0;
          stats = new Array(yearsFound.length);
          yearsFound.forEach((year, index) => {
            var yearId = year.dataValues.id;
            statsdb.caussenardGenderOfYear(yearId, res)
              .then((response) => {
                stats.splice(index, 1, {
                  year: year.dataValues.number,
                  stats: statsUtils.genderStats(response)
                });
                count += 1;
                if(count == yearsFound.length) {
                  done();
                }
              });
          });
        } else {
          done();
        }
      }
    ], 
    // Return the response
    function() {
      Return.success(res, 200, stats);
    });

  },

  getAgePerYear: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var yearStart = req.query.start;
    var yearEnd = req.query.end;
    var stats = new Array();

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearStart
      function(done) {
        if (yearStart != undefined || yearStart != null) {
          check.year(yearStart, res, done);
        } else {
          yearStart = 1976;
          done(null);
        }
      },
      // Check yearEnd
      function(done) {
        if (yearEnd != undefined || yearEnd != null) {
          check.year(yearEnd, res, done);
        } else {
          yearEnd = 2100;
          done(null);
        }
      },
      // Get YearId 
      function(done) {
        yeardb.selectYearsBetween( yearStart, yearEnd,
          [["number", "ASC"]], res, done);
      },
      // For each year 
      function(yearsFound, done) {
        if (yearsFound.length > 0) {
          var count = 0;
          stats = new Array(yearsFound.length);
          yearsFound.forEach((year, index) => {
            var yearId = year.dataValues.id;
            statsdb.caussenardAgeOfYear(yearId, res)
              .then((response) => {
                stats.splice(index, 1, {
                  year: year.dataValues.number,
                  stats: statsUtils.ageStats(response)
                });
                count += 1;
                if(count == yearsFound.length) {
                  done();
                }
              });
          });
        } else {
          done();
        }
      }
    ],
    // Return the response
    function() {
      Return.success(res, 200, stats);
    });
  },

  getTeamsPerYear: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var yearStart = req.query.start;
    var yearEnd = req.query.end;
    var stats = new Array();

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearStart
      function(done) {
        if (yearStart != undefined || yearStart != null) {
          check.year(yearStart, res, done);
        } else {
          yearStart = 1975;
          done(null);
        }
      },
      // Check yearEnd
      function(done) {
        if (yearEnd != undefined || yearEnd != null) {
          check.year(yearEnd, res, done);
        } else {
          yearEnd = 2100;
          done(null);
        }
      },
      // Get YearId 
      function(done) {
        yeardb.selectYearsBetween( yearStart, yearEnd,
          [["number", "ASC"]], res, done);
      },
      // Get number of teams 
      function(yearsFound, done) {
        if (yearsFound.length > 0) {
          var count = 0;
          stats = new Array(yearsFound.length);
          yearsFound.forEach((year, index) => {
            statsdb.teamsCountOfYear(year.dataValues.id)
              .then((response) => {
                stats.splice(index, 1, {
                  year: year.dataValues.number,
                  stats: response
                });
                count += 1;
                if(count == yearsFound.length) {
                  done();
                }
              });
          });
        } else {
          done();
        }
      }
    ],
    // Return the response
    function() {
      Return.success(res, 200, stats);
    });
  },

  getNbOfCaussenards: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var yearStart = req.query.start;
    var yearEnd = req.query.end;
    var sum = 0;
    
    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearStart
      function(done) {
        if (yearStart != undefined || yearStart != null) {
          check.year(yearStart, res, done);
        } else {
          yearStart = 1975;
          done(null);
        }
      },
      // Check yearEnd
      function(done) {
        if (yearEnd != undefined || yearEnd != null) {
          check.year(yearEnd, res, done);
        } else {
          yearEnd = 2100;
          done(null);
        }
      },
      // Get YearId 
      function(done) {
        yeardb.selectYearsBetween( yearStart, yearEnd,
          [["number", "ASC"]], res, done);
      },
      // For each year 
      function(yearsFound, done) {
        if (yearsFound.length > 0) {
          var count = 0;
          let stats = new Array(yearsFound.length);
          yearsFound.forEach((year) => {
            var yearId = year.dataValues.id;
            statsdb.caussenardsDistinctCountOfYear(yearId, stats, res)
              .then((response) => {
                sum += response;
                count += 1;
                if(count == yearsFound.length) {
                  done();
                }
              });
          });
        } else {
          done();
        }
      }
    ], 
    // Return the response
    function() {
      Return.success(res, 200, {count: sum});
    });
  },

  getNbOfParticipations: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var yearStart = req.query.start;
    var yearEnd = req.query.end;
    var sum = 0;
    
    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearStart
      function(done) {
        if (yearStart != undefined || yearStart != null) {
          check.year(yearStart, res, done);
        } else {
          yearStart = 1975;
          done(null);
        }
      },
      // Check yearEnd
      function(done) {
        if (yearEnd != undefined || yearEnd != null) {
          check.year(yearEnd, res, done);
        } else {
          yearEnd = 2100;
          done(null);
        }
      },
      // Get YearId 
      function(done) {
        yeardb.selectYearsBetween( yearStart, yearEnd,
          [["number", "ASC"]], res, done);
      },
      // For each year 
      function(yearsFound, done) {
        if (yearsFound.length > 0) {
          var count = 0;
          yearsFound.forEach((year) => {
            var yearId = year.dataValues.id;
            statsdb.caussenardsCountOfYear(yearId, res)
              .then((response) => {
                sum += response;
                count += 1;
                if(count == yearsFound.length) {
                  done();
                }
              });
          });
        } else {
          done();
        }
      }
    ], 
    // Return the response
    function() {
      Return.success(res, 200, {count: sum});
    });
  },

  getNbOfTeams: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var yearStart = req.query.start;
    var yearEnd = req.query.end;
    var sum = 0;

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearStart
      function(done) {
        if (yearStart != undefined || yearStart != null) {
          check.year(yearStart, res, done);
        } else {
          yearStart = 1975;
          done(null);
        }
      },
      // Check yearEnd
      function(done) {
        if (yearEnd != undefined || yearEnd != null) {
          check.year(yearEnd, res, done);
        } else {
          yearEnd = 2100;
          done(null);
        }
      },
      // Get YearId 
      function(done) {
        yeardb.selectYearsBetween( yearStart, yearEnd,
          [["number", "ASC"]], res, done);
      },
      // Get number of teams 
      function(yearsFound, done) {
        if (yearsFound.length > 0) {
          var count = 0;
          yearsFound.forEach((year) => {
            statsdb.teamsCountOfYear(year.dataValues.id)
              .then((response) => {
                sum += response;
                count += 1;
                if(count == yearsFound.length) {
                  done();
                }
              });
          });
        } else {
          done();
        }
      }
    ],
    // Return the response
    function() {
      Return.success(res, 200, sum);
    });
  }

};


