// Import 
var asyncLib  = require("async");
var Return = require("../../functions/returns");
var statsUtils = require("../../utils/stats.utils");
var statsdb = require("../../databaseRequests/stats");
var yeardb = require("../../databaseRequests/yearsReq");
var check  = require("../../functions/check");


module.exports = {
  
  getCommonAge: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var yearStart = req.query.start;
    var yearEnd = req.query.end;
    var listAge = new Array();

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearStart
      function(done) {
        if (yearStart != undefined || yearStart != null) {
          check.year(yearStart, res, done);
        } else {
          yearStart = 1975;
          done(null);
        }
      },
      // Check yearEnd
      function(done) {
        if (yearEnd != undefined || yearEnd != null) {
          check.year(yearEnd, res, done);
        } else {
          yearEnd = 2100;
          done(null);
        }
      },
      // Get YearId 
      function(done) {
        yeardb.selectYearsBetween( yearStart, yearEnd,
          [["number", "ASC"]], res, done);
      },
      // For each year 
      function(yearsFound, done) {
        if (yearsFound.length > 0) {
          var count = 0;
          yearsFound.forEach((year) => {
            var yearId = year.dataValues.id;
            statsdb.caussenardAgeOfYear(yearId, res)
              .then((response) => {
                listAge = listAge.concat(response);
                count += 1;
                if(count == yearsFound.length) {
                  done(null);
                }
              });
          });
        } else {
          done();
        }
      },
      function(done) {
        done(statsUtils.ageStats(listAge));
      }
    ],
    // Return the response
    function(stats) {
      Return.success(res, 200, stats);
    });
  },

  getCommonGender: function(req, res) {
    // Authorization
    const authLevel = 0;
    // Params
    var yearStart = req.query.start;
    var yearEnd = req.query.end;
    var genderObject = {
      men: 0,
      women: 0
    };

    // Process
    asyncLib.waterfall([
      // Check Authorization
      function(done) {
        check.authorization(authLevel, req, res, done);
      },
      // Check yearStart
      function(done) {
        console.log("A1");
        if (yearStart != undefined || yearStart != null) {
          console.log("A2");
          check.year(yearStart, res, done);
        } else {
          console.log("A3");
          yearStart = 1975;
          done(null);
        }
      },
      // Check yearEnd
      function(done) {
        console.log("B1");
        if (yearEnd != undefined || yearEnd != null) {
          console.log("B2");
          check.year(yearEnd, res, done);
        } else {
          console.log("B3");
          yearEnd = 2100;
          done(null);
        }
      },
      // Get YearId 
      function(done) {
        console.log("je suis ici");
        yeardb.selectYearsBetween( yearStart, yearEnd, null, res, done);
        console.log("et là");
      },
      // For each year 
      function(yearsFound, done) {
        console.log("size: ", yearsFound.length);
        if (yearsFound.length > 0) {
          var count = 0;
          yearsFound.forEach((year) => {
            var yearId = year.dataValues.id;
            statsdb.caussenardGenderOfYear(yearId, res)
              .then((response) => {
                genderObject.men = response.men;
                genderObject.women = response.women;
                count += 1;
                if(count == yearsFound.length) {
                  console.log("je suis passé par là : 1" );
                  done(null);
                }
              });
          });
        } else {
          console.log("je suis passé par là : 2" );
          done();
        }
      },
      function(done) {
        console.log("je suis passé par là : 3" );
        done(statsUtils.genderStats(genderObject));
      }
    ],
    // Return the response
    function(stats) {
      Return.success(res, 200, stats);
    });
  }
  
};
