// Import 
var express = require("express");

var generalCtrl     = require("./stats/generalCtrl");
var yearsCtrl       = require("./stats/yearsCtrl");
var teamsCtrl       = require("./stats/teamsCtrl");
var caussenardsCtrl = require("./stats/caussenardsCtrl");

// Router
exports.router = (function() {
  var statsRouter = express.Router();

  statsRouter.route("/years/caussenards/")          .get(generalCtrl.getCaussenardsPerYear);
  statsRouter.route("/years/caussenards/gender")    .get(generalCtrl.getGenderPerYear);
  statsRouter.route("/years/caussenards/age")       .get(generalCtrl.getAgePerYear);
  statsRouter.route("/years/teams")                 .get(generalCtrl.getTeamsPerYear);
  statsRouter.route("/years/count/caussenards/")    .get(generalCtrl.getNbOfCaussenards);
  statsRouter.route("/years/count/participations/") .get(generalCtrl.getNbOfParticipations);
  statsRouter.route("/years/count/teams/")          .get(generalCtrl.getNbOfTeams);
  
  statsRouter.route("/years/:id/caussenards/gender").get(yearsCtrl.getGender);
  statsRouter.route("/years/:id/caussenards/age")   .get(yearsCtrl.getAge);
    
  statsRouter.route("/teams/:id/caussenards/gender").get(teamsCtrl.getGender);
  statsRouter.route("/teams/:id/caussenards/age")   .get(teamsCtrl.getAge);
  
  statsRouter.route("/caussenards/gender")          .get(caussenardsCtrl.getCommonGender);
  statsRouter.route("/caussenards/age")             .get(caussenardsCtrl.getCommonAge);

  return statsRouter;
})();