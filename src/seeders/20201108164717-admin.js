"use strict";

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.bulkInsert("Users", [{
      email: "admin@test.fr",
      password: "$2b$05$.C8lX8ntKmUNKGoS2AE19u0/5uxFlAoE3S1LsudRm6vgqlwAEu3wi",
      authLevel: 5,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete("Users", null, {});
  }
};
