// Import
require("dotenv").config({path: __dirname + "/.env"});
const express     = require("express");
const bodyParser  = require("body-parser");

const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const swaggerOptions = require("../doc/OpenAPI/config.json");

// Documentation
const swaggerDocs = swaggerJsDoc(swaggerOptions);

// Instanciate server
var server = express();

if (process.env.NODE_ENV != "cicd") {
  server.use(require("cors")());
}

// Body Parser configuration
server.use(bodyParser.urlencoded({limit: "10mb", extended: true}));
server.use(bodyParser.json({limit: "10mb", extended: true}));

server.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs, { explorer: true }));

// Set hearders
server.use(function(req, res, next) {
  res.header("Content-Type", "application/json");
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Origin, Authorization, X-Requested-With");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, PATCH");
  next();
});

// Configure routes
server.use("/api/", require("./routes/apiRouter").router);
server.use("/stats/", require("./routes/statsRouter").router);

server.use("/", function(req, res) { 
  res.status(404).json({
    result: "fail",
    error : {
      devMessage : "Request not found",
      userMessage : "Requête inconnue"
    }
  });
});

// Launch server
var app = server.listen(process.env.PORT, function() {
  console.log("API Server started on " + process.env.PORT);
});

function shutDown() {
  console.log("close server");
  app.close();
}

module.exports = server;
module.exports.shutDown = shutDown;
