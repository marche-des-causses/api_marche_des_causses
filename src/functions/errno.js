
module.exports = {
  
  unknown         : 0,
  miss_param      : 1,
  wrong_param     : 2,
  already_exist   : 3,
  not_exist       : 4,
  cant_log        : 5,

  find_fail       : 101,
  create_fail     : 102,
  update_fail     : 103,
  delete_fail     : 104,
  request_fail    : 110,

  invalid_token    : 200,
  expired_token    : 201,
  failed_token     : 202,
  unauthorized     : 203,
  miss_token       : 204,

  hash_fail        : 300,
  compare_fail     : 301,

};