

/**
 * @module Responses
 * @description Send differents types of responses
 */
module.exports = {

  /**
   * @description To return a success response
   * @param {*} res the response object
   * @param {*} status Status of the response
   * @param {*} content Content of the response
   */
  success : function(res, status, content) {
    return res.status(status).json({
      "result": "success",
      "content": content
    });
  },

  /**
   * @description To return a fail response
   * @param {*} res the response object
   * @param {*} status Status of the response
   * @param {*} code the error number of the response @see errno
   * @param {*} userMessage Message destinate to user
   * @param {*} devMessage Message destinate to developpers
   */
  fail : function(res, status, code, userMessage, devMessage) {
    console.log(devMessage);
    return res.status(status).json({
      "result": "fail",
      "error": {
        "code": code,
        "devMessage" : devMessage,
        "userMessage" : userMessage
      }
    });
  }
};