
const Return = require("../functions/returns");
const errno = require("../functions/errno");
const jwtUtils = require("../utils/jwt.utils");

const EMAIL_REGEX     = /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX  = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$/;
const DATE_REGEX      = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/;

module.exports = {

  authorization: function (authLevel, req, res, done) {
    if (jwtUtils.checkAuthorization(authLevel, req, res, done)) {
      return done(null);
    } 
  },

  elementId: function (value, name, res, done) {
    if (value == undefined || value == null) {
      return done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", name + " is missing"));
    } else if (isNaN(value) || value <= 0) {
      return done(Return.fail(res, 400, errno.wrong_param, "Mauvaise valeur", name + " has wrong value. (value= " + value + " )"));
    } else {
      return done(null);
    }
  },

  name: function (value, name, res, done) {
    if (value == undefined || value == null) {
      return done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", name + " is missing"));
    } else if (value.length < 3) {
      return done(Return.fail(res, 400, errno.wrong_param, "Mauvaise valeur", name + " has wrong value. (value= " + value + " )"));
    } else {
      return done(null);
    }
  },

  email: function (value, res, done) {
    if (value == undefined || value == null) {
      return done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", "email is missing"));
    } else if (!EMAIL_REGEX.test(value)) {
      return done(Return.fail(res, 400, errno.wrong_param, "Mauvaise valeur","email has wrong value. (value= " + value + " )"));
    } else {
      return done(null);
    }
  },

  password: function (value, res, done) {
    if (value == undefined || value == null) {
      return done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", "password is missing"));
    } else if (!PASSWORD_REGEX.test(value)) {
      return done(Return.fail(res, 400, errno.wrong_param, "Mauvaise valeur", "password has wrong value. (value= " + value + " )"));
    } else {
      return done(null);
    }
  },

  year: function (value, res, done) {
    if (value == undefined || value == null) {
      return done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", "year number is missing"));
    } else if (isNaN(value) || value < 1976 || value >  2100) {
      return done(Return.fail(res, 400, errno.wrong_param, "Mauvaise valeur", "year number has wrong value. (value= " + value + " )"));
    } else {
      return done(null);
    }
  },

  teamNumber: function (value, res, done) {
    if (value == undefined || value == null) {
      return done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", "team number is missing"));
    } else if (isNaN(value) || value < 1) {  
      return done(Return.fail(res, 400, errno.wrong_param, "Mauvaise valeur", "team number has wrong value. (value= " + value + " )"));
    } else {
      return done(null);
    }
  },

  teamName: function (value, res, done) {
    if (value == undefined || value == null) {
      return done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", "Theme name is missing"));
    } else if (value.length <= 5) {
      return done(Return.fail(res, 400, errno.wrong_param, "Mauvaise valeur", "Theme name has wrong value. (value= " + value + " )"));
    } else {
      return done(null);
    }
  },

  theme: function (value, res, done) {
    if (value == undefined || value == null) {
      return done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", "theme is missing"));
    } else if (value.length < 6) {
      return done(Return.fail(res, 400, errno.wrong_param, "Mauvaise valeur", "theme has wrong value. (value= " + value + " )"));
    } else {
      return done(null);
    }
  },

  authLevel: function (value, res, done) {
    if (value == undefined || value == null) {
      return done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", "authLevel is missing"));
    } else if (isNaN(value) || value < 0 || value >  5) {
      return done(Return.fail(res, 400, errno.wrong_param, "Mauvaise valeur", "authLevel has wrong value. (value= " + value + " )"));
    } else {
      return done(null);
    }
  },

  birth: function (value, res, done) {
    if (value == undefined || value == null) {
      return done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", "Date of birth is missing"));
    } else if (!DATE_REGEX.test(value)) {
      return done(Return.fail(res, 400, errno.wrong_param, "Mauvaise valeur", "Date of birth has wrong value. (value= " + value + " )"));
    } else {
      return done(null);
    }
  },

  gender: function (value, res, done) {
    if (value == undefined || value == null) {
      return done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", "Gender is missing"));
    } else if (isNaN(value) || value <= 0 || value > 3) {
      return done(Return.fail(res, 400, errno.wrong_param, "Mauvaise valeur", "Gender has wrong value. (value= " + value + " )"));
    } else {
      return done(null);
    }
  },

  status: function (value, res, done) {
    if (value == undefined || value == null) {
      return done(Return.fail(res, 400, errno.miss_param, "Paramètre manquant", "Status is missing"));
    } else if (["pending", "running", "finished", "cancel", "error"].includes(value)) {
      return done(Return.fail(res, 400, errno.wrong_param, "Mauvaise valeur", "Status has wrong value. (value= " + value + " )"));
    } else {
      return done(null);
    }
  }

};