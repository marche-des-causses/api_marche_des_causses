
const statsUtils = require("../../utils/stats.utils");

module.exports = class LeaderBuild {

  constructor(list) {
    this.groupList = list; 
    this.grade = 0;
    this.grades = {
      mixity: {},
      unknowledge: {},
      experience: {},
      leaderExperience: {},
      majority: {},
    };
  }

  evaluateGroup() {

    this.grades.mixity = statsUtils.stats(this.groupList.map(elem => elem.grades.mixity));

    this.grades.unknowledge = statsUtils.stats(this.groupList.map(elem => elem.grades.unknowledge));

    this.grades.experience = statsUtils.stats(this.groupList.map(elem => elem.grades.experience));

    this.grades.leaderExperience = statsUtils.stats(this.groupList.map(elem => elem.grades.leaderExperience));

    this.grades.majority = statsUtils.stats(this.groupList.map(elem => elem.grades.majority));

  }

  gradeGroup(coefficients) {
    var gradeSum =  this.grades.mixity.average + this.grades.unknowledge.average +
                    this.grades.leaderExperience.average + this.grades.majority.average;
    var coeffSum =  coefficients.mixity + coefficients.unknowledge +
                    coefficients.leaderExperience + coefficients.majority;
    this.grade = gradeSum / coeffSum;
  }
};