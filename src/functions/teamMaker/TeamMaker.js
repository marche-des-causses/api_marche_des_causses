// Import
const models = require("../../models");
const Caussenard = require("./Caussenard");
const LeaderBuild = require("./LeaderBuild");
const TeamBuild = require("./TeamBuid");

module.exports = class TeamMaker {
  static COEFFICIENT
  static NB_OF_PROPOSITIONS

  constructor() {
    if (TeamMaker.exists) {
      return TeamMaker.instance;
    }
    TeamMaker.instance = this;
    TeamMaker.exists = true;
    TeamMaker.COEFFICIENT = {
      mixity: 2,
      unknowledge: 8,
      leaderExperience: 5,
      majority: 5,
    };
    TeamMaker.NB_OF_PROPOSITIONS = 3;
    this.id = 0;
    this.startTime = Date.now();
    this.params = new Object();
    this.caussenardList = new Array();
    this.startProcess();
    return this;
  }

  destructor() {
    TeamMaker.exists = false;
  }

  startProcess() {
    // Get TeamMaker
    models.TeamMaker.findOne({
      where: {status: "pending"},
      order: [["updatedAt", "ASC"]],
      attributes: ["id", "params"]
    }).then((teamMaker) => {
      if (teamMaker) {
        this.startTime = Date.now();
        this.id = teamMaker.dataValues.id;
        this.params = teamMaker.dataValues.params;
        teamMaker.update({
          status: "running",
          progress: 0
        }).then(() => {
          this.params.list.forEach((id) => {
            var caussenard = new Caussenard(id);
            caussenard.generateProfile().then(() => {
              this.caussenardList.push(caussenard);
              if (this.caussenardList.length == this.params.list.length) {
                this.process();
              }
            }).catch((err) => {
              // catch error : impossible de générer un profil 
              this.catchError(err);
            });
          });
        });
      } else {
        // catch error : Pas de process à effectuer 
        this.destructor();
      }
    });
  }

  process() {
    var result = new Object();

    var teamMatesTable = new Object();

    // Provide grades
    var menSum = 0;
    var majSum = 0;
    var xpSum = 0;
    var lxpSum = 0;
    this.caussenardList.forEach(element => {
      menSum += (element.gender == 1) ? 1 : 0;
      majSum += (element.majority) ? 1 : 0;
      xpSum += element.years;
      lxpSum += element.leaderYears;

      teamMatesTable[element.id] = {};
      this.caussenardList.forEach(c => {
        teamMatesTable[element.id][c.id] = (element.teamMates.includes(c.id));
      });

    });

    this.params.nbOfteamsList.forEach(nbOfteams => {
      // Determine sizes of teams
      const minSize = Math.trunc(this.caussenardList.length/nbOfteams);
      const maxSize = minSize + 1;

      TeamBuild.setGlobalMenProportion(menSum/this.caussenardList.length);
      TeamBuild.setXPAverage(xpSum/this.caussenardList.length);
      TeamBuild.setLXPAverage(lxpSum/this.caussenardList.length);

      // create team build
      var teamBuildList = new Array();
      this.createTeamBuild([], this.caussenardList, minSize, teamBuildList);
      this.createTeamBuild([], this.caussenardList, maxSize, teamBuildList);

      teamBuildList.forEach(element => {
        element.evaluateTeam();
      });

      // create groups of team build => proposition
      var propositionList = new Array();
      this.createLeaderBuild([], teamBuildList, nbOfteams, 
        this.caussenardList.length, propositionList);
      
      propositionList.forEach(element => {
        element.evaluateGroup();
        element.gradeGroup(TeamMaker.COEFFICIENT);
      });
      
      // first selection
      var list = Array.from(propositionList);
      // Unknowledge filter
      list = list.filter(e => e.grades.unknowledge.average == 100);
      // Majority filter
      if (majSum > nbOfteams) {
        list = list.filter(e => e.grades.majority.min == 0);
      }
      // Mixity filter
      list = list.filter(e => e.grades.mixity.average >= Caussenard.MAX_GRADE * (1 - 2 * Math.abs(0.5 - menSum/this.caussenardList.length)));
      
      // Leader Experience sort 
      list.sort(function(a,b) {
        return a.grades.leaderExperience.rsd - b.grades.leaderExperience.rsd;
      });

      if (list.length == 0) {
        list = Array.from(propositionList);
        list.sort(function(a,b) {
          return a.grade - b.grade;
        });
      }

      // Set result
      result[nbOfteams] = (list.slice(0, TeamMaker.NB_OF_PROPOSITIONS))
        .map(
          x => x.groupList.map(
            y => y.mates.map(
              z => z.id
            )
          )
        );

      // Made TeamMates Table

    });

    result["duration"] = Date.now() - this.startTime;
    result["teamMates"] = teamMatesTable;
    models.TeamMaker.update({
      status: "finished",
      progress: 100,
      result: result
    }, {
      where: {
        id: this.id
      }
    }).then(() => {
      this.startProcess();
    });

  }


  createTeamBuild(createdList, restList, teamSize, returnList) {
    if (teamSize == 0) {
      returnList.push(new TeamBuild(createdList));
    } else {
      if (teamSize < restList.length) {
        restList.forEach((caussenard, index) => {
          this.createTeamBuild(createdList.concat([caussenard]), 
            restList.slice(index + 1), teamSize - 1, returnList);
        });
      } else if (teamSize == restList.length) {
        returnList.push(new TeamBuild(createdList.concat(restList)));
      }
    }
  }

  createLeaderBuild(createdList, restList, nbOfTeam, nbOfCaussenard, returnList) {
    if (nbOfTeam == 0) {
      if (nbOfCaussenard == 0) {
        returnList.push(new LeaderBuild(createdList));
      }
    } else {
      if (nbOfTeam <= restList.length) {
        restList.forEach((teamBuild, index) => {
          var newRestList = restList.slice(index + 1);
          newRestList = newRestList.filter(tb => tb.isMatching(teamBuild) == false);
          this.createLeaderBuild(
            createdList.concat([teamBuild]), 
            newRestList, 
            nbOfTeam - 1, 
            nbOfCaussenard - teamBuild.mates.length, 
            returnList
          );
        });
      }
    }
  }

  catchError(err) {
    models.TeamMaker.update({
      status: "error",
      progress: 100,
      result: {error: err}
    }, {
      where: {id: this.id}
    });
  }

};