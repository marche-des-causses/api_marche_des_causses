// Import
const models  = require("../../models");
//const asyncLib  = require("async");


module.exports = class Caussenard {
  constructor(id) {
    this.id = id;
    this.years = 0;
    this.leaderYears = 0;
    this.teamMates = new Array();
    this.majority = false;
    this.gender = true;
    this.experience = 0;
  }

  isMatching(otherCaussenard) {
    return otherCaussenard.id == this.id;
  }

  generateProfile() {
    var self = this;
    return new Promise (function (resolve, reject) {
      models.Caussenard.findOne({
        where: {id: self.id},
        attributes: ["id", "birth", "gender"],
        include: {
          model: models.Team,
          as: "teams",
          attributes: models.Team.basicFields,
          include: {
            model: models.Caussenard,
            as: "members",
            attributes: ["id"],
            through: {}
          },
          through: {
            as: "role",
            attributes: ["isLeader"]
          }
        }
      }).then((caussenard) => {
        if (caussenard) {
          caussenard.dataValues.teams.forEach(team => {
            self.years += 1;
            if (team.dataValues.role) {
              self.leaderYears += team.dataValues.role.isLeader ? 1 : 0;
            }
            team.dataValues.members.forEach(caussenard => {
              self.teamMates.push(caussenard.dataValues.id);
            });
          });
          self.majority = Math.abs((new Date(Date.now() - Date.parse(caussenard.dataValues.birth))).getUTCFullYear() - 1970);
          self.gender = caussenard.dataValues.gender;
          resolve();
        } else {
          reject("Caussenard introuvable");
        }
      }).catch((err) => {
        reject(err);
      });
    });
  }
};