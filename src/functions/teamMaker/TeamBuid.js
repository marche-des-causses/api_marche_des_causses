

module.exports = class TeamBuild {
  static MAX_GRADE
  static GLOBAL_MEN_PROPORTION
  static XP_AVERAGE
  static LXP_AVERAGE

  static setGlobalMenProportion(value) {
    this.GLOBAL_MEN_PROPORTION = value;
  }
  static setXPAverage(value) {
    this.XP_AVERAGE = value;
  }
  static setLXPAverage(value) {
    this.LXP_AVERAGE = value;
  }

  constructor(list) {
    TeamBuild.MAX_GRADE = 100;
    TeamBuild.GLOBAL_MEN_PROPORTION = 50;
    TeamBuild.XP_AVERAGE = 2;
    TeamBuild.LXP_AVERAGE = 2;
    this.mates = list;
    this.xp = 0;
    this.binomeXp = 0;
    this.grades = {
      mixity: 0,
      unknowledge: 0,
      experience: 0,
      leaderExperience: 0,
      majority: 0,
    };
  }

  isMatching(otherTeamBuild) {
    var result = false;
    this.mates.forEach((caussenard) => {
      if (otherTeamBuild.mates.filter(c => c.isMatching(caussenard)).length > 0) {
        result = true;
      }
    });
    return result;
  }

  evaluateTeam() {
    this.evaluateUnknowledge();
    this.evaluateMixity();
    this.evaluateMajority();
    this.evaluateExperience();
  }

  evaluateUnknowledge() {
    var grade = TeamBuild.MAX_GRADE;
    this.mates.forEach(caussenard => {
      var otherMates = this.mates.filter(id => id != caussenard.id);
      otherMates.forEach(mate => {
        if(caussenard.teamMates.includes(mate.id)) {
          grade = 0;
        }
      });
    });
    this.grades.unknowledge = grade;
  }

  evaluateMixity() {
    var men = 0;
    this.mates.forEach(caussenard => {
      if (caussenard.gender == 1) {
        men ++;
      }
    });
    if (this.mates.length == 1) {
      this.grades.mixity = this.MAX_GRADE * (1 - 2 * Math.abs(0.5 - this.GLOBAL_MEN_PROPORTION));
    } else {
      this.grades.mixity = this.MAX_GRADE * (1 - 2 * Math.abs(0.5 - men/this.mates.length));
    }
  }

  evaluateMajority() {
    var maj = 0;
    this.mates.forEach(caussenard => {
      if (caussenard.majority) {
        maj ++;
      }
    });
    //this.grades.majority = (maj >= 1) ? Caussenard.MAX_GRADE : 0;
    this.grades.majority = maj/this.mates.length * this.MAX_GRADE;
  }

  evaluateExperience() {
    var xp = 0;
    var lxp = 0;
    this.mates.forEach(caussenard => {
      xp += caussenard.years;
      lxp += caussenard.leaderYears;
    });
    this.grades.experience = xp/this.mates.length * this.MAX_GRADE/(2*this.XP_AVERAGE) ;
    this.grades.leaderExperience = lxp/this.mates.length * this.MAX_GRADE/(2*this.LXP_AVERAGE) ;
  }

};