const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    var caussenard;
    
    beforeEach( async () => {
      caussenard = await models.Caussenard.create(
        {firstname:"Prenom", lastname:"Nom", gender: 1});
    });
    
    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",401],
      ["editer",   200],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .put("/api/caussenards/"+ caussenard.id)
        .send({firstname: "Prénomm", lastname: "Nomm"})
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test to update an unexistiting caussenard", async () => {
      const res = await request(app)
        .put("/api/caussenards/"+ (caussenard.id + 100))
        .send({firstname:"Prenomm", lastname:"Nomm"})
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.not_exist);
    });

    test("Test param id", async () => {
      const res = await request(app)
        .put("/api/caussenards/lala")
        .send({firstname:"Prenomm", lastname:"Nomm"})
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.wrong_param);
    });

    test("Test to update a caussenard to an existing caussenard", async () => {
      await models.Caussenard.create({firstname:"Prenomm", lastname:"Nomm", gender: 1});
      const res = await request(app)
        .put("/api/caussenards/"+ caussenard.id)
        .send({firstname:"Prenomm", lastname:"Nomm"})
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(409);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.already_exist);
    });

    each([
      [{firstname:"P"}                        , 400, errno.wrong_param],
      [{lastname:"N"}                         , 400, errno.wrong_param],
      [{}                                     , 400, errno.miss_param]
    ]).test("test update fail with: '%s'", async (body, statusCode, errnb) => {
      const res = await request(app)
        .put("/api/caussenards/"+ caussenard.id)
        .send(body)
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errnb);
    });

    each([
      [{firstname:"Prenomm", lastname:"Nomm"} , true, true],
      [{firstname:"Prenomm"}                  , true, false],
      [{lastname:"Nomm"}                      , false, true]
    ]).test("test update success with: '%s'", async (body, fnameUp, lnameUp) => {
      const res = await request(app)
        .put("/api/caussenards/"+ caussenard.id)
        .send(body)
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      const theCaussenard = await models.Caussenard.findOne({where: {id: caussenard.id}});
      expect(theCaussenard.firstname).toEqual(fnameUp ? body.firstname : caussenard.firstname);
      expect(theCaussenard.lastname).toEqual(lnameUp ? body.lastname : caussenard.lastname);
    });

  }
};