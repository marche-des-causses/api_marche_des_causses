const request = require("supertest");
var models = require("../../models");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",200],
      ["editer",   200],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .get("/api/caussenards/")
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test with no caussenard created", async () => {
      const res = await request(app)
        .get("/api/caussenards/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.length).toEqual(0);
    });

    test("Test with one caussenard created", async () => {
      await models.Caussenard.create({firstname: "Prénom", lastname:"Nom", gender: 1});
      const res = await request(app)
        .get("/api/caussenards/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toHaveLength(1);
    });

    describe("Test with several caussenards", () => {

      var caussenard1;
      var caussenard2;
      var caussenard3;

      beforeEach( async () => {
        caussenard1 = await models.Caussenard.create(
          {firstname: "aPrénom", lastname:"aNom", gender: 1});
        caussenard2 = await models.Caussenard.create(
          {firstname: "aPrénom", lastname:"bNom", gender: 1});
        caussenard3 = await models.Caussenard.create(
          {firstname: "bPrénom", lastname:"bNom", gender: 1});
      });

      describe("Test request order", () => {
        each([
          ["firstname:ASC"              , 0, 1, 2],
          ["firstname:DESC"             , 2, 1, 0],
          ["firstname:DESC;lastname:ASC", 1, 2, 0],
          ["firstname:ASC;lastname:DESC", 1, 0, 2],
          ["lastname:DESC;firstname:ASC", 2, 0, 1],
          ["lastname:ASC;firstname:DESC", 0, 2, 1],
        ]).test("Test with order : '%s'", async (str, idOf1, idOf2, idOf3) => {
          const res = await request(app)
            .get("/api/caussenards/")
            .query({order: str})
            .set({"Authorization": tokens.verifUser});
          expect(res.type).toEqual("application/json");
          expect(res.statusCode).toEqual(200);
          expect(res.body.result).toEqual("success");
          expect(res.body.content).toHaveLength(3);
          expect(res.body.content[idOf1].id).toEqual(caussenard1.id);
          expect(res.body.content[idOf2].id).toEqual(caussenard2.id);
          expect(res.body.content[idOf3].id).toEqual(caussenard3.id);
        });
      });

      describe("Test request limit", () => {
        each([
          [0, 0],
          [1, 1],
          [3, 3],
          [6, 3],
        ]).test("Test with limit : '%d'", async (aLimit, listSize) => {
          const res = await request(app)
            .get("/api/caussenards/")
            .query({limit: aLimit})
            .set({"Authorization": tokens.verifUser});
          expect(res.type).toEqual("application/json");
          expect(res.statusCode).toEqual(200);
          expect(res.body.result).toEqual("success");
          expect(res.body.content.length).toEqual(listSize);
        });  
      });

      describe("Test request offset", () => {
        each([
          [0, 3],
          [1, 2],
          [3, 0],
          [5, 0],
        ]).test("Test with limit : '%d'", async (anOffset, listSize) => {
          const res = await request(app)
            .get("/api/caussenards/")
            .query({offset: anOffset})
            .set({"Authorization": tokens.verifUser});
          expect(res.type).toEqual("application/json");
          expect(res.statusCode).toEqual(200);
          expect(res.body.result).toEqual("success");
          expect(res.body.content).toHaveLength(listSize);
        });  
      });
    });
  }
};