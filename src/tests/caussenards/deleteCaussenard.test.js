const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    var caussenard;
    
    beforeEach( async () => {
      caussenard = await models.Caussenard.create(
        {firstname: "Prénom", lastname:"Nom", gender: 1});
    });
    
    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",401],
      ["editer",   401],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .delete("/api/caussenards/"+ caussenard.id)
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test to delete an unexistiting caussenard", async () => {
      const res = await request(app)
        .delete("/api/caussenards/"+ (caussenard.id + 1))
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.not_exist);
    });

    test("Test param id", async () => {
      const res = await request(app)
        .delete("/api/caussenards/lala/")
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.wrong_param);
    });

    test("Test delete caussenard", async () => {
      const res = await request(app)
        .delete("/api/caussenards/"+ caussenard.id + "/")
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.id).toEqual(caussenard.id);
      expect(await models.Caussenard.findOne({where: {id: caussenard.id}})).toBeNull();
    });

    test("Test recursive delete", async () => {
      const year = await models.Year.create({number: 2020,theme: "un theme"});
      const team = await models.Team.create({yearId: year.id , number: 1});
      await models.Member.create(
        {teamId: team.id, caussenardId: caussenard.id}, 
        {fields: models.Member.basicFields});

      const res = await request(app)
        .delete("/api/caussenards/"+ caussenard.id + "/")
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.id).toEqual(caussenard.id);
      expect(await models.Year.findOne({where: {id: year.id}})).not.toBeNull();
      expect(await models.Team.findOne({where: {id: team.id}})).not.toBeNull();
      expect(await models.Caussenard.findOne({where: {id: caussenard.id}})).toBeNull();
      expect(await models.Member.findOne({where: {teamId: team.id}})).toBeNull();
      
    });
  }
};