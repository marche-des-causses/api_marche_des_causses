const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",200],
      ["editer",   200],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const caussenard = await models.Caussenard.create({firstname:"Prenom", lastname:"Nom", gender: 1}, {fields: models.Caussenard.classicFields});
      const res = await request(app)
        .get("/api/caussenards/" + caussenard.id)
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    describe("Test parameter : caussenardId", () => {      
      test("Test with non-integer caussenardId", async () => {
        const res = await request(app)
          .get("/api/caussenards/ok/")
          .set({"Authorization": tokens.verifUser});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.wrong_param);
      });
    });

    test("Get an unexisting caussenard", async () => {
      const res = await request(app)
        .get("/api/caussenards/" + 1)
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.not_exist);
    });

    test("Get a caussenard", async () => {
      const year1 = await models.Year.create({number: 2020, theme: "le theme"}, {fields: models.Year.basicFields});
      const year2 = await models.Year.create({number: 2019, theme: "le theme"}, {fields: models.Year.basicFields});
      const team1 = await models.Team.create({name: "La team", number: 2, yearId:year1.id}, {fields: models.Team.basicFields});
      const team2 = await models.Team.create({name: "La team", number: 3, yearId:year2.id}, {fields: models.Team.basicFields});
      const caussenard = await models.Caussenard.create({firstname:"Prenom", lastname:"Nom", gender: 1}, {fields: models.Caussenard.classicFields});
      await models.Member.create({teamId: team1.id, caussenardId: caussenard.id});
      await models.Member.create({teamId: team2.id, caussenardId: caussenard.id});
      const res = await request(app)
        .get("/api/caussenards/" + caussenard.id)
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.firstname).toEqual("Prenom");
      expect(res.body.content.lastname).toEqual("Nom");
      expect(res.body.content.teams).toHaveLength(2);
    });

  }
};