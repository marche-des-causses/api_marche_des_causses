const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    let caussenard1;
    let caussenard2;
    let year1;
    let team1;
    let team2;
    let team3;

    beforeEach(async () => {
      year1 = await models.Year.create({number: 2020, theme: "le theme"}, {fields: models.Year.basicFields});
      await models.Year.create({number: 2019, theme: "le theme"}, {fields: models.Year.basicFields});
      await models.Year.create({number: 2018, theme: "le theme"}, {fields: models.Year.basicFields});
      team1 = await models.Team.create({name: "nom2", number: 1, yearId:year1.id}, {attributes: models.Team.basicFields});
      team2 = await models.Team.create({name: "nom3", number: 2, yearId:year1.id}, {fields: models.Team.basicFields});
      team3 = await models.Team.create({name: "nom1", number: 3, yearId:year1.id}, {fields: models.Team.basicFields});
      caussenard1 = await models.Caussenard.create({firstname:"Prenom", lastname:"Nom", gender: 1}, {fields: models.Caussenard.classicFields});
      caussenard2 = await models.Caussenard.create({firstname:"autrePrenom", lastname:"autreNom", gender: 1}, {fields: models.Caussenard.classicFields});
      await models.Member.create({teamId: team1.id, caussenardId: caussenard1.id});
      await models.Member.create({teamId: team2.id, caussenardId: caussenard1.id});
      await models.Member.create({teamId: team3.id, caussenardId: caussenard1.id});
    });

    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",200],
      ["editer",   200],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .get("/api/caussenards/" + caussenard1.id + "/teams")
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test with not existing caussenard", async () => {
      const res = await request(app)
        .get("/api/caussenards/"+ (caussenard1.id + 100) + "/teams/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.not_exist);
    });
    
    test("Test with existing caussenard without teams", async () => {
      const res = await request(app)
        .get("/api/caussenards/"+ caussenard2.id + "/teams/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.length).toEqual(0);
    });

    test("Test param id", async () => {
      const res = await request(app)
        .get("/api/caussenards/lala/teams")
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.wrong_param);
    });

    test("Test with existing caussenard", async () => {
      const res = await request(app)
        .get("/api/caussenards/"+ caussenard1.id + "/teams/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.length).toEqual(3);
      expect(res.body.content[0].id).toEqual(team1.id);
      expect(res.body.content[1].id).toEqual(team2.id);
      expect(res.body.content[2].id).toEqual(team3.id);
    });

    describe("Test request order", () => {
      each([
        ["number:ASC"   , 0, 1, 2],
        ["number:DESC"  , 2, 1, 0],
        ["name:ASC"     , 1, 2, 0],
        ["name:DESC"    , 1, 0, 2],
      ]).test("Test with order : '%s'", async (str, idOfT1, idOfT2, idOfT3) => {
        const res = await request(app)
          .get("/api/caussenards/"+ caussenard1.id + "/teams/")
          .query({order: str})
          .set({"Authorization": tokens.verifUser});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(200);
        expect(res.body.result).toEqual("success");
        expect(res.body.content.length).toEqual(3);
        expect(res.body.content[idOfT1].id).toEqual(team1.id);
        expect(res.body.content[idOfT2].id).toEqual(team2.id);
        expect(res.body.content[idOfT3].id).toEqual(team3.id);
      });
    });

    describe("Test request limit", () => {
      each([
        [0, 0],
        [1, 1],
        [3, 3],
        [6, 3],
      ]).test("Test with limit : '%d'", async (aLimit, listSize) => {
        const res = await request(app)
          .get("/api/caussenards/"+ caussenard1.id + "/teams/")
          .query({limit: aLimit})
          .set({"Authorization": tokens.verifUser});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(200);
        expect(res.body.result).toEqual("success");
        expect(res.body.content.length).toEqual(listSize);
      });  
    });

    describe("Test request offset", () => {
      each([
        [0, 3],
        [1, 2],
        [3, 0],
        [5, 0],
      ]).test("Test with limit : '%d'", async (anOffset, listSize) => {
        const res = await request(app)
          .get("/api/caussenards/"+ caussenard1.id + "/teams/")
          .query({offset: anOffset})
          .set({"Authorization": tokens.verifUser});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(200);
        expect(res.body.result).toEqual("success");
        expect(res.body.content.length).toEqual(listSize);
      });  
    });
  }
};