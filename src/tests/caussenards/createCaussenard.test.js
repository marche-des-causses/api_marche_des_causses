const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });
    
    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",401],
      ["editer",   201],
      ["manager",  201],
      ["admin",    201],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .post("/api/caussenards")
        .send({firstname: "Prénom", lastname: "Nom", gender: 1})
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });
    
    test("Test without lastname", async () => {
      const res = await request(app)
        .post("/api/caussenards")
        .send({firstname: "Prénom", gender: 1})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.miss_param);
    });

    test("Test without firstname", async () => {
      const res = await request(app)
        .post("/api/caussenards")
        .send({lastname: "Nom", gender: 1})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.miss_param);
    });
    
    test("Test without gender", async () => {
      const res = await request(app)
        .post("/api/caussenards")
        .send({firstname: "Prénom", lastname: "Nom"})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.miss_param);
    });

    test("Test with existing caussenards", async () => {
      await models.Caussenard.create({firstname: "Prénom", lastname: "Nom", gender: 1});
      const res = await request(app)
        .post("/api/caussenards")
        .send({firstname: "Prénom", lastname: "Nom", gender: 1})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(409);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.already_exist);
    });

    test("Test creation", async () => {
      const res = await request(app)
        .post("/api/caussenards")
        .send({firstname: "Prénom", lastname: "Nom", gender: 1})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(201);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.firstname).toEqual("Prénom");
      expect(res.body.content.lastname).toEqual("Nom");
    });
  }
};