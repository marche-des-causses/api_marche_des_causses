
module.exports = {
  test: function() {

    describe("Create new caussenard", require("./createCaussenard.test").test);

    describe("Count number of caussenards", require("./getCaussenardCount.test").test);

    describe("Get list of caussenards", require("./getCaussenardList.test").test);

    describe("Get caussenard informations", require("./getCaussenard.test").test);

    describe("Update caussenard informations", require("./updateCaussenard.test").test);

    describe("Delete caussenard", require("./deleteCaussenard.test").test);

    describe("Get teams of the caussenard", require("./getTeams.test").test);

    describe("Get years of the caussenard", require("./getYears.test").test);
  }
};
