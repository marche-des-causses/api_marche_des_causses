const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    var caussenard;
    var year;
    var team;

    beforeEach( async () => {
      caussenard = await models.Caussenard.create(
        {firstname: "Prénom", lastname: "Nom", gender: 1});
      year = await models.Year.create(
        {number: 2020});
      team = await models.Team.create(
        {yearId: year.id, number: 1, name: "la team"});
      await models.Member.create(
        {teamId: team.id, caussenardId: caussenard.id});
    });
    
    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",401],
      ["editer",   200],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .delete("/api/members/unlink")
        .send({teamId: team.id, caussenardId: caussenard.id})
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });
    
    describe("Test parameter : teamId", () => {
      test("Test without teamId", async () => {
        const res = await request(app)
          .delete("/api/members/unlink")
          .send({caussenardId: caussenard.id})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.miss_param);
      });

      test("Test with string teamId", async () => {
        const res = await request(app)
          .delete("/api/members/unlink")
          .send({teamId: "lala", caussenardId: caussenard.id})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.wrong_param);
      });

      test("Test with negative teamId", async () => {
        const res = await request(app)
          .delete("/api/members/unlink")
          .send({teamId: -3, caussenardId: caussenard.id})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.wrong_param);
      });

      test("Test with an unexisting team", async () => {
        const res = await request(app)
          .delete("/api/members/unlink")
          .send({teamId: team.id + 100, caussenardId: caussenard.id})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(404);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.not_exist);
      });
    });

    describe("Test parameter : caussenardId", () => {
      test("Test without caussenardId", async () => {
        const res = await request(app)
          .delete("/api/members/unlink")
          .send({teamId: team.id})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.miss_param);
      });

      test("Test with string caussenardId", async () => {
        const res = await request(app)
          .delete("/api/members/unlink")
          .send({caussenardId: "lala", teamId: team.id})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.wrong_param);
      });

      test("Test with negative caussenardId", async () => {
        const res = await request(app)
          .delete("/api/members/unlink")
          .send({caussenardId: -3, teamId: team.id})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.wrong_param);
      });

      test("Test with an unexisting caussenard", async () => {
        const res = await request(app)
          .delete("/api/members/unlink")
          .send({teamId: team.id, caussenardId: caussenard.id + 100})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(404);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.not_exist);
      });
  
    });

    test("Test with an unexisting member", async () => {
      var team2 = await models.Team.create(
        {yearId: year.id, number: 2, name: "la team"});
      const res = await request(app)
        .delete("/api/members/unlink")
        .send({teamId: team2.id, caussenardId: caussenard.id})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.not_exist);
    });

    test("Test delete", async () => {
      const res = await request(app)
        .delete("/api/members/unlink")
        .send({teamId: team.id, caussenardId: caussenard.id})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      const verif = await models.Member.count(
        {where: {teamId: team.id, caussenardId: caussenard.id}});
      expect(verif).toEqual(0);

    });
  }
};