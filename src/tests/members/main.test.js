
module.exports = {
  test: function() {

    describe("Get members associations", require("./getMemberList.test").test);

    describe("Link Caussenard to Team", require("./link.test").test);

    describe("Unlink Caussenard from Team", require("./unlink.test").test);

  }
};
