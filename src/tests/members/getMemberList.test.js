const request = require("supertest");
var models = require("../../models");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    test("Test with no member created", async () => {
      const res = await request(app)
        .get("/api/members/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.length).toEqual(0);
    });

    test("Test with one member created", async () => {
      const year = await models.Year.create({number: 2020, theme: "le theme"});
      const team = await models.Team.create({yearId: year.id, number: 2});
      const caussenard = await models.Caussenard.create({firstname: "Prénom_", lastname: "Nom", gender: 1});
      await models.Member.create({teamId: team.id, caussenardId: caussenard.id});
      const res = await request(app)
        .get("/api/members/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toHaveLength(1);
    });

    describe("Test with several members", () => {

      var year1;
      var year2;
      var year3;
      var team1;
      var team2;
      var team3;
      var caussenard;

      beforeEach( async () => {
        year1 = await models.Year.create({number: 2020, theme: "le theme"});
        year2 = await models.Year.create({number: 2019, theme: "le theme"});
        year3 = await models.Year.create({number: 2018, theme: "le theme"});
        team1 = await models.Team.create({yearId: year1.id, number: 1, name: "la team"});
        team2 = await models.Team.create({yearId: year2.id, number: 1, name: "la team"});
        team3 = await models.Team.create({yearId: year3.id, number: 1, name: "la team"});
        caussenard = await models.Caussenard.create({firstname: "Prénom_", lastname: "Nom", gender: 1});
        await models.Member.create({teamId: team1.id, caussenardId: caussenard.id});
        await models.Member.create({teamId: team2.id, caussenardId: caussenard.id});
        await models.Member.create({teamId: team3.id, caussenardId: caussenard.id});
      });

      describe("Test request limit", () => {
        each([
          [0, 0],
          [1, 1],
          [3, 3],
          [6, 3],
        ]).test("Test with limit : '%d'", async (aLimit, listSize) => {
          const res = await request(app)
            .get("/api/members/")
            .query({limit: aLimit})
            .set({"Authorization": tokens.verifUser});
          expect(res.type).toEqual("application/json");
          expect(res.statusCode).toEqual(200);
          expect(res.body.result).toEqual("success");
          expect(res.body.content.length).toEqual(listSize);
        });  
      });

      describe("Test request offset", () => {
        each([
          [0, 3],
          [1, 2],
          [3, 0],
          [5, 0],
        ]).test("Test with limit : '%d'", async (anOffset, listSize) => {
          const res = await request(app)
            .get("/api/members/")
            .query({offset: anOffset})
            .set({"Authorization": tokens.verifUser});
          expect(res.type).toEqual("application/json");
          expect(res.statusCode).toEqual(200);
          expect(res.body.result).toEqual("success");
          expect(res.body.content).toHaveLength(listSize);
        });  
      });
    });
  }
};