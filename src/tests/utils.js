var jwt  = require("jsonwebtoken");

var tokens = {};


module.exports = {

  initTokens: function () {
    tokens.client = jwt.sign({ authLevel: 0 },
      process.env.JWT_SIGN_SECRET, { expiresIn: "10m" });
    tokens.user = jwt.sign({ authLevel: 1 },
      process.env.JWT_SIGN_SECRET, { expiresIn: "10m" });
    tokens.verifUser = jwt.sign({ authLevel: 2 },
      process.env.JWT_SIGN_SECRET, { expiresIn: "10m" });
    tokens.editer = jwt.sign({ authLevel: 3 },
      process.env.JWT_SIGN_SECRET, { expiresIn: "10m" });
    tokens.manager = jwt.sign({ authLevel: 4 },
      process.env.JWT_SIGN_SECRET, { expiresIn: "10m" });
    tokens.admin = jwt.sign({ authLevel: 5 },
      process.env.JWT_SIGN_SECRET, { expiresIn: "10m" });
    tokens.expired = jwt.sign({ authLevel: 0 },
      process.env.JWT_SIGN_SECRET, { expiresIn: "1ms" });
    tokens.wrong = "duzgauohfzhoc";
  },

  getTokens: function () {
    return tokens;
  }

};