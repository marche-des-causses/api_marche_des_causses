var models = require("../models");
var usersTest       = require("./users/main.test");
var yearsTest       = require("./years/main.test");
var teamsTest       = require("./teams/main.test");
var caussenardsTest = require("./caussenards/main.test");
var membersTest     = require("./members/main.test");

describe("Tests of the API", () => {

  beforeAll(async () => {
    require("../server");
    require("./utils").initTokens();
  });

  afterAll(async () => {
    await require("../server").shutDown();
  });

  beforeEach(async () => {
    await models.Member.destroy({where: {}});
    await models.User.destroy({where: {}});
    await models.Caussenard.destroy({where: {}});
    await models.Team.destroy({where: {}});
    await models.Year.destroy({where: {}});
  });

  describe("Users tests", usersTest.test);
  
  describe("Years tests", yearsTest.test);

  describe("Teams tests", teamsTest.test);
  
  describe("Caussenards tests", caussenardsTest.test);

  describe("Member tests", membersTest.test);

});


