const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    var year;
    var team;
    
    beforeEach( async () => {
      year = await models.Year.create({number: 2020,theme: "un theme"});
      team = await models.Team.create({yearId: year.id , number: 1});
    });

    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",401],
      ["editer",   401],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .delete("/api/teams/" + team.id)
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test to delete an unexistiting team", async () => {
      const res = await request(app)
        .delete("/api/teams/"+ (team.id + 1))
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.not_exist);
    });

    test("Test param id", async () => {
      const res = await request(app)
        .delete("/api/teams/lala")
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.wrong_param);
    });

    test("Test delete team", async () => {
      const res = await request(app)
        .delete("/api/teams/"+ team.id)
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.id).toEqual(team.id);
      expect(await models.Team.findOne({where: {id: team.id}})).toBeNull();
    });

    test("Test recursive delete", async () => {
      var caussenard1 = await models.Caussenard.create({firstname:"aPrenom", lastname:"aNom", gender: 1}, {fields: models.Caussenard.classicFields});
      var caussenard2 = await models.Caussenard.create({firstname:"aPrenom", lastname:"bNom", gender: 1}, {fields: models.Caussenard.classicFields});
      var caussenard3 = await models.Caussenard.create({firstname:"bPrenom", lastname:"bNom", gender: 1}, {fields: models.Caussenard.classicFields});
      await models.Member.create({teamId: team.id, caussenardId: caussenard1.id}, {fields: models.Member.basicFields});
      await models.Member.create({teamId: team.id, caussenardId: caussenard2.id}, {fields: models.Member.basicFields});
      await models.Member.create({teamId: team.id, caussenardId: caussenard3.id}, {fields: models.Member.basicFields});

      const res = await request(app)
        .delete("/api/teams/"+ team.id)
        .set({"Authorization": tokens.manager});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.id).toEqual(team.id);
      expect(await models.Year.findOne({where: {id: year.id}})).not.toBeNull();
      expect(await models.Team.findOne({where: {id: team.id}})).toBeNull();
      expect(await models.Caussenard.findOne({where: {id: caussenard1.id}})).not.toBeNull();
      expect(await models.Caussenard.findOne({where: {id: caussenard2.id}})).not.toBeNull();
      expect(await models.Caussenard.findOne({where: {id: caussenard3.id}})).not.toBeNull();
      expect(await models.Member.findOne({where: {teamId: team.id}})).toBeNull();
      
    });
  }
};