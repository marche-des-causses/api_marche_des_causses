const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;
    var year;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    beforeEach( async () => {
      year = await models.Year.create({number: 2020});
    });
    
    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",401],
      ["editer",   201],
      ["manager",  201],
      ["admin",    201],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .post("/api/teams")
        .send({yearId: year.id, number: 1, name:"Name of the Team"})
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json"); 
      expect(res.statusCode).toEqual(statusCode);
    });
    
    describe("Test parameter : yearId", () => {
      test("Test without yearId", async () => {
        const res = await request(app)
          .post("/api/teams")
          .send({number: 2})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.miss_param);
      });
    });

    describe("Test parameter : number", () => {
      test("Test without number", async () => {
        const res = await request(app)
          .post("/api/teams")
          .send({yearId: year.id})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.miss_param);
      });
    });

    test("Test with unexisting year", async () => {
      const res = await request(app)
        .post("/api/teams")
        .send({yearId: year.id+1, number: 1})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.not_exist);
    });

    test("Test with existing team", async () => {
      await models.Team.create({yearId: year.id, number: 1});
      const res = await request(app)
        .post("/api/teams")
        .send({yearId: year.id, number: 1})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(409);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.already_exist);
    });

    test("Test creation", async () => {
      const res = await request(app)
        .post("/api/teams")
        .send({yearId: year.id, number: 1, name:"Name of the Team"})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(201);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.yearId).toEqual(year.id);
      expect(res.body.content.number).toEqual(1);
      expect(res.body.content.name).toEqual("Name of the Team");
    });
  }
};