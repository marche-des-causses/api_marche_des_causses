const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    var year1;
    var year2;
    var team;
    
    beforeEach( async () => {
      year1 = await models.Year.create({number: 2020,theme: "un theme"});
      year2 = await models.Year.create({number: 2019,theme: "un theme"});
      team = await models.Team.create({yearId: year1.id , number: 1, name: "l'équipe"});
    });

    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",401],
      ["editer",   200],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .put("/api/teams/" + team.id)
        .send({yearId: year1.id, number: 2, name: "l'équipe_"})
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test to update an unexistiting team", async () => {
      const res = await request(app)
        .put("/api/teams/"+ (team.id + 1))
        .send({yearId: year2.id , number: 2, name: "l'équipe_"})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.not_exist);
    });

    test("Test to update a team to an existing team", async () => {
      await models.Team.create({yearId: year2.id , number: 2, name: "l'équipe_"});
      const res = await request(app)
        .put("/api/teams/"+ team.id + "/")
        .send({yearId: year2.id , number: 2, name: "l'équipe_"})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(409);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.already_exist);
    });

    test("Test param id", async () => {
      const res = await request(app)
        .put("/api/teams/lala")
        .send({yearId: year2.id , number: 2, name: "l'équipe_"})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.wrong_param);
    });

  }
};