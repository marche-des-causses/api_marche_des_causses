const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    each([
      ["client",   200],
      ["user",     200],
      ["verifUser",200],
      ["editer",   200],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const year = await models.Year.create({number: 2020, theme: "le theme"}, {fields: models.Year.basicFields});
      const team = await models.Team.create({name: "La team", number: 3, yearId:year.id}, {fields: models.Team.basicFields});
      const res = await request(app)
        .get("/api/teams/" + team.id)
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    describe("Test parameter : teamId", () => {      
      test("Test with non-integer teamId", async () => {
        const res = await request(app)
          .get("/api/teams/ok/")
          .set({"Authorization": tokens.verifUser});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.wrong_param);
      });
    });

    test("Get an unexisting team", async () => {
      const res = await request(app)
        .get("/api/teams/" + 1)
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.not_exist);
    });

    test("Get a team", async () => {
      const year = await models.Year.create({number: 2020, theme: "le theme"}, {fields: models.Year.basicFields});
      const team = await models.Team.create({name: "La team", number: 3, yearId:year.id}, {fields: models.Team.basicFields});
      const caussenard1 = await models.Caussenard.create({firstname:"aPrenom", lastname:"aNom", gender: 1}, {fields: models.Caussenard.classicFields});
      const caussenard2 = await models.Caussenard.create({firstname:"aPrenom", lastname:"bNom", gender: 1}, {fields: models.Caussenard.classicFields});
      const caussenard3 = await models.Caussenard.create({firstname:"bPrenom", lastname:"bNom", gender: 1}, {fields: models.Caussenard.classicFields});
      await models.Member.create({teamId: team.id, caussenardId: caussenard1.id});
      await models.Member.create({teamId: team.id, caussenardId: caussenard2.id});
      await models.Member.create({teamId: team.id, caussenardId: caussenard3.id});
      const res = await request(app)
        .get("/api/teams/" + team.id)
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.name).toEqual("La team");
      expect(res.body.content.number).toEqual(3);
      expect(res.body.content.year.number).toEqual(2020);
      expect(res.body.content.year.theme).toEqual("le theme");
      expect(res.body.content.members).toHaveLength(3);
    });

  }
};