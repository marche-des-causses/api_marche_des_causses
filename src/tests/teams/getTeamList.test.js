const request = require("supertest");
var models = require("../../models");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    each([
      ["client",   200],
      ["user",     200],
      ["verifUser",200],
      ["editer",   200],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .get("/api/teams/")
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test with no team created", async () => {
      const res = await request(app)
        .get("/api/teams/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.length).toEqual(0);
    });

    test("Test with one team created", async () => {
      const year = await models.Year.create({number: 2020, theme: "le theme"});
      await models.Team.create({yearId: year.id, number: 2});
      const res = await request(app)
        .get("/api/teams/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toHaveLength(1);
    });

    describe("Test with several teams", () => {

      var year1;
      var year2;
      var team1;
      var team2;
      var team3;

      beforeEach( async () => {
        year1 = await models.Year.create({number: 2020, theme: "le theme"});
        year2 = await models.Year.create({number: 2019, theme: "le theme"});
        team1 = await models.Team.create({yearId: year1.id, number: 1, name: "une team"});
        team2 = await models.Team.create({yearId: year1.id, number: 2, name: "la team"});
        team3 = await models.Team.create({yearId: year2.id, number: 1, name: "la team"});
      });

      describe("Test request order", () => {
        each([
          ["year:number:ASC;number:ASC"   , 1, 2, 0],
          ["year:number:DESC;number:DESC" , 1, 0, 2],
          ["year:number:ASC;number:DESC"  , 2, 1, 0],
          ["year:number:DESC;number:ASC"  , 0, 1, 2],
          ["year:number:DESC;name:ASC"    , 1, 0, 2],
          ["name:DESC"                    , 0, 1, 2],
          ["name:ASC"                     , 2, 0, 1],
        ]).test("Test with order : '%s'", async (str, idOf1, idOf2, idOf3) => {
          const res = await request(app)
            .get("/api/teams/")
            .query({order: str})
            .set({"Authorization": tokens.verifUser});
          expect(res.type).toEqual("application/json");
          expect(res.statusCode).toEqual(200);
          expect(res.body.result).toEqual("success");
          expect(res.body.content).toHaveLength(3);
          expect(res.body.content[idOf1].id).toEqual(team1.id);
          expect(res.body.content[idOf2].id).toEqual(team2.id);
          expect(res.body.content[idOf3].id).toEqual(team3.id);
        });
      });

      describe("Test request limit", () => {
        each([
          [0, 0],
          [1, 1],
          [3, 3],
          [6, 3],
        ]).test("Test with limit : '%d'", async (aLimit, listSize) => {
          const res = await request(app)
            .get("/api/teams/")
            .query({limit: aLimit})
            .set({"Authorization": tokens.verifUser});
          expect(res.type).toEqual("application/json");
          expect(res.statusCode).toEqual(200);
          expect(res.body.result).toEqual("success");
          expect(res.body.content.length).toEqual(listSize);
        });  
      });

      describe("Test request offset", () => {
        each([
          [0, 3],
          [1, 2],
          [3, 0],
          [5, 0],
        ]).test("Test with limit : '%d'", async (anOffset, listSize) => {
          const res = await request(app)
            .get("/api/teams/")
            .query({offset: anOffset})
            .set({"Authorization": tokens.verifUser});
          expect(res.type).toEqual("application/json");
          expect(res.statusCode).toEqual(200);
          expect(res.body.result).toEqual("success");
          expect(res.body.content).toHaveLength(listSize);
        });  
      });
    });
  }
};