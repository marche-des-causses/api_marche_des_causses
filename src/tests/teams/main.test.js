
module.exports = {
  test: function() {

    describe("Create new team", require("./createTeam.test").test);

    describe("Count number of teams", require("./getCount.test").test);

    describe("Get list of teams", require("./getTeamList.test").test);

    describe("Get team informations", require("./getTeam.test").test);

    describe("Update team informations", require("./updateTeam.test").test);

    describe("Delete team", require("./deleteTeam.test").test);

    describe("Get participants of the team", require("./getMembers.test").test);
  }
};
