const request = require("supertest");
var models = require("../../models");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",401],
      ["editer",   401],
      ["manager",  401],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .get("/api/users/")
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test with no user created", async () => {
      const res = await request(app)
        .get("/api/users/")
        .set({"Authorization": tokens.admin});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.length).toEqual(0);
    });

    test("Test with one user created", async () => {
      await models.User.create({
        email: "email@user.test",
        password: "password",
        authLevel: 1
      });
      const res = await request(app).get("/api/users/")
        .set({"Authorization": tokens.admin});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toHaveLength(1);
    });

    describe("Test with several users", () => {

      var user1;
      var user2;
      var user3;

      beforeEach( async () => {
        user1 = await models.User.create(
          {email: "aemail@user.test", password: "password", authLevel: 1});
        user2 = await models.User.create(
          {email: "aemail@user.test", password: "password", authLevel: 2});
        user3 = await models.User.create(
          {email: "bemail@user.test", password: "password", authLevel: 2});
      });

      describe("Test request order", () => {
        each([
          ["email:DESC;authLevel:ASC" , 1, 2, 0],
          ["email:ASC;authLevel:DESC" , 1, 0, 2],
          ["authLevel:DESC;email:ASC" , 2, 0, 1],
          ["authLevel:ASC;email:DESC" , 0, 2, 1],
        ]).test("Test with order : '%s'", async (str, idOf1, idOf2, idOf3) => {
          const res = await request(app)
            .get("/api/users/")
            .query({order: str})
            .set({"Authorization": tokens.admin});
          expect(res.type).toEqual("application/json");
          expect(res.statusCode).toEqual(200);
          expect(res.body.result).toEqual("success");
          expect(res.body.content).toHaveLength(3);
          expect(res.body.content[idOf1].id).toEqual(user1.id);
          expect(res.body.content[idOf2].id).toEqual(user2.id);
          expect(res.body.content[idOf3].id).toEqual(user3.id);
        });
      });

      describe("Test request limit", () => {
        each([
          [0, 0],
          [1, 1],
          [3, 3],
          [6, 3],
        ]).test("Test with limit : '%d'", async (aLimit, listSize) => {
          const res = await request(app)
            .get("/api/users/")
            .query({limit: aLimit})
            .set({"Authorization": tokens.admin});
          expect(res.type).toEqual("application/json");
          expect(res.statusCode).toEqual(200);
          expect(res.body.result).toEqual("success");
          expect(res.body.content.length).toEqual(listSize);
        });  
      });

      describe("Test request offset", () => {
        each([
          [0, 3],
          [1, 2],
          [3, 0],
          [5, 0],
        ]).test("Test with limit : '%d'", async (anOffset, listSize) => {
          const res = await request(app)
            .get("/api/users/")
            .query({offset: anOffset})
            .set({"Authorization": tokens.admin});
          expect(res.type).toEqual("application/json");
          expect(res.statusCode).toEqual(200);
          expect(res.body.result).toEqual("success");
          expect(res.body.content).toHaveLength(listSize);
        });  
      });
    });
  }
};