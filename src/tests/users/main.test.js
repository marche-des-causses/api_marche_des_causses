
module.exports = {
  test: function() {

    describe("Registration of a user", require("./register.test").test);

    describe("Login a user", require("./login.test").test);

    describe("Get user informations", require("./getUserInfos.test").test);

    describe("Update user informations", require("./updateUserProfile.test").test);

    describe("Update my informations", require("./updateMyProfile.test").test);

    describe("Delete user", require("./deleteUser.test").test);

    describe("Get user list", require("./getList.test").test);

    describe("Count users", require("./getCount.test").test);
  }
};
