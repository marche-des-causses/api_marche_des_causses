const request = require("supertest");
const models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;
const jwt = require("jsonwebtoken");

module.exports = {
  test: function() {

    const email = "email@user.test";
    const password = "randompassword";

    var tokens;
    var user;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    beforeEach( async () => {
      user = await models.User.create({
        email: email,
        password: password,
        authLevel: 1
      });
    });
    
    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",401],
      ["editer",   401],
      ["manager",  401],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .get("/api/users/" + user.id)
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test with unexisting user", async () => {
      const res = await request(app)
        .get("/api/users/" + (user.id + 100))
        .set({"Authorization": tokens["admin"]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.error.code).toEqual(errno.not_exist);
    });

    test("Test response for id param", async () => {
      const res = await request(app)
        .get("/api/users/" + user.id)
        .set({"Authorization": tokens["admin"]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.content.email).toEqual(email);
    });

    test("Test response for me param", async () => {
      var token = jwt.sign({
        userId: user.id,
        authLevel: user.authLevel
      },
      process.env.JWT_SIGN_SECRET,
      {
        expiresIn: "3s"
      });
      const res = await request(app)
        .get("/api/users/me")
        .set({"Authorization": token});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.content.email).toEqual(email);
    });

  }
};