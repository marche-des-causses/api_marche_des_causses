const request = require("supertest");
const models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;
const bcrypt    = require("bcrypt");

module.exports = {
  test: function() {

    const good_password = "Test1337";
    const good_email = "email@user.test";

    beforeEach( async () => {
      const bcryptedPassword = bcrypt.hashSync(good_password, 5);
      await models.User.create({
        email: good_email,
        password: bcryptedPassword,
        authLevel: 1
      });
    });

    test("Test without email parameter", async () => {
      const res = await request(app)
        .post("/api/users/login/")
        .send({password: good_password});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.error.code).toEqual(errno.miss_param);
    });
      
    each([
      [good_email, 200],
      ["email@.test", 400],
      ["email@register.t", 400],
      ["em@il@register.test", 400],
      ["ema)il@register.test", 400],
      ["okcestmoi", 400],
      ["wrongemail@user.fr", 404],
    ]).test("Test email parameter : '%s'", async (email, statusCode) => {
      const res = await request(app)
        .post("/api/users/login/")
        .send({email: email, password: good_password});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test without password parameter", async () => {
      const res = await request(app)
        .post("/api/users/login/")
        .send({email: good_email});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.error.code).toEqual(errno.miss_param);
    });

    each([
      [good_password, 200],
      ["te", 400],              // Too small
      ["ceciestlemotdepassebeaucouptroplong", 400],  // Too long
      ["testdemotdepasse", 400],// Without Number
      ["TEST123456", 400],      // Without lowercase letter
      ["test123456", 400],      // Without upercase letter
      ["WrongPassword1", 403],      // Without upercase letter
    ]).test("Test password parameter : '%s'", async (password, statusCode) => {
      const res = await request(app)
        .post("/api/users/login/")
        .send({email: good_email, password: password});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test response content", async () => {
      const res = await request(app)
        .post("/api/users/login/")
        .send({email: good_email, password: good_password});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.content.token).not.toBeUndefined();
      expect(res.body.content.user).not.toBeUndefined();
      expect(res.body.content.user.email).toEqual(good_email);
    });
   
  }
};