const request = require("supertest");
const models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    const email = "email@user.test";
    const newEmail = "newemail@user.test";
    const authLevel = 1;
    const newAuthLevel = 2;
    const password = "Passw0rd";
    const newPassword = "NewPassw0rd";

    var tokens;
    var user;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    beforeEach( async () => {
      user = await models.User.create({
        email: email,
        password: password,
        authLevel: authLevel
      });
    });

    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",401],
      ["editer",   401],
      ["manager",  401],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ])
      .test("Authentification Test : '%s'", async (token, statusCode) => {
        const res = await request(app)
          .put("/api/users/" + user.id)
          .send({email: newEmail})
          .set({"Authorization": tokens[token]});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(statusCode);
      });
 
    test("Test with unexisting user", async () => {
      const res = await request(app)
        .put("/api/users/" + (user.id + 100))
        .send({email: newEmail})
        .set({"Authorization": tokens.admin});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.error.code).toEqual(errno.not_exist);
    });

    each([
      [{email: newEmail, password: newPassword, authLevel: newAuthLevel} , 200, null],
      [{email: newEmail} , 200, null],
      [{email: "email@.test"} , 400, errno.wrong_param],
      [{email: "email@register.t"} , 400, errno.wrong_param],
      [{email: "em@il@register.test"} , 400, errno.wrong_param],
      [{email: "ema)il@register.test"} , 400, errno.wrong_param],
      [{email: "okcestmoi"} , 400, errno.wrong_param],
      [{password: newPassword} , 200, null],
      [{password: "te"} , 400, errno.wrong_param],
      [{password: "ceciestlemotdepassebeaucouptroplong"} , 400, errno.wrong_param],
      [{password: "testdemotdepasse"} , 400, errno.wrong_param],
      [{password: "TEST123456"} , 400, errno.wrong_param],
      [{password: "test123456"} , 400, errno.wrong_param],
      [{password: "test123456"} , 400, errno.wrong_param],
      [{authLevel: newAuthLevel} , 200, null],
      [{authLevel: -1} , 400, errno.wrong_param],
      [{authLevel: 6} , 400, errno.wrong_param],
      [{authLevel: "lala"} , 400, errno.wrong_param],
      [{email: null, password: null, authLevel: null} , 200, null],
      [{}, 200, null]
    ])
      .test("Test request body: %o", async (body, statusCode, code) => {
        const res = await request(app)
          .put("/api/users/"+ user.id)
          .send(body)
          .set({"Authorization": tokens.admin});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(statusCode);
        if (statusCode == 200) {
          expect(res.body.result).toEqual("success");
          if (body.email)
            expect(res.body.content.email).toEqual( body.email );
          if (body.authLevel)
            expect(res.body.content.authLevel).toEqual( body.authLevel );
          var newUser = await models.User.findOne({where: {id: user.id}});
          expect(newUser.email).toEqual( (body.email == undefined) ? user.email : body.email );
          expect(newUser.authLevel).toEqual( (body.authLevel == undefined) ? user.authLevel : body.authLevel );
        } else {
          expect(res.body.error.code).toEqual(code);
        }
      });
  }
};