const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    const good_password = "Test1337";
    const good_email = "email@register.test";

    test("Test without email parameter", async () => {
      const res = await request(app)
        .post("/api/users/register/")
        .send({password: good_password});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.error.code).toEqual(errno.miss_param);
    });
      
    each([
      [good_email, 201],
      ["email@.test", 400],
      ["email@register.t", 400],
      ["em@il@register.test", 400],
      ["ema)il@register.test", 400],
      ["okcestmoi", 400],
    ]).test("Test email parameter : '%s'", async (email, statusCode) => {
      const res = await request(app)
        .post("/api/users/register/")
        .send({email: email, password: good_password});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test without password parameter", async () => {
      const res = await request(app)
        .post("/api/users/register/")
        .send({email: good_email});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.error.code).toEqual(errno.miss_param);
    });

    each([
      [good_password, 201],
      ["te", 400],              // Too small
      ["ceciestlemotdepassebeaucouptroplong", 400],  // Too long
      ["testdemotdepasse", 400],// Without Number
      ["TEST123456", 400],      // Without lowercase letter
      ["test123456", 400],      // Without upercase letter
    ]).test("Test password parameter : '%s'", async (password, statusCode) => {
      const res = await request(app)
        .post("/api/users/register/")
        .send({email: good_email, password: password});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test with an existing user", async () => {
      await models.User.create({
        email: good_email, 
        password: good_password,
        authLevel: 1
      });
      const res = await request(app)
        .post("/api/users/register/")
        .send({email: good_email, password: good_password});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(409);
      expect(res.body.error.code).toEqual(errno.already_exist);
    }),

    test("Test response", async () => {
      const res = await request(app)
        .post("/api/users/register/")
        .send({email: good_email, password: good_password});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(201);
      expect(res.body.content.email).toEqual(good_email);
      expect(res.body.content.password).not.toEqual(good_password);
      expect(res.body.content.authLevel).toEqual(1);
    });
  }
};