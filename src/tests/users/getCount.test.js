const request = require("supertest");
var models = require("../../models");
const app = require("../../server.js");

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });
    
    test("Test with no user created", async () => {
      const res = await request(app)
        .get("/api/users?count=true")
        .set({"Authorization": tokens.admin});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toEqual(0);
    });

    test("Test with one user created", async () => {
      await models.User.create(
        {email: "cemail@user.test", password: "password", authLevel: 2});
      const res = await request(app)
        .get("/api/users?count=true")
        .set({"Authorization": tokens.admin});
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toEqual(1);
    });

    test("Test with three user created", async () => {
      await models.User.create(
        {email: "aemail@user.test", password: "password", authLevel: 2});
      await models.User.create(
        {email: "bemail@user.test", password: "password", authLevel: 2});
      await models.User.create(
        {email: "cemail@user.test", password: "password", authLevel: 2});
      const res = await request(app).get("/api/users?count=true")
        .set({"Authorization": tokens.admin});
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toEqual(3);
    });

  }
};