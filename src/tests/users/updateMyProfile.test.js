const request = require("supertest");
const models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;
const bcrypt    = require("bcrypt");
var jwt = require("jsonwebtoken");

module.exports = {
  test: function() {

    const email = "email@user.test";
    const newEmail = "newemail@user.test";
    const authLevel = 1;
    const password = "Passw0rd";
    var bcryptPassword;
    const newPassword = "NewPassw0rd";

    var user;
    var token;

    beforeAll( async () => {
      await bcrypt.hash(password, 5, function( err, bp) {
        bcryptPassword = bp;
      });
    });

    beforeEach( async () => {
      user = await models.User.create({
        email: email,
        password: bcryptPassword,
        authLevel: authLevel
      });
      token = jwt.sign({
        userId: user.id,
        authLevel: user.authLevel
      },
      process.env.JWT_SIGN_SECRET,
      {
        expiresIn: "2m"
      });
    });
 
    each([
      [{email: newEmail, newPassword: newPassword, lastPassword: password} , 200, null],
      [{newPassword: newPassword, lastPassword: password} , 200, null],
      [{email: newEmail} , 200, null],
      [{email: "email@.test"} , 400, errno.wrong_param],
      [{email: "email@register.t"} , 400, errno.wrong_param],
      [{email: "em@il@register.test"} , 400, errno.wrong_param],
      [{email: "ema)il@register.test"} , 400, errno.wrong_param],
      [{email: "okcestmoi"} , 400, errno.wrong_param],
      [{newPassword: newPassword} , 400, errno.miss_param],
      [{newPassword: "te"} , 400, errno.wrong_param],
      [{newPassword: "ceciestlemotdepassebeaucouptroplong"} , 400, errno.wrong_param],
      [{newPassword: "testdemotdepasse"} , 400, errno.wrong_param],
      [{newPassword: "TEST123456"} , 400, errno.wrong_param],
      [{newPassword: "test123456"} , 400, errno.wrong_param],
      [{newPassword: "test123456"} , 400, errno.wrong_param],
      [{email: null, newPassword: null, lastPassword: null} , 200, null],
      [{}, 200, null]
    ])
      .test("Test request body: %o", async (body, statusCode, code) => {
        const res = await request(app)
          .put("/api/users/me")
          .send(body)
          .set({"Authorization": token});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(statusCode);
        if (statusCode == 200) {
          expect(res.body.result).toEqual("success");
          if (body.email)
            expect(res.body.content.email).toEqual( body.email );
          var newUser = await models.User.findOne({where: {id: user.id}});
          expect(newUser.email).toEqual( (body.email == undefined) ? user.email : body.email );
        } else {
          expect(res.body.error.code).toEqual(code);
        }
      });
  }
};