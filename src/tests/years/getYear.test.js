const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    each([
      ["client",   200],
      ["user",     200],
      ["verifUser",200],
      ["editer",   200],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const year = await models.Year.create({number: 2020, theme: "le theme"}, {fields: models.Year.basicFields});
      const res = await request(app)
        .get("/api/years/" + year.id)
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    describe("Test parameter : yearId", () => {      
      test("Test with non-integer yearId", async () => {
        const res = await request(app)
          .get("/api/years/ok/")
          .set({"Authorization": tokens.verifUser});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.wrong_param);
      });
    });

    test("Get an unexisting year", async () => {
      const res = await request(app)
        .get("/api/years/" + 1)
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.not_exist);
    });

    test("Get a year", async () => {
      const year = await models.Year.create({number: 2020, theme: "le theme"}, {fields: models.Year.basicFields});
      const team1 = await models.Team.create({name: "team 1", number: 1, yearId:year.id}, {attributes: models.Team.basicFields});
      const team2 = await models.Team.create({name: "team 2", number: 2, yearId:year.id}, {fields: models.Team.basicFields});
      await models.Team.create({name: "team 3", number: 3, yearId:year.id}, {fields: models.Team.basicFields});
      const caussenard1 = await models.Caussenard.create({firstname:"aPrenom", lastname:"aNom", gender: 1}, {fields: models.Caussenard.classicFields});
      const caussenard2 = await models.Caussenard.create({firstname:"aPrenom", lastname:"bNom", gender: 1}, {fields: models.Caussenard.classicFields});
      const caussenard3 = await models.Caussenard.create({firstname:"bPrenom", lastname:"bNom", gender: 1}, {fields: models.Caussenard.classicFields});
      await models.Member.create({teamId: team1.id, caussenardId: caussenard1.id});
      await models.Member.create({teamId: team1.id, caussenardId: caussenard2.id});
      await models.Member.create({teamId: team2.id, caussenardId: caussenard3.id});
      const res = await request(app)
        .get("/api/years/" + year.id)
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content.number).toEqual(2020);
      expect(res.body.content.theme).toEqual("le theme");
      expect(res.body.content.teams.length).toEqual(3);
    });

  }
};