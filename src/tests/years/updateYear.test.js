const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    var year;
    
    beforeEach( async () => {
      year = await models.Year.create({
        number: 2020,
        theme: "un theme"
      });
    });

    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",401],
      ["editer",   200],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ])
      .test("Authentification Test : '%s'", async (token, statusCode) => {
        const res = await request(app)
          .put("/api/years/"+ year.id)
          .send({theme: "Le theme"})
          .set({"Authorization": tokens[token]});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(statusCode);
      });

    test("Test to update an unexistiting year", async () => {
      const res = await request(app)
        .put("/api/years/"+ (year.id + 1))
        .send({number: 2019, theme: "Le theme"})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.not_exist);
    });

    test("Test param id", async () => {
      const res = await request(app)
        .put("/api/years/lala")
        .send({number: 2019, theme: "Le theme"})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.wrong_param);
    });

    test("Test to update a year to an existing year", async () => {
      await models.Year.create({number: 2019});
      const res = await request(app)
        .put("/api/years/"+ year.id)
        .send({number: 2019, theme: "Le theme"})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(409);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.already_exist);
    });

    each([
      [{number: 2019, theme: "le nouveau theme"}, 200, null],
      [{number: 2019}, 200, null],
      [{number: "date"}, 400, errno.wrong_param],
      [{number: 1795}, 400, errno.wrong_param],
      [{number: 13657}, 400, errno.wrong_param],
      [{theme: "le nouveau theme"}, 200, null],
      [{}, 200, null]
    ])
      .test("Test to update year: %o", async (body, statusCode, code) => {
        const res = await request(app)
          .put("/api/years/"+ year.id)
          .send(body)
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(statusCode);
        if (statusCode == 200) {
          expect(res.body.result).toEqual("success");
          expect(res.body.content.number).toEqual( (body.number == undefined) ? year.number : body.number );
          expect(res.body.content.theme).toEqual( (body.theme == undefined) ? year.theme : body.theme );
          var newYear = await models.Year.findOne({where: {id: year.id}});
          expect(newYear.number).toEqual( (body.number == undefined) ? year.number : body.number );
          expect(newYear.theme).toEqual( (body.theme == undefined) ? year.theme : body.theme );
        } else {
          expect(res.body.error.code).toEqual(code);
        }
      });
  }
};