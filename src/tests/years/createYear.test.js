const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;


module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });

    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",401],
      ["editer",   201],
      ["manager",  201],
      ["admin",    201],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .post("/api/years")
        .send({number: 2020, theme: "un theme réussi"})
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    describe("Test parameter : number", () => {
      each([
        [-2005],
        [1975],
        [15630]
      ]).test("Test with wrong number: '%d'", async (nb) => {
        const res = await request(app)
          .post("/api/years")
          .send({number: nb, theme: "un theme"})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.wrong_param);
      });

      each([
        [1976],
        [2020]
      ]).test("Test with great number: ", async (nb) => {
        const res = await request(app)
          .post("/api/years")
          .send({number: nb, theme: "un theme"})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(201);
        expect(res.body.content.number).toEqual(nb);
      });
      
    });

    test("Test without number", async () => {
      const res = await request(app)
        .post("/api/years")
        .send({theme: "un theme"})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.miss_param);
    });

    describe("Test parameter : theme", () => {
      each([
        ["le"]
      ]).test("Test with wrong theme: '%s'", async (thm) => {
        const res = await request(app)
          .post("/api/years")
          .send({number: 2020, theme: thm})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(400);
        expect(res.body.result).toEqual("fail");
        expect(res.body.error.code).toEqual(errno.wrong_param);
      });

      each([
        ["un theme réussi"],
        ["un theme"]
      ]).test("Test with great number: ", async (thm) => {
        const res = await request(app)
          .post("/api/years")
          .send({number: 2020, theme: thm})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(201);
        expect(res.body.content.theme).toEqual(thm);
      });
      
      test("Test without theme", async () => {
        const res = await request(app)
          .post("/api/years")
          .send({number: 2020})
          .set({"Authorization": tokens.editer});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(201);
      });
    });
    
    test("Test to create an existing year", async () => {
      await models.Year.create({number: 2020});
      const res = await request(app)
        .post("/api/years")
        .send({number: 2020})
        .set({"Authorization": tokens.editer});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(409);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.already_exist);
    });

  }
};