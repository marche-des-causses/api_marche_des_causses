
module.exports = {
  test: function() {

    describe("Create new year", require("./createYear.test").test);

    describe("Count number of years", require("./getYearCount.test").test);

    describe("Get list of years", require("./getYearList.test").test);

    describe("Get year informations", require("./getYear.test").test);

    describe("Update year informations", require("./updateYear.test").test);

    describe("Delete year", require("./deleteYear.test").test);

    describe("Get teams of the year", require("./getTeams.test").test);

    describe("Get participants of the year", require("./getParticipants.test").test);
  }
};
