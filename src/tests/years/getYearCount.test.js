const request = require("supertest");
var models = require("../../models");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });
    
    each([
      ["client",   200],
      ["user",     200],
      ["verifUser",200],
      ["editer",   200],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .get("/api/years?count=true")
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test with no year created", async () => {
      const res = await request(app)
        .get("/api/years?count=true")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toEqual(0);
    });

    test("Test with one year created", async () => {
      await models.Year.create({number: 2020, theme: "le theme"});
      const res = await request(app)
        .get("/api/years?count=true")
        .set({"Authorization": tokens.verifUser});
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toEqual(1);
    });

    test("Test with three year created", async () => {
      await models.Year.create({number: 2020, theme: "le theme"});
      await models.Year.create({number: 2019, theme: "le theme"});
      await models.Year.create({number: 2018, theme: "le theme"});
      const res = await request(app)
        .get("/api/years?count=true")
        .set({"Authorization": tokens.verifUser});
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toEqual(3);
    });

  }
};