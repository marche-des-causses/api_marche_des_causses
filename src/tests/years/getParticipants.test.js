const request = require("supertest");
var models = require("../../models");
const errno = require("../../functions/errno");
const app = require("../../server.js");
const each = require("jest-each").default;

module.exports = {
  test: function() {

    var tokens;

    beforeAll( async () => {
      tokens = require("../utils").getTokens();
    });
    
    let year;
    let year2;
    let team1;
    let team2;
    let caussenard1;
    let caussenard2;
    let caussenard3;

    beforeEach(async () => {
      year = await models.Year.create({number: 2020, theme: "le theme"}, {fields: models.Year.basicFields});
      year2 = await models.Year.create({number: 2019, theme: "le theme"}, {fields: models.Year.basicFields});
      team1 = await models.Team.create({name: "team 1", number: 1, yearId:year.id}, {attributes: models.Team.basicFields});
      team2 = await models.Team.create({name: "team 2", number: 2, yearId:year.id}, {fields: models.Team.basicFields});
      caussenard1 = await models.Caussenard.create({firstname:"aPrenom", lastname:"aNom", gender:1}, {fields: models.Caussenard.classicFields});
      caussenard2 = await models.Caussenard.create({firstname:"aPrenom", lastname:"bNom", gender:1}, {fields: models.Caussenard.classicFields});
      caussenard3 = await models.Caussenard.create({firstname:"bPrenom", lastname:"bNom", gender:1}, {fields: models.Caussenard.classicFields});
      await models.Member.create({teamId: team1.id, caussenardId: caussenard1.id});
      await models.Member.create({teamId: team1.id, caussenardId: caussenard2.id});
      await models.Member.create({teamId: team2.id, caussenardId: caussenard3.id});
    });

    each([
      ["client",   401],
      ["user",     401],
      ["verifUser",200],
      ["editer",   200],
      ["manager",  200],
      ["admin",    200],
      ["expired",  498],
      ["wrong",    498],
    ]).test("Authentification Test : '%s'", async (token, statusCode) => {
      const res = await request(app)
        .get("/api/years/"+ year.id + "/caussenards/")
        .set({"Authorization": tokens[token]});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(statusCode);
    });

    test("Test with not existing year", async () => {
      const res = await request(app)
        .get("/api/years/"+ (year.id + 5) + "/caussenards/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(404);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.not_exist);
    });
    
    test("Test with existing year without participant", async () => {
      const res = await request(app)
        .get("/api/years/"+ year2.id + "/caussenards/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toHaveLength(0);
    });

    test("Test count without participant", async () => {
      const res = await request(app)
        .get("/api/years/"+ year2.id + "/caussenards?count=true")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toEqual(0);
    });

    test("Test param id", async () => {
      const res = await request(app)
        .get("/api/years/lala/caussenards/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(400);
      expect(res.body.result).toEqual("fail");
      expect(res.body.error.code).toEqual(errno.wrong_param);
    });

    test("Test with existing year", async () => {
      const res = await request(app)
        .get("/api/years/"+ year.id + "/caussenards/")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toHaveLength(3);
      expect(res.body.content[0].id).toEqual(caussenard1.id);
      expect(res.body.content[1].id).toEqual(caussenard2.id);
      expect(res.body.content[2].id).toEqual(caussenard3.id);
    });

    test("Test count", async () => {
      const res = await request(app)
        .get("/api/years/"+ year.id + "/caussenards?count=true")
        .set({"Authorization": tokens.verifUser});
      expect(res.type).toEqual("application/json");
      expect(res.statusCode).toEqual(200);
      expect(res.body.result).toEqual("success");
      expect(res.body.content).toEqual(3);
    });

    describe("Test request order", () => {
      each([
        ["firstname:ASC"              , 0, 1, 2],
        ["firstname:DESC"             , 2, 1, 0],
        ["firstname:DESC;lastname:ASC", 1, 2, 0],
        ["firstname:ASC;lastname:DESC", 1, 0, 2],
        ["lastname:DESC;firstname:ASC", 2, 0, 1],
        ["lastname:ASC;firstname:DESC", 0, 2, 1],
      ]).test("Test with order : '%s'", async (str, idOfC1, idOfC2, idOfC3) => {
        const res = await request(app)
          .get("/api/years/"+ year.id + "/caussenards/")
          .query({order: str})
          .set({"Authorization": tokens.verifUser});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(200);
        expect(res.body.result).toEqual("success");
        expect(res.body.content).toHaveLength(3);
        expect(res.body.content[idOfC1].id).toEqual(caussenard1.id);
        expect(res.body.content[idOfC2].id).toEqual(caussenard2.id);
        expect(res.body.content[idOfC3].id).toEqual(caussenard3.id);
      });
    });

    describe("Test request limit", () => {
      each([
        [0, 0],
        [1, 1],
        [3, 3],
        [6, 3],
      ]).test("Test with limit : '%d'", async (aLimit, listSize) => {
        const res = await request(app)
          .get("/api/years/"+ year.id + "/caussenards/")
          .query({limit: aLimit})
          .set({"Authorization": tokens.verifUser});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(200);
        expect(res.body.result).toEqual("success");
        expect(res.body.content.length).toEqual(listSize);
      });  
    });

    describe("Test request offset", () => {
      each([
        [0, 3],
        [1, 2],
        [3, 0],
        [5, 0],
      ]).test("Test with limit : '%d'", async (anOffset, listSize) => {
        const res = await request(app)
          .get("/api/years/"+ year.id + "/caussenards/")
          .query({offset: anOffset})
          .set({"Authorization": tokens.verifUser});
        expect(res.type).toEqual("application/json");
        expect(res.statusCode).toEqual(200);
        expect(res.body.result).toEqual("success");
        expect(res.body.content).toHaveLength(listSize);
      });  
    });
  }
};