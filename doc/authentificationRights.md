# Authorization Levels

## Levels

- 0: Client
- 1: User
- 2: Verified User
- 3: Editer
- 4: Manager
- 5: Admin



## Authorizations

|                                  | 0    | 1          | 2          | 3    | 4    | 5    |
| -------------------------------- | ---- | ---------- | ---------- | ---- | ---- | ---- |
| Update Token                     |      | x          |            |      |      |      |
|                                  |      |            |            |      |      |      |
| Get User Profile                 |      | (x if own) |            |      |      | x    |
| Update User Profile              |      | (x if own) |            |      |      | x    |
| Update User Password             |      | (x if own) |            |      |      | x    |
| Update User Authentication level |      |            |            |      |      | x    |
| Delete User Profile              |      | (x if own) |            |      |      | x    |
| Get Users List                   |      |            |            |      |      | x    |
|                                  |      |            |            |      |      |      |
| Get Years List                   | x    |            |            |      |      |      |
| Get Years Count                  | x    |            |            |      |      |      |
| Create Year                      |      |            |            | x    |      |      |
| Get Year                         | x    |            |            |      |      |      |
| Update Year                      |      |            |            | x    |      |      |
| Delete Year                      |      |            |            |      | x    |      |
| Get Teams of a Year              | x    |            |            |      |      |      |
| Get Caussenards of a Year        | x    |            |            |      |      |      |
|                                  |      |            |            |      |      |      |
| Get Teams List                   | x    |            |            |      |      |      |
| Get Teams Count                  | x    |            |            |      |      |      |
| Create Year                      |      |            |            | x    |      |      |
| Get Team                         | x    |            |            |      |      |      |
| Update Team                      |      |            |            | x    |      |      |
| Delete Team                      |      |            |            |      | x    |      |
| Get Caussenards of a Team        | x    |            |            |      |      |      |
|                                  |      |            |            |      |      |      |
| Get Caussenards List             | x    |            |            |      |      |      |
| Get Caussenards Count            | x    |            |            |      |      |      |
| Create Caussenard                |      | (x if own) |            | x    |      |      |
| Get Caussenard                   | x    |            |            |      |      |      |
| Update Caussenard                |      |            | (x if own) | x    |      |      |
| Delete Caussenard                |      |            |            |      | x    |      |
| Get Years of a Caussenard        | x    |            |            |      |      |      |
| Get Teams of a Caussenard        | x    |            |            |      |      |      |
|                                  |      |            |            |      |      |      |
| Get Members                      | x    |            |            |      |      |      |
| Link Member                      |      |            | (x if own) | x    |      |      |
| Unlink Member                    |      |            | (x if own) | x    |      |      |
|                                  |      |            |            |      |      |      |
|                                  |      |            |            |      |      |      |
|                                  |      |            |            |      |      |      |
|                                  |      |            |            |      |      |      |
|                                  |      |            |            |      |      |      |
|                                  |      |            |            |      |      |      |
|                                  |      |            |            |      |      |      |
|                                  |      |            |            |      |      |      |
|                                  |      |            |            |      |      |      |
|                                  |      |            |            |      |      |      |
|                                  |      |            |            |      |      |      |

